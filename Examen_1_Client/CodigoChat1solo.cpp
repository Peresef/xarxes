#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>

#define MAX_MENSAJES 30
#define MAX_BYTES 1300

sf::TcpSocket socket;
bool quit=false;

std::string mensaje = "";
std::vector<std::string> aMensajes;

int main(){
	sf::Thread* thread = 0;
	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf")){
		std::cout << "Can't load the font file" << std::endl;
	}

	sf::Text chattingText(mensaje, font, 14);
	chattingText.setFillColor(sf::Color(0, 160, 0));
	chattingText.setStyle(sf::Text::Bold);
	

	sf::Text text(mensaje, font, 14);
	text.setFillColor(sf::Color(0, 160, 0));
	text.setStyle(sf::Text::Bold);
	text.setPosition(0, 560);

	sf::RectangleShape separator(sf::Vector2f(800, 5));
	separator.setFillColor(sf::Color(200, 200, 200, 255));
	separator.setPosition(0, 550);
	
	sf::IpAddress ip = sf::IpAddress::getLocalAddress();
	socket.connect(ip, 50001);
	std::cout << "Conectado cliente" << std::endl;

	while (window.isOpen()){
		std::string chatting = "1.Reproducir musica.";
		chattingText.setPosition(sf::Vector2f(0, (float)20));
		chattingText.setString(chatting);
		window.draw(chattingText);
		chatting = "2.Parar musica.";
		chattingText.setPosition(sf::Vector2f(0, (float)40));
		chattingText.setString(chatting);
		window.draw(chattingText);
		chatting = "3.Mostrar Monstruos y cantidad: 3_4";
		chattingText.setPosition(sf::Vector2f(0, (float)60));
		chattingText.setString(chatting);
		window.draw(chattingText);
		chatting = "4.Parar Monstruos.";
		chattingText.setPosition(sf::Vector2f(0, (float)80));
		chattingText.setString(chatting);
		window.draw(chattingText);

		sf::Event evento;
		while (window.pollEvent(evento)){
			switch (evento.type){
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (evento.key.code == sf::Keyboard::Escape) {
					window.close();
				}
				else if (evento.key.code == sf::Keyboard::Return){
					sf::Socket::Status state;
					state = socket.send(mensaje.c_str(), mensaje.size());
					switch (state){
						case sf::Socket::Done:
							break;
						case sf::Socket::NotReady:
							break;
						case sf::Socket::Partial:
							break;
						case sf::Socket::Disconnected:
							std::cout << "Desconectado" << std::endl;
							aMensajes.push_back("El otro usuario se ha desconectado.");
							quit = true;
							socket.disconnect();
							break;
						case sf::Socket::Error:
							std::cout << "Error al enviar" << std::endl;
							break;
						default:
							break;
					}

					if (aMensajes.size() > 25){
						aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
					}
					mensaje = "";
				}
				break;
			case sf::Event::TextEntered:
				if (evento.text.unicode >= 32 && evento.text.unicode <= 126)
					mensaje += (char)evento.text.unicode;
				else if (evento.text.unicode == 8 && mensaje.size() > 0)
					mensaje.erase(mensaje.size() - 1, mensaje.size());
				break;
			}
		}
		window.draw(separator);
		for (size_t i = 0; i < aMensajes.size(); i++){
			std::string chatting = aMensajes[i];
			chattingText.setPosition(sf::Vector2f(0, (float)100 * i));
			chattingText.setString(chatting);
			window.draw(chattingText);
		}

		std::string mensaje_ = mensaje + "_";
		text.setString(mensaje_);
		window.draw(text);

		window.display();
		window.clear();
	}
	std::string exitmmssg = "/exit";
	socket.send(exitmmssg.c_str(), exitmmssg.size());

	quit = true;
	socket.disconnect();
}