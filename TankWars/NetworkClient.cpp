#include "NetworkClient.h"
#include <string>

#define MAX_BYTES 1300

//comprovem si es rep alguna cosa, i a partir d'aqui gestionem tot
void NetworkClient::Update() {
	char data[MAX_BYTES];
	std::size_t received;
	memset(data, 0, sizeof data);
	if (socket.receive(data, MAX_BYTES, received) == sf::Socket::Done) {
		std::string DataStr(data);
		std::string DataRec = DataStr;
		switch (DataStr.at(0) - '0') {
		case SPAWNCLIENT:
			SpawnTanks( DataStr.at(2)-'0');
			//sera per a spawnejar players;
			break;
		case INITURN:
			//Comen�ar el countdown		
			_game->InitTurn(); //Li diem que pot comen�ar el torn
			break;
		case ENDTURN:
			//Acabem el torn, i enviem les dades al server
			_game->TurnMovement = false;
			_game->isinTurn = false;
			_game->TurnAttack = false;
			SendData();
			break;
		case SELFMOV:
			//aqui es faria el rebre dades del teu tank
			ReceiveSelfData(DataStr);
			break;
		case OTHERMOV:
			//rebre dades d'altres tanks
			ReceiveOtherPlayerData(DataStr);
			break;
		case ENDGAME:
			//Donem avis de que s'ha acabat el game
			if(DataStr.at(2) == 'T')_game->EndServerGame(true);
			else _game->EndServerGame(false);
			break;
		case DISCONNECTED:
			//avisem de una desconexio
			_game->Disconnected();
			break;
		default:
			break;
		}
	}
}



//Funci� que rep dades de s� mateix
void NetworkClient::ReceiveSelfData(std::string data){
	//Type_BoolMovimentCorrecte_BoolCollisioDispar_Score_\
	//Moviment correcte
	if (data.at(2) == 'F') {
		//aix� vol dir que el moviment es incorrecte
		//s'hauria de tornar a la posici� inicial
		glm::vec3 positionTop;
		positionTop = _game->Panzer.getPositionTOP();
		positionTop.x = _game->Panzer.TempsCells[0].getPosX()*(-0.5f)-0.25f;
		positionTop.y = _game->Panzer.TempsCells[0].getPosY()*0.5f+0.25;
		_game->Panzer.setPositionTOP(positionTop);
		//Positionbottom
		glm::vec3 positionBottom;
		positionBottom = _game->Panzer.getPositionBOTTOM();
		positionBottom.x = _game->Panzer.TempsCells[0].getPosX()*(-0.5f)-0.25f;
		positionBottom.y = _game->Panzer.TempsCells[0].getPosY()*0.5f+0.25f;
		_game->Panzer.setPositionBOTTOM(positionBottom);
		//cel�les
		_game->Panzer.setCellX(_game->Panzer.TempsCells[0].getPosX());
		_game->Panzer.setCellY(_game->Panzer.TempsCells[0].getPosY());
		//un cop l'enviem a la posici� inicial netejem les cel�les
		_game->Panzer.TempsCells.clear();
	}
	else if (data.at(2) == 'T') {
		//vol dir que el moviment es correcte per tant podem eliminar les cel�les
		_game->Panzer.TempsCells.clear();
	}
	//Bool Col�lisio del tret
	if (data.at(4) == 'T') {
		//aix� vol dir que s'ha tocat
	}
	else if (data.at(4) == 'F') {
		//aix� vol dir que no s'ha tocat
	}
	//Score del jugador
	std::string score;
	score = data.at(6);
	score += data.at(7);
	_game->score = std::stoi(score);
}

//Per rebre dades d'altre jugadors
void NetworkClient::ReceiveOtherPlayerData(std::string data){
	//Type_ID_Angle_BoolDispara_Score_VectorCell/
	//guardem les dades de manera temporal i despr�s gestionem, que sin� es molt merder anar fent fors
	//Id  que farem servir per dir quin tanc s'ha d'actualitzar la posici�
	int _ID;
	_ID = data.at(2) - '0';
	//Gestionem el angle de la bala
	int _Angle;
	std::string AngleStr;
	AngleStr = data.at(4);
	AngleStr += data.at(5);
	AngleStr += data.at(6);
	_Angle = std::stoi(AngleStr);
	//Bool de atac
	bool _Attack;
	if (data.at(8) == 'T') {
		//voldra dir que disparara
		_Attack = true;
	}
	else if (data.at(8) == 'F') {
		//voldra dir que no disparara
		_Attack = false;
	}
	//Score
	std::string score;
	score = data.at(10);
	score += data.at(11);
	_game->_Enemies[_ID].setScore(std::stoi(score));
	//recepci� de les Cel�les
	char nextChar;
	std::vector<Cell> cells;
	//a partir de quin numero comen�a el llistat de cel�les
	int positioncells = 13;
	nextChar = data.at(positioncells);
	while (nextChar != '/') {
		//Mentre no trobem aquest s�mbol anirem fent 
		std::string splitStr;
		splitStr = data.at(positioncells);
		splitStr += data.at(positioncells + 1);
		splitStr += data.at(positioncells + 2);
		splitStr += data.at(positioncells + 3);
		Cell cell;
		splitStr >> cell;
		cells.push_back(cell);
		if (data.at(positioncells + 4) == '/') {
			positioncells += 4;
		}
		else positioncells += 5;
		nextChar = data.at(positioncells);
	}
	//Fem la gestio del missatge rebut
	for (int i = 0; i < _game->_Enemies.size(); i++) {
		if (_game->_Enemies[i].GetID() == _ID) {
			//ja tenim el enemic identificat i li diem que haur� de fer 
			//li diem a l'enemic que pot atacar
			_game->_Enemies[i].InitMovement(cells, _Attack, _Angle);
		}
	}
}



//Per enviar dades quan el jugador es mou
void NetworkClient::SendData(){
	std::string msg;
	//Part de la ID 
	msg = std::to_string(_game->Panzer.GetID());
	msg += "_";
	//Part del boolea de si ha disparat
	if (_game->Shooted)msg += "T";
	else msg += "F";
	msg += "_";
	//Falta el angle amb el qual disparara
	float angle = _game->Panzer.getAngleTOP();
	angle = glm::mod(angle, 360.0f);
	if (glm::abs((int)angle) < 10)msg += "00";
	else if (glm::abs((int)angle) < 100)msg += "0";
	msg += std::to_string(glm::abs((int)angle));
	//Part del vector de cel�les
	for (int i = 0; i < _game->Panzer.TempsCells.size(); i++) {
		std::string cellst;
		cellst << _game->Panzer.TempsCells[i];
		msg += cellst;
	}
	//Per avisar que s'ha acabat
	msg += "/";
	//enviem el missatge
	_game->Shooted = false;
	Send(msg);

}

//Funcio que envia les dades al servidor, en principi nom�s enviar� lo de les cel�les i tal, per� es gen�rica aix� es pot enviar m�s dades
//Falta testejar
void NetworkClient::Send(std::string data){
	std::string splidata;
	sf::Socket::Status state;
	std::size_t BytesSent;
	bool FinishedSending;
	state = socket.send(data.c_str(), data.size(), BytesSent);
	switch (state) {
	case sf::Socket::Done:
		//std::cout << "Enviado" << std::endl;
		break;
	case sf::Socket::NotReady:
		//std::cout << "NotReady" << std::endl;
		break;
	case sf::Socket::Partial:
	//	std::cout << "S'est� enviant el misstage" << std::endl;
		FinishedSending = false;
		splidata = data;
		while (!FinishedSending) {
			sf::Socket::Status state;
			splidata = splidata.substr(BytesSent, data.size()- BytesSent);
			state = socket.send(splidata.c_str(), splidata.size(), BytesSent);
			switch (state) {
			case sf::Socket::Done:
				FinishedSending = true;
				break;
			case sf::Socket::NotReady:
				FinishedSending = true;
				break;
			case sf::Socket::Partial:
				break;
			case sf::Socket::Disconnected:
				FinishedSending = true;
				std::cout << "Desconectado" << std::endl;
				socket.disconnect();
				break;
			case sf::Socket::Error:
				FinishedSending = true;
				std::cout << "Error al enviar" << std::endl;
				break;
			default:
				break;
			}

		}
		break;
	case sf::Socket::Disconnected:
		//vol dir que s'ha desconectaat el servidor
		std::cout << "Desconectado" << std::endl;
		socket.disconnect();
		break;
	case sf::Socket::Error:
		std::cout << "Error al enviar" << std::endl;
		//Si hi ha un error al enviar dades que hauriem de fer?? reenviar al cap de X? 
		//Lo normal seria en els jocs un popup de retry, pero como que no
		break;
	default:
		break;
	}

}

void NetworkClient::SpawnTanks(int ID){
	//Ho fiquem aqui per no embrutar m�s el Game.
	//Spawnejar el player
	//es la ID de 1-4, per tant id-1 per trobar posici�
	glm::vec3 positionTop;
	positionTop = _game->Panzer.getPositionTOP();
	positionTop.x = _game->SpawnPoints[ID - 1]._point.x;
	positionTop.y = _game->SpawnPoints[ID - 1]._point.y;
	_game->Panzer.setPositionTOP(positionTop);
	//Positionbottom
	glm::vec3 positionBottom;
	positionBottom = _game->Panzer.getPositionBOTTOM();
	positionBottom.x = _game->SpawnPoints[ID - 1]._point.x;
	positionBottom.y = _game->SpawnPoints[ID - 1]._point.y;
	_game->Panzer.setPositionBOTTOM(positionBottom);
	//cel�les

	_game->Panzer.setCellX(_game->ConvertPositionToGridX(glm::abs(_game->SpawnPoints[ID - 1]._point.x)));
	_game->Panzer.setCellY(_game->ConvertPositionToGridX(_game->SpawnPoints[ID - 1]._point.y));
	_game->Panzer.SetID(ID);
	//moure el model
	_game->_gameElements.getGameElement(TOP_TANK)._angle = _game->Panzer.getAngleTOP();
	_game->_gameElements.getGameElement(TOP_TANK)._rotation.z = 1.0f;
	_game->_gameElements.getGameElement(BOTTOM_TANK)._angle = _game->Panzer.getAngleBOTTOM();
	_game->_gameElements.getGameElement(BOTTOM_TANK)._rotation.z = 1.0f;
	_game->_gameElements.getGameElement(TOP_TANK)._translate = _game->Panzer.getPositionTOP();
	_game->_gameElements.getGameElement(BOTTOM_TANK)._translate = _game->Panzer.getPositionBOTTOM();

	//Els enemics
	_game->SpawnEnemy(ID);

}

NetworkClient::NetworkClient(Game* game){
	sf::IpAddress ip = sf::IpAddress::getLocalAddress();
	socket.connect(ip, 55001);
	socket.setBlocking(false);
	_game = game;
}

NetworkClient::NetworkClient()
{
}




NetworkClient::~NetworkClient(){
	socket.disconnect();
}


//Operators
std::string & operator<<(std::string & out, Cell cell)
{
	out += "_";
	if (cell.getPosX() < 10)out += "0";
	int PosX = cell.getPosX();
	if (cell.getPosX() < 0)PosX = 0;
	out += std::to_string(PosX);
	if (cell.getPosY() < 10)out += "0";
	int PosY = cell.getPosY();
	if (cell.getPosY() < 0)PosY = 0;
	out += std::to_string(PosY);
	return out;
}

std::string & operator >> (std::string & is, Cell & cell)
{
	std::string _X;
	_X = is.at(0);
	_X += is.at(1);
	std::string _Y;
	_Y = is.at(2);
	_Y += is.at(3);
	cell.setCellPosition(std::stoi(_X), std::stoi(_Y));
	return is;
}

