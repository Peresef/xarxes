#pragma once
class Cell
{
private:
	int _PosX;
	int _PosY;
public:
	void setCellPosition(int posx, int posy);
	int getPosX();
	int getPosY();
	Cell();
	~Cell();
};

