#pragma once


struct State {
	float x;
	float v;
};

struct Derivative {
	float dx;
	float dv;
};