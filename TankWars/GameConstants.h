#pragma once

//Player Tank objects

#define TOP_TANK 4
#define BOTTOM_TANK 5
#define BULLETOBJECT 7

#define DIRLIGHT 0
#define POINTLIGHT 1
#define SPOTLIGHT 2

#define SPAWNCLIENT 0
#define INITURN 1
#define ENDTURN 2
#define SELFMOV 3
#define OTHERMOV 4
#define ENDGAME 5
#define DISCONNECTED 6
