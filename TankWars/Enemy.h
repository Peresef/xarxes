#pragma once
class Enemy
{
private:
	int _idspawn;//id del objecte spawnejat
	bool _move;//boolea de si es moura o no (mes endavant)
	bool _good;//boolea de si dona punts o no
public:
	Enemy(int idspawn, bool move, bool good);
	~Enemy();
	bool getGood();
	int getID();
};

