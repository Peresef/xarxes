#pragma once
#include "Game.h"
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>

class NetworkClient
{
	friend class Game;
private:
	bool Debug = false;
	sf::TcpSocket socket;
	Game* _game;
public:
	friend std::string &operator<<(std::string &out, Cell cell);
	friend std::string &operator>> (std::string & is, Cell & cell);

	void Update();
	void ReceiveSelfData(std::string data);
	void ReceiveOtherPlayerData(std::string data);
	void SendData();
	void Send(std::string data);
	void SpawnTanks(int ID);
	NetworkClient(Game * game);
	NetworkClient();
	~NetworkClient();
};

