#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <SFML\Audio.hpp>
#include <string>
#include <iostream>
#include <vector>
#include "Constants.h"

#define MAX_MENSAJES 30
#define MAX_BYTES 1300

void RecieveFunction();

sf::Mutex chat;
sf::TcpSocket socket;
bool quit=false;

std::string mensaje = "";
std::vector<std::string> aMensajes;
std::vector<sf::Sprite*> monsters;

int main(){
	sf::Thread* thread = 0;
	/*
	sf::RectangleShape separator(sf::Vector2f(800, 5));
	separator.setFillColor(sf::Color(200, 200, 200, 255));
	separator.setPosition(0, 550);
	*/

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Examen");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf")){
		std::cout << "Can't load the font file" << std::endl;
	}

	sf::Text chattingText(mensaje, font, 14);
	chattingText.setFillColor(sf::Color(0, 160, 0));
	chattingText.setStyle(sf::Text::Bold);

	//
	sf::Text text(mensaje, font, 14);
	text.setFillColor(sf::Color(0, 160, 0));
	text.setStyle(sf::Text::Bold);
	text.setPosition(0, 560);

	//sound
	sf::Sound sound;
	sf::SoundBuffer soundBuffer;
	if (!soundBuffer.loadFromFile("terror.wav")){
		std::cout << "Can't load the audio file" << std::endl;
	}
	sound.setBuffer(soundBuffer);
	sound.setLoop(true);


	sf::Texture imageSource;
	if (!imageSource.loadFromFile("monster.png")){
		std::cout << "Can't load the image file" << std::endl;
	}



	sf::IpAddress ip = sf::IpAddress::getLocalAddress();

	sf::TcpListener listener;
	if (listener.listen(50001) != sf::Socket::Done) {
		std::cout << "No te puedes vincular al puerto 5000" << std::endl;
		return -1;
	}

	if (listener.accept(socket) != sf::Socket::Done) {
		std::cout << "Error al aceptar conexi�n" << std::endl;
		return -1;
	}

	thread = new sf::Thread(&RecieveFunction);
	thread->launch();

	std::cout << "Conectado server" << std::endl;
		while (window.isOpen()){
			while (!aMensajes.empty()){
				int size;
				std::string tempString = "";
				tempString = aMensajes.front();
				aMensajes.pop_back();
				switch (std::atoi(&tempString[0])){
				case PLAYMUSIC:
					std::cout << "Playing Music" << std::endl;
					sound.play();
					break;
				case STOPMUSIC:
					std::cout << "Stopping music" << std::endl;
					sound.stop();
					break;
				case SHOWMONSTER:
					std::cout << "Print monsters" << std::endl;
					size = std::atoi(&tempString[2]);
					for (int i = 0;i < size;i++){
						sf::Sprite* imageSprite = new sf::Sprite;
						imageSprite->setTexture(imageSource, true);
						imageSprite->setPosition(((double)rand() / (RAND_MAX + 1)), (double)rand() / (RAND_MAX + 1));
						monsters.push_back(imageSprite);
					}
					break;
				case STOPMONSTER:
					std::cout << "Stop monsters" << std::endl;
					for (int i = 0;i < monsters.size();i++){
						delete monsters[i];
						monsters.pop_back();
					}

					break;
				default:
					break;
				}
			}
			sf::Sprite imageSprite;
			imageSprite.setTexture(imageSource, true);
			imageSprite.setPosition(0.2f, 0.2f);
			window.draw(imageSprite);
			for (int i = 0;i < monsters.size();i++){
				window.draw(*monsters[i]);
			}
		}

	quit = true;
	for (int i = 0;i < monsters.size();i++){
		delete monsters[i];
	}
	monsters.clear();
	listener.close();
	socket.disconnect();
}

void RecieveFunction() {
	char data[MAX_BYTES];
	while (!quit) {
		std::size_t received;
		memset(data, 0, sizeof data);
		if (socket.receive(data, MAX_BYTES, received) == sf::Socket::Done) {
			std::string DataStr(data);
			if (DataStr == "/exit") {
				std::cout << "Desconectado" << std::endl;
				chat.lock();
				aMensajes.push_back("El otro usuario se ha desconectado.");
				chat.unlock();
				quit = true;
				socket.disconnect();
				break;
			}
			chat.lock();		
			aMensajes.push_back(DataStr);
			chat.unlock();
		}
	}
}