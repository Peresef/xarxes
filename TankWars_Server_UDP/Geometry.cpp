#include "Geometry.h"
#include "ErrorManagement.h"
#include <iostream>


using namespace std;


/*
* Constructor 
*/
Geometry::Geometry(){
	
}


Geometry::~Geometry(){
	for (int i = 0; i < NUMBASICOBJECTS; i++) {
		if (_verticesData[i] != 0)
			delete[] _verticesData[i];
	};
}

/*
* Load the game elements from a text file
*/
void Geometry::loadGameElements(char fileName[100]) {
	/* Text format
	<number of game elements>
	<type of game element> <vec3 position> <angle> <vec3 rotation> <vec3 scale>
	*/
	GameObject tempObject;

	ifstream file;
	file.open(fileName);

	if (file.is_open()) {
		//TODO: Read the content and add it into the data structure

		file >> bullet;
		file >> tempObject;
		bool boolea = true;
		while (!file.eof()) {
			if (tempObject._objectType == BOTTOM_TANK || tempObject._objectType == TOP_TANK) tempObject._textureFile = "./resources/textures/gold.jpg";
			else if (tempObject._objectType == PLANE) {
				if (boolea) {
					tempObject._textureFile = "./resources/textures/floor.jpg";
					boolea = false;
				}
				else tempObject._textureFile = "./resources/textures/roof.jpg";
			}
			else tempObject._textureFile = "./resources/textures/brick.jpg";
			tempObject._ID = 100;
			_listOfObjects.push_back(tempObject);
			file >> tempObject;

		}

		file.close();
		findingBB();
	}

	else {
		string message = "The file " + string(fileName) + " doesn't exists";
		ErrorManagement::errorRunTime(message);
	}
}

void Geometry::findingBB() {
	// Find Aligned Bounding Box (ABB)
	for (unsigned int i = 0;i < _listOfObjects.size(); i++) {
		float maxX = -100000.f; //very small value
		float maxY = -100000.f;
		float maxZ = -100000.f;
		float minX = 100000.f;  //very big value
		float minY = 100000.f;
		float minZ = 100000.f;

		for (int j = 0; j < _numVertices[_listOfObjects[i]._objectType];j++) {
			if (_verticesData[_listOfObjects[i]._objectType][j].position.x > maxX) maxX = _verticesData[_listOfObjects[i]._objectType][j].position.x;
			if (_verticesData[_listOfObjects[i]._objectType][j].position.y > maxY) maxY = _verticesData[_listOfObjects[i]._objectType][j].position.y;
			if (_verticesData[_listOfObjects[i]._objectType][j].position.z > maxZ) maxZ = _verticesData[_listOfObjects[i]._objectType][j].position.z;
			if (_verticesData[_listOfObjects[i]._objectType][j].position.x < minX) minX = _verticesData[_listOfObjects[i]._objectType][j].position.x;
			if (_verticesData[_listOfObjects[i]._objectType][j].position.y < minY) minY = _verticesData[_listOfObjects[i]._objectType][j].position.y;
			if (_verticesData[_listOfObjects[i]._objectType][j].position.z < minZ) minZ = _verticesData[_listOfObjects[i]._objectType][j].position.z;
		}

		_listOfObjects[i]._boundingAABB.OMin = glm::vec3(minX, minY, minZ);
		_listOfObjects[i]._boundingAABB.OMax = glm::vec3(maxX, maxY, maxZ);
		_listOfObjects[i]._boundingAABB.OExtent = ((_listOfObjects[i]._boundingAABB.OMax - _listOfObjects[i]._boundingAABB.OMin) / 2.0f);
		_listOfObjects[i]._boundingAABB.OCentre = _listOfObjects[i]._boundingAABB.OMin + _listOfObjects[i]._boundingAABB.Extent;
	}
}

void Geometry::findingBB(int i) {
	float maxX = -100000.f; //very small value
	float maxY = -100000.f;
	float maxZ = -100000.f;
	float minX = 100000.f;  //very big value
	float minY = 100000.f;
	float minZ = 100000.f;

	for (int j = 0; j < _numVertices[_listOfObjects[i]._objectType];j++) {
		if (_verticesData[_listOfObjects[i]._objectType][j].position.x > maxX) maxX = _verticesData[_listOfObjects[i]._objectType][j].position.x;
		if (_verticesData[_listOfObjects[i]._objectType][j].position.y > maxY) maxY = _verticesData[_listOfObjects[i]._objectType][j].position.y;
		if (_verticesData[_listOfObjects[i]._objectType][j].position.z > maxZ) maxZ = _verticesData[_listOfObjects[i]._objectType][j].position.z;
		if (_verticesData[_listOfObjects[i]._objectType][j].position.x < minX) minX = _verticesData[_listOfObjects[i]._objectType][j].position.x;
		if (_verticesData[_listOfObjects[i]._objectType][j].position.y < minY) minY = _verticesData[_listOfObjects[i]._objectType][j].position.y;
		if (_verticesData[_listOfObjects[i]._objectType][j].position.z < minZ) minZ = _verticesData[_listOfObjects[i]._objectType][j].position.z;
	}

	_listOfObjects[i]._boundingAABB.OMin = glm::vec3(minX, minY, minZ);
	_listOfObjects[i]._boundingAABB.OMax = glm::vec3(maxX, maxY, maxZ);
	_listOfObjects[i]._boundingAABB.OExtent = ((_listOfObjects[i]._boundingAABB.OMax - _listOfObjects[i]._boundingAABB.OMin) / 2.0f);
	_listOfObjects[i]._boundingAABB.OCentre = _listOfObjects[i]._boundingAABB.OMin + _listOfObjects[i]._boundingAABB.Extent;
}


/*
* Get the vertices data for an specific object
* @param objectID is the identifier of the requested object
* @return Vertex * is an array with all the vertices data
*/
Vertex * Geometry::getData(int objectID){
	return _verticesData[objectID];
}

/*
* Get the number of vertices for an specific object
* @param objectID is the identifier of the requested object
* @return int is the number of vertices
*/

int Geometry::getNumVertices(int objectID){
	return _numVertices[objectID];
}

/*
* Get the number of elements to render
*/
int Geometry::getNumGameElements() {
	return _listOfObjects.size();
}

/*
* Get the number of vertices of an specific game element
* @param objectID is the identifier of the requested object
*/
GameObject & Geometry::getGameElement(int objectID) {
	return (_listOfObjects[objectID]);
}

int Geometry::getGameElementTankPos(int ID) {
	for (int i = 0;i < _listOfObjects.size();i++){
		if (_listOfObjects[i]._ID == ID)return i;
	}
	return NULL;
}

void Geometry::removeLastElement(int id){
	_listOfObjects.erase(_listOfObjects.begin() + id);
}

void Geometry::removeLastElement(){
	_listOfObjects.erase(_listOfObjects.end()-1);
}

void Geometry::createCube(int ID, glm::vec3 RGB) {
	
	_numVertices.push_back(MAXVERTICESSQUARE);
	_verticesData.push_back(new Vertex[_numVertices[ID]]);

	//FRONTAL
	_verticesData[ID][0].setPosition(-1, -1, 0);
	_verticesData[ID][0].setUV(0, 0);
	_verticesData[ID][0].setNormal(0,0,-1);
	_verticesData[ID][1].setPosition(1, 1, 0);
	_verticesData[ID][1].setUV(1, 1);
	_verticesData[ID][1].setNormal(0, 0, -1);
	_verticesData[ID][2].setPosition(-1, 1, 0);
	_verticesData[ID][2].setUV(0, 1);
	_verticesData[ID][2].setNormal(0, 0, -1);
	_verticesData[ID][3].setPosition(-1, -1, 0);
	_verticesData[ID][3].setUV(0, 0);
	_verticesData[ID][3].setNormal(0, 0, -1);
	_verticesData[ID][4].setPosition(1, -1, 0);
	_verticesData[ID][4].setUV(1, 0);
	_verticesData[ID][4].setNormal(0, 0, -1);
	_verticesData[ID][5].setPosition(1, 1, 0);
	_verticesData[ID][5].setUV(1, 1);
	_verticesData[ID][5].setNormal(0, 0, -1);

	//TRASERA 
	_verticesData[ID][6].setPosition(  1, -1, 2);
	_verticesData[ID][6].setUV(1, 0);
	_verticesData[ID][6].setNormal(0, 0, 1);
	_verticesData[ID][7].setPosition( -1,  1, 2);
	_verticesData[ID][7].setUV(0, 1);
	_verticesData[ID][7].setNormal(0, 0, 1);
	_verticesData[ID][8].setPosition(  1,  1, 2);
	_verticesData[ID][8].setUV(1, 1);
	_verticesData[ID][8].setNormal(0, 0, 1);
	_verticesData[ID][9].setPosition(  1, -1, 2);
	_verticesData[ID][9].setUV(1, 0);
	_verticesData[ID][9].setNormal(0, 0, 1);
	_verticesData[ID][10].setPosition(-1, -1, 2);
	_verticesData[ID][10].setUV(0, 0);
	_verticesData[ID][10].setNormal(0, 0, 1);
	_verticesData[ID][11].setPosition(-1,  1, 2);
	_verticesData[ID][11].setUV(0, 1);
	_verticesData[ID][11].setNormal(0, 0, 1);

	//LATERAL ESQUERRA (JORDI ALBA)
	_verticesData[ID][12].setPosition(-1, -1, 0);
	_verticesData[ID][12].setUV(1, 0);
	_verticesData[ID][12].setNormal(-1, 0, 0);
	_verticesData[ID][14].setPosition(-1,  1, 0);
	_verticesData[ID][14].setUV(0, 0);
	_verticesData[ID][14].setNormal(-1, 0, 0);
	_verticesData[ID][13].setPosition(-1,  1, 2);
	_verticesData[ID][13].setUV(0, 1);
	_verticesData[ID][13].setNormal(-1, 0, 0);
	_verticesData[ID][15].setPosition(-1, -1, 0);
	_verticesData[ID][15].setUV(1, 0);
	_verticesData[ID][15].setNormal(-1, 0, 0);
	_verticesData[ID][16].setPosition(-1,  1, 2);
	_verticesData[ID][16].setUV(0, 1);
	_verticesData[ID][16].setNormal(-1, 0, 0);
	_verticesData[ID][17].setPosition(-1, -1, 2);
	_verticesData[ID][17].setUV(1, 1);
	_verticesData[ID][17].setNormal(-1, 0, 0);

	//LATERAL DRET (DANI ALVES)
	_verticesData[ID][18].setPosition(1, -1, 0);
	_verticesData[ID][18].setUV(0, 0);
	_verticesData[ID][18].setNormal(1, 0, 0);
	_verticesData[ID][19].setPosition(1,  1, 2);
	_verticesData[ID][19].setUV(1, 1);
	_verticesData[ID][19].setNormal(1, 0, 0);
	_verticesData[ID][20].setPosition(1,  1, 0);
	_verticesData[ID][20].setUV(1, 0);
	_verticesData[ID][20].setNormal(1, 0, 0);
	_verticesData[ID][21].setPosition(1, -1, 0);
	_verticesData[ID][21].setUV(0, 0);
	_verticesData[ID][21].setNormal(1, 0, 0);
	_verticesData[ID][22].setPosition(1, -1, 2);
	_verticesData[ID][22].setUV(0, 1);
	_verticesData[ID][22].setNormal(1, 0, 0);
	_verticesData[ID][23].setPosition(1,  1, 2);
	_verticesData[ID][23].setUV(1, 1);
	_verticesData[ID][23].setNormal(1, 0, 0);

	//ADALT
	_verticesData[ID][24].setPosition(-1, 1, 0);
	_verticesData[ID][24].setUV(1, 0);
	_verticesData[ID][24].setNormal(0, 1, 0);
	_verticesData[ID][25].setPosition(1,  1, 0);
	_verticesData[ID][25].setUV(0, 0);
	_verticesData[ID][25].setNormal(0, 1, 0);
	_verticesData[ID][26].setPosition(1,  1, 2);
	_verticesData[ID][26].setUV(0, 1);
	_verticesData[ID][26].setNormal(0, 1, 0);
	_verticesData[ID][27].setPosition(-1, 1, 0);
	_verticesData[ID][27].setUV(1, 0);
	_verticesData[ID][27].setNormal(0, 1, 0);
	_verticesData[ID][28].setPosition(1, 1, 2);
	_verticesData[ID][28].setUV(0, 1);
	_verticesData[ID][28].setNormal(0, 1, 0);
	_verticesData[ID][29].setPosition(-1, 1, 2);
	_verticesData[ID][29].setUV(1, 1);
	_verticesData[ID][29].setNormal(0, 1, 0);

	//ABAIX
	_verticesData[ID][30].setPosition(-1, -1,  0);
	_verticesData[ID][30].setUV(1, 0);
	_verticesData[ID][30].setNormal(0, -1, 0);
	_verticesData[ID][32].setPosition(-1, -1, 2);
	_verticesData[ID][32].setUV(1, 1);
	_verticesData[ID][32].setNormal(0, -1, 0);
	_verticesData[ID][31].setPosition( 1,  -1, 2);
	_verticesData[ID][31].setUV(0, 1);
	_verticesData[ID][31].setNormal(0, -1, 0);
	_verticesData[ID][33].setPosition(-1, -1,  0);
	_verticesData[ID][33].setUV(1, 0);
	_verticesData[ID][33].setNormal(0, -1, 0);
	_verticesData[ID][35].setPosition(1, -1, 2);
	_verticesData[ID][35].setUV(0, 1);
	_verticesData[ID][35].setNormal(0, -1, 0);
	_verticesData[ID][34].setPosition( 1,  -1, 0);
	_verticesData[ID][34].setUV(0, 0);
	_verticesData[ID][34].setNormal(0, -1, 0);

	for (int i = 0;i < 35;i++) {
		glm::vec3 pos = glm::normalize(glm::vec3(_verticesData[ID][i].position.x, _verticesData[ID][i].position.y, _verticesData[ID][i].position.z));
		_verticesData[ID][i].setNormal(pos);
	}

	//VAMO A PINTA
	for (int i = 0; i < MAXVERTICESSQUARE; i++) {
		_verticesData[ID][i].setColor((GLubyte)RGB.r, (GLubyte)RGB.g, (GLubyte)RGB.b, 255);
	}
	
}

void Geometry::LoadASE() {
	_loader.loadAse("models/tankUP.ASE", _numVertices, _verticesData);
	_loader.loadAse("models/tankBOTTOM.ASE", _numVertices, _verticesData);
	_loader.loadAse("models/enemy.ASE", _numVertices, _verticesData);
}

void Geometry::createPlane(int i, glm::vec3 RGB) {
	_numVertices.push_back(6);
	_verticesData.push_back(new Vertex[_numVertices[i]]);

	// top: P1, P2, P4, P4, P3, P1
	_verticesData[i][0].setPosition(-0.5f, -0.5f, 0.0f);
	_verticesData[i][0].setUV(0, 0);
	_verticesData[i][1].setPosition(0.5f, -0.5f, 0.0f);
	_verticesData[i][1].setUV(1, 0);
	_verticesData[i][2].setPosition(0.5f, 0.5f, 0.0f);
	_verticesData[i][2].setUV(1, 1);
	_verticesData[i][3].setPosition(0.5f, 0.5f, 0.0f);
	_verticesData[i][3].setUV(1, 1);
	_verticesData[i][4].setPosition(-0.5f, 0.5f, 0.0f);
	_verticesData[i][4].setUV(0, 1);
	_verticesData[i][5].setPosition(-0.5f, -0.5f, 0.0f);
	_verticesData[i][5].setUV(0, 0);

	for (int z = 0; z < 6; z++){
		glm::vec3 pos = glm::vec3(0.0f, 1.0f, 0.0f);
		_verticesData[i][z].setNormal(pos);
	}

	for (int _i = 0; _i < 6; _i++) {
		_verticesData[i][_i].setColor((GLubyte)RGB.r, (GLubyte)RGB.g, (GLubyte)RGB.b, 255);
	}
}

void Geometry::UpdateBB(){
	GameObject currentRenderedGameElement;

	for (int i = 0; i < getNumGameElements(); i++) {
		currentRenderedGameElement = getGameElement(i);
		glm::mat4 scaleMatrix, noRotateMatrix;
		noRotateMatrix = glm::translate(noRotateMatrix, currentRenderedGameElement._translate);

		scaleMatrix = glm::scale(scaleMatrix, currentRenderedGameElement._scale);
		noRotateMatrix = glm::scale(noRotateMatrix, currentRenderedGameElement._scale);

		//posicions al mon del objecte
		glm::vec4 centre = noRotateMatrix*glm::vec4(getGameElement(i)._boundingAABB.OCentre, 1);
		glm::vec4 extent = scaleMatrix*glm::vec4(getGameElement(i)._boundingAABB.OExtent, 1);

		glm::vec4 min = noRotateMatrix*glm::vec4(getGameElement(i)._boundingAABB.OMin, 1);
		glm::vec4 max = noRotateMatrix*glm::vec4(getGameElement(i)._boundingAABB.OMax, 1);

		//asignem valors a les BB segons els calculs
		getGameElement(i)._boundingAABB.Centre.x = centre.x;
		getGameElement(i)._boundingAABB.Centre.y = centre.y;
		getGameElement(i)._boundingAABB.Centre.z = centre.z;

		getGameElement(i)._boundingAABB.Min.x = min.x;
		getGameElement(i)._boundingAABB.Min.y = min.y;
		getGameElement(i)._boundingAABB.Min.z = min.z;

		getGameElement(i)._boundingAABB.Max.x = max.x;
		getGameElement(i)._boundingAABB.Max.y = max.y;
		getGameElement(i)._boundingAABB.Max.z = max.z;


		getGameElement(i)._boundingAABB.Extent.x = extent.x;
		getGameElement(i)._boundingAABB.Extent.y = extent.y;
		getGameElement(i)._boundingAABB.Extent.z = extent.z;
	}
}

bool Geometry::collision(glm::vec3 movement, int position){
	//calculem la BB en la posicio a on es moura el objecte
	glm::mat4 scaleMatrix, noRotateMatrix;
	//glm::vec3 tempTrans = getGameElement(BOTTOM_TANK)._translate;
	glm::vec3 tempTrans = movement;
	noRotateMatrix = glm::translate(noRotateMatrix, tempTrans);

	scaleMatrix = glm::scale(scaleMatrix, getGameElement(position)._scale);
	noRotateMatrix = glm::scale(noRotateMatrix, getGameElement(position)._scale);

	//posicions al mon del objecte
	glm::vec4 centre = noRotateMatrix*glm::vec4(getGameElement(position)._boundingAABB.OCentre, 1);
	glm::vec4 extent = scaleMatrix*glm::vec4(getGameElement(position)._boundingAABB.OExtent, 1);

	glm::vec4 min = noRotateMatrix*glm::vec4(getGameElement(position)._boundingAABB.OMin, 1);
	glm::vec4 max = noRotateMatrix*glm::vec4(getGameElement(position)._boundingAABB.OMax, 1);

	//asignem valors a les BB segons els calculs
	getGameElement(position)._boundingAABB.Centre.x = centre.x;
	getGameElement(position)._boundingAABB.Centre.y = centre.y;
	getGameElement(position)._boundingAABB.Centre.z = centre.z;

	getGameElement(position)._boundingAABB.Min.x = min.x;
	getGameElement(position)._boundingAABB.Min.y = min.y;
	getGameElement(position)._boundingAABB.Min.z = min.z;

	getGameElement(position)._boundingAABB.Max.x = max.x;
	getGameElement(position)._boundingAABB.Max.y = max.y;
	getGameElement(position)._boundingAABB.Max.z = max.z;


	getGameElement(position)._boundingAABB.Extent.x = extent.x;
	getGameElement(position)._boundingAABB.Extent.y = extent.y;
	getGameElement(position)._boundingAABB.Extent.z = extent.z;

	for (int i = 0; i < getNumGameElements(); i++) {

		if (getGameElement(position)._ID != getGameElement(i)._ID) {
				if (getGameElement(position).OverlapAABB(getGameElement(i)._boundingAABB)) {
					//com collisionará recalculem la BB en la posicio anterior.
					glm::mat4 scaleMatrix, noRotateMatrix;
					noRotateMatrix = glm::translate(noRotateMatrix, getGameElement(position)._translate);

					scaleMatrix = glm::scale(scaleMatrix, getGameElement(position)._scale);
					noRotateMatrix = glm::scale(noRotateMatrix, getGameElement(position)._scale);

					//posicions al mon del objecte
					glm::vec4 centre = noRotateMatrix*glm::vec4(getGameElement(position)._boundingAABB.OCentre, 1);
					glm::vec4 extent = scaleMatrix*glm::vec4(getGameElement(position)._boundingAABB.OExtent, 1);

					glm::vec4 min = noRotateMatrix*glm::vec4(getGameElement(position)._boundingAABB.OMin, 1);
					glm::vec4 max = noRotateMatrix*glm::vec4(getGameElement(position)._boundingAABB.OMax, 1);

					//asignem valors a les BB segons els calculs
					getGameElement(position)._boundingAABB.Centre.x = centre.x;
					getGameElement(position)._boundingAABB.Centre.y = centre.y;
					getGameElement(position)._boundingAABB.Centre.z = centre.z;

					getGameElement(position)._boundingAABB.Min.x = min.x;
					getGameElement(position)._boundingAABB.Min.y = min.y;
					getGameElement(position)._boundingAABB.Min.z = min.z;

					getGameElement(position)._boundingAABB.Max.x = max.x;
					getGameElement(position)._boundingAABB.Max.y = max.y;
					getGameElement(position)._boundingAABB.Max.z = max.z;


					getGameElement(position)._boundingAABB.Extent.x = extent.x;
					getGameElement(position)._boundingAABB.Extent.y = extent.y;
					getGameElement(position)._boundingAABB.Extent.z = extent.z;

					return true;
				}
		}
	}
	//al no colisionar podem deixar la BB com a la de seguent pos.
	return false;
}

void Geometry::createPyramid(int ID, glm::vec3 RGB){
	
	_numVertices.push_back(MAXVERTICESPYRAMID);
	_verticesData.push_back(new Vertex[_numVertices[ID]]);
	
	//FRONTAL
	_verticesData[ID][0].setPosition(-1, -1, 0);
	_verticesData[ID][1].setPosition( 1, -1, 0);
	_verticesData[ID][2].setPosition( 0,  1, 0.5);
	
	//TRASERA
	_verticesData[ID][3].setPosition( 1, -1, 1);
	_verticesData[ID][4].setPosition(-1, -1, 1);
	_verticesData[ID][5].setPosition( 0,  1, 0.5);
	
	//LATERAL ESQUERRA  (ADRIANO --> TOCA EL SUPLENT)
	_verticesData[ID][6].setPosition(-1,  -1, 0);
	_verticesData[ID][7].setPosition(-1, -1, 1);
	_verticesData[ID][8].setPosition (0,  1, 0.5);
	
	//LATERAL DRET (ALEIX VIDAL)
	_verticesData[ID][9].setPosition(  1, -1, 0);
	_verticesData[ID][10].setPosition( 1, -1, 0);
	_verticesData[ID][11].setPosition( 0,  0, 0.5);
	
	//BASE
	_verticesData[ID][12].setPosition(-1, -1, 0);
	_verticesData[ID][13].setPosition( 1, -1, 0);
	_verticesData[ID][14].setPosition( 1, -1, 1);
	_verticesData[ID][15].setPosition(-1, -1, 0);
	_verticesData[ID][16].setPosition( 1, -1, 1);
	_verticesData[ID][17].setPosition(-1, -1, 1);
	
	//VAMO A PINTA
	for (int i = 0; i < MAXVERTICESPYRAMID; i++) {
		_verticesData[ID][i].setColor((GLubyte)RGB.r, (GLubyte)RGB.g, (GLubyte)RGB.b, 255);
	}
}

/*
funcio que serveix per a instanciar la bala del enemic, li passem el texture manager per agafar la ID de la textura
*/
void Geometry::BulletEnemy(int ID) {	
	GameObject tempObject;
	tempObject._ID = ID;
	tempObject._objectType = 1;
	tempObject._translate = glm::vec3(0.0f, 0.0f, 0.0f);
	tempObject._angle = 180;
	tempObject._rotation = glm::vec3(0.0f, 0.0f, 1.0f);
	tempObject._scale = glm::vec3(0.025f, 0.025f, 0.025f);
	tempObject._textureFile = "./resources/textures/susin.jpg";
	tempObject._materialType = 1;
	_listOfObjects.push_back(tempObject);
	findingBB((getNumGameElements() - 1));
}

void Geometry::ChangeColor(glm::vec3 color, int ID){
	int maxVer = _numVertices[ID];
	for (int i = 0; i < maxVer; i++) {
		_verticesData[ID][i].setColor((GLubyte)color.r, (GLubyte)color.g, (GLubyte)color.b, 255);
	}
}

/*
funcio per a fer el spawneig dels enemics
*/
void Geometry::SpawnEnemy(glm::vec3 translate, int ID){
	GameObject tempObject;
	tempObject._ID = ID;
	tempObject._objectType = 6;
	tempObject._translate = translate;
	tempObject._angle = 0;
	tempObject._rotation = glm::vec3(0.0f, 0.0f, 1.0f);
	tempObject._scale = glm::vec3(1.0f, 1.0f, 1.0f);
	tempObject._textureFile = "./resources/textures/susin.jpg";
	tempObject._materialType = 1;
	_listOfObjects.push_back(tempObject);
	findingBB((getNumGameElements() - 1));
}
