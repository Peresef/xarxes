#include "Network.h"

struct less_than_IdMove{
	inline bool operator() (const NetworkInfo& struct1, const NetworkInfo& struct2){
		return (struct1._idMove < struct2._idMove);
	}
};


Network::Network() :
	listening(&Network::Receive, this),
	_users(&_recievedMessages,
		&_sendMessages,
		&_lockqueueReceive,
		&_lockqueueSend){
	_exit = false;
	_enemies;
	_gameElements;
	run = false;
	if (socketClients.bind(55000) != sf::Socket::Done) {
		std::cout << "Can't bind to port 55000" << std::endl;
	}
	listening.launch();
	for (int i = 0;i < MAXGAMES;i++){
		_GamesStarted[i] = false;
	}
}

Network::~Network() {
	Clear();
}

void Network::Clear() {
	_exit = true;
	_lockqueueReceive.lock();
	while (!_recievedMessages.empty())_recievedMessages.pop_front();
	_lockqueueReceive.unlock();
	_lockqueueSend.lock();
	while (!_sendMessages.empty())_sendMessages.pop_front();
	_lockqueueSend.unlock();
	socketClients.unbind();
}

void Network::AssignPos(int IDGame, int ID){
	if (_users.getNumberUsersInRoom(IDGame) > _playersInGameReady[IDGame]._playersReady){
		//Posicionament Randomitzat y codificat
		srand(time(NULL));
		int X = rand() % 12;
		int Y = rand() % 12;
		_enemies = _users.GetEnemies(IDGame);
		if (_enemies->size() > 0){
			for (int i = 0; i < _enemies->size(); i++){
				if (_enemies->at(i).getCellX() == X)X = rand() % 12;
			}
		}
		OutputMemoryBitStream ombs;
		//el O es la Id del missatge el 4 es el numero de bits que necessites per representar
		ombs.Write(2, 4);
		ombs.Write(ID, 3);
		ombs.Write(X, 4);
		ombs.Write(Y, 4);
		//envia posicio
		send(ID, ombs.GetBufferPtr(), ombs.GetByteLength());
		//activa el resendtime que es el encarregat de enviar 
		//cada resendtime els missatges critics
		resendTime = clock();

		//Notify the game that it has to set ID player to X Y pos.
		Cell tempCell;
		tempCell.setCellPosition(X, Y);
		NetworkInfo tempInfo1;
		tempInfo1._movement.push_back(tempCell);
		tempInfo1._TYPE = SETPOS;
		tempInfo1._ID = ID;
		tempInfo1.IDGame = IDGame;
		addTank(tempInfo1, IDGame);
		_playersInGameReady[IDGame]._playersReady += 1;
		//notifica als jugadors que hi ha un nou amic
		OutputMemoryBitStream ombsNP;
		Code(NEWPLAYER, tempInfo1, ombsNP);
		for (int j = 0; j < _users.getNumberUsersInRoom(IDGame); j++) {
			send(_users.getPlayersInRoom(IDGame)->at(j)->getID(), ombsNP.GetBufferPtr(), ombsNP.GetByteLength());
			NetworkInfo tempInfo;
			tempInfo._missatge = ombsNP.GetBufferPtr();
			tempInfo._missatgeSize = ombsNP.GetByteLength();
			tempInfo._critical = true;
			tempInfo._TYPE = NEWPLAYER;
			tempInfo._life = 4;
			tempInfo.IDGame = IDGame;
			tempInfo._WHO_TO_SEND_ID = _users.getPlayersInRoom(IDGame)->at(j)->getID();
			CriticalMessages.push_back(tempInfo);
		}
		if (_users.getNumberUsersInRoom(IDGame) == _playersInGameReady[IDGame]._playersReady){
			_GamesStarted[IDGame] = true;
			for (int j = 0; j < _users.getNumberUsersInRoom(IDGame); j++) {
				NetworkInfo tempInfo2;
				OutputMemoryBitStream ombsG0;
				Code(START, tempInfo2, ombsG0);
				send(_users.getPlayersInRoom(IDGame)->at(j)->getID(), ombsG0.GetBufferPtr(), ombsG0.GetByteLength());
			}
			pingtime = clock();
		}
	}
}

void Network::Decode(InputMemoryBitStream data, sf::IpAddress _rIP, unsigned short _rPort) {
	NetworkInfo tempInfo;
	std::vector<NetworkPlayer*>* chatUsers;
	int _ID = 0;
	int Type = 0; int IDPl = 0;
	int idMove = 0;
	int posX = 10;
	int posY = 10;
	int sizeInputs = 0;
	int input = 0;
	int idfake = 0;
	int angle = 10;
	bool positiu = 0;
	bool shoot = 0;
	int MaxPL = 0;
	int IDSala = 0;
	bool state = false;
	std::string mssg= "0";
	std::string UserName = "";
	std::string Pass = "";
	data.Read(&Type, 4);
	OutputMemoryBitStream ombs;
	switch (Type) {
	case RECEIVEHELLO:
		if (_users.addUser(_rIP, _rPort)){
			ombs.Write(0, 4);
			ombs.Write(_users.getUser(_rIP, _rPort)->getID(), 4);
			send(_users.getUser(_rIP, _rPort)->getID(), ombs.GetBufferPtr(), ombs.GetByteLength());

		}
		break;
	case RECEIVEDATA:
		//type 4 bits
		//idmove 14 bits
		//idplayer 3 bits
		//angle 9 bits
		//signe X 1 bit
		//posXFinal 13bits
		//posYFinal 13bits
		//posXInicial 13bits
		//posYInicial 13bits
		//numero de inputs que rebem 8bits
		//inputs 3 bits
		if (_users.getUser(_rIP, _rPort) != nullptr){
			_ID = _users.getUser(_rIP, _rPort)->getID();
			data.Read(&idMove, 14);
			data.Read(&idfake, 3);
			data.Read(&angle, 9);
			data.Read(&positiu, 1);
			data.Read(&posX, 13);
			data.Read(&posY, 13);
			data.Read(&sizeInputs, 8);
			for (int i = 0; i < sizeInputs;i++){
				data.Read(&input, 3);
				tempInfo.Inputs.push_back(input);
				input = 0;
			}
			tempInfo.IDGame = _users.getUserRoom(_ID);
			tempInfo._ID = _ID;
			tempInfo._TYPE = Type;
			tempInfo._idMove = idMove;
			tempInfo._angle = angle;
			if (positiu)tempInfo._posX = ((float)posX / 1000.0f);
			else tempInfo._posX = -((float)posX / 1000.0f);
			tempInfo._posY = ((float)posY / 1000.0f);
			IDSala = _users.getUserRoom(_ID);
			tempInfo.IDGame = IDSala;
			_lockqueueReceive.lock();
			_recievedMessages.push_back(tempInfo);
			_lockqueueReceive.unlock();
		}
		return;
	case ACKPING:
		if (_users.getUser(_rIP, _rPort) != nullptr){
			_ID = _users.getUser(_rIP, _rPort)->getID();
			_lockqueueCritical.lock();
			for (int i = 0; i < CriticalMessages.size(); i++) {
				if (CriticalMessages[i]._TYPE == PING){
					if (CriticalMessages[i]._WHO_TO_SEND_ID == _ID) {
						CriticalMessages[i]._ack = true;
					}
				}
			}
			_lockqueueCritical.unlock();
		}
		break;
	case ACKSTARTIME:
		_ID = _users.getUser(_rIP, _rPort)->getID();
		for (int i = 0; i < CriticalMessages.size(); i++) {
			if (CriticalMessages[i]._TYPE == STARTIMER){
				if (CriticalMessages[i]._WHO_TO_SEND_ID == _ID) {
					CriticalMessages[i]._ack = true;
				}
			}
		}
		break;
	case RECEIVESHOOT:
		_ID = _users.getUser(_rIP, _rPort)->getID();
		tempInfo._TYPE = Type;
		tempInfo._ID = _ID;
		tempInfo._shoot = true;
		IDSala = _users.getUserRoom(_ID);
		tempInfo.IDGame = IDSala;
		_lockqueueReceive.lock();
		_recievedMessages.push_back(tempInfo);
		_lockqueueReceive.unlock();
		break;
	case ACKSHOOT:
		_ID = _users.getUser(_rIP, _rPort)->getID();
		_lockqueueCritical.lock();
		for (int i = 0; i < CriticalMessages.size(); i++) {
			if (CriticalMessages[i]._TYPE == SENDSHOOT){
				if (CriticalMessages[i]._WHO_TO_SEND_ID == _ID) {
					CriticalMessages[i]._ack = true;
				}
			}
		}
		_lockqueueCritical.unlock();
		break;
	case ACKNEWPLAYER:
		_ID = _users.getUser(_rIP, _rPort)->getID();
		_lockqueueCritical.lock();
		for (int i = 0; i < CriticalMessages.size(); i++) {
			if (CriticalMessages[i]._TYPE == NEWPLAYER){
				if (CriticalMessages[i]._WHO_TO_SEND_ID == _ID)
					CriticalMessages[i]._ack = true;
			}
		}
		_lockqueueCritical.unlock();
		break;
	case RECEIVECREATEROOMREQUEST:
		_ID = _users.getUser(_rIP, _rPort)->getID();
		data.ReadString(&mssg);
		data.Read(&MaxPL, 3);
		state = _users.createRoom(_ID,mssg, MaxPL);
		if (state){
			Code(CANCREATEROOM, tempInfo, ombs);
			send(_ID, ombs.GetBufferPtr(), ombs.GetByteLength());
			NetworkInfo tempInfo;
			_playersInGameReady.push_back(tempInfo);
		}
		else {
			Code(CANNOTCREATEROOM, tempInfo, ombs);
			send(_ID, ombs.GetBufferPtr(), ombs.GetByteLength());
		}
		break;
	case GETROOMS:
		if (_users.getUser(_rIP, _rPort) != nullptr){
			_ID = _users.getUser(_rIP, _rPort)->getID();
			Code(SENDROOMLIST, tempInfo, ombs);
			send(_ID, ombs.GetBufferPtr(), ombs.GetByteLength());
		}
		break;
	case JOINROOM:
		_ID = _users.getUser(_rIP, _rPort)->getID();
		data.Read(&IDSala);
		state =_users.UserJoinsGame(_ID, IDSala);
		if (state){
			Code(CANJOINROOM, tempInfo, ombs);
			send(_ID, ombs.GetBufferPtr(), ombs.GetByteLength());
		}
		else{
			Code(CANNOTJOINROOM, tempInfo, ombs);
			send(_ID, ombs.GetBufferPtr(), ombs.GetByteLength());
		}
		if (_users.isRoomFull(IDSala)){
			OutputMemoryBitStream ombsTemp;
			_ID=_users.getRoomOwner(IDSala)->getID();
			Code(CANSTARTGAME, tempInfo, ombsTemp);
			send(_ID, ombsTemp.GetBufferPtr(), ombsTemp.GetByteLength());
		}
		break;
	case CHAT:
		_ID = _users.getUser(_rIP, _rPort)->getID();
		IDSala = _users.getUserRoom(_ID);
		data.ReadString(&mssg);
		tempInfo._chat = mssg;
		Code(CHATMESSAGE, tempInfo, ombs);
		chatUsers = _users.getPlayersInRoom(IDSala);
		for (int i = 0;i < chatUsers->size();i++){
			send(chatUsers->at(i)->getID(), ombs.GetBufferPtr(), ombs.GetByteLength());
		}
		break;
	case STARTGAMEREQUEST:
		_ID = _users.getUser(_rIP, _rPort)->getID();
		IDSala = _users.getUserRoom(_ID);
		startGame(IDSala);
		break;
	case READYTORECEIVEPOS:
		_ID= _ID = _users.getUser(_rIP, _rPort)->getID();
		IDSala = _users.getUserRoom(_ID);
		AssignPos(IDSala,_ID);
		break;
	case RECEIVELOGIN:
		//_ID = _users.getUser(_rIP, _rPort)->getID();
		data.ReadString(&UserName);
		data.ReadString(&Pass);
		state = CheckUserPass(UserName, Pass);
		ombs.Write(SENDACKLOGIN, 4);
		ombs.Write(state, 1);
		//send(_ID, ombs.GetBufferPtr(), ombs.GetByteLength());
		socketClients.send(ombs.GetBufferPtr(), ombs.GetByteLength(), _rIP, _rPort);
		break;
	case RECEIVEADDGAME:
		data.ReadString(&UserName);
		AddGameSQL(UserName);
		break;
	default:
		break;
	}
}

void Network::Code(int _case, NetworkInfo tempInfo, OutputMemoryBitStream& ombs) {
	int posX = 0;
	int posY = 0;
	int numSales = 0;
	switch (_case) {
	case STARTIMER:
		ombs.Write(1, 4);
		return;
	case ANSWERCLIENT:
		ombs.Write(3, 4);
		ombs.Write(tempInfo._idMove, 14);
		ombs.Write(tempInfo._ID, 3);
		ombs.Write(tempInfo._angle, 9);
		ombs.Write(tempInfo._movementAllowed, 1);
		if (tempInfo._posX < 0.0f)
			ombs.Write(0, 1);
		else ombs.Write(1, 1);
		posX = (int)(glm::abs(tempInfo._posX) * 1000.0f);
		ombs.Write((uint16_t)posX, 13);
		posY = (int)(tempInfo._posY * 1000.0f);
		ombs.Write((uint16_t)posY, 13);
		ombs.Write(tempInfo._shoot, 1);
		ombs.Write(tempInfo.Inputs.size(), 8);
		for (int i = 0;i < tempInfo.Inputs.size();i++){
			ombs.Write(tempInfo.Inputs[i], 3);
		}
		return;
	case SENDSHOOT:
		ombs.Write(SENDSHOOT, 4);
		ombs.Write(tempInfo._ID, 3);
		ombs.Write(tempInfo._canShoot, 1);
		return;
	case ENDGAME_WIN:
		ombs.Write(5, 4);
		ombs.Write(1, 1);
		return;
	case ENDGAME_LOSE:
		ombs.Write(5, 4);
		ombs.Write(0, 1);
		return;
	case NEWPLAYER:
		ombs.Write(6, 4);
		_enemies = _users.GetEnemies(tempInfo.IDGame);
		ombs.Write(_enemies->size(), 3);
		for (int i = 0; i < _enemies->size(); i++) {
			ombs.Write(_enemies->at(i).GetID(), 3);
			ombs.Write(_enemies->at(i).getCellX(), 4);
			ombs.Write(_enemies->at(i).getCellY(), 4);
		}
		return;
	case PING:
		ombs.Write(7, 4);
		_enemies = _users.GetEnemies(tempInfo.IDGame);
		ombs.Write(_enemies->size(), 3);
		for (int i = 0; i < _enemies->size(); i++) {
			ombs.Write(_enemies->at(i).GetID(), 3);
			ombs.Write(_enemies->at(i).score, 7);
		}
		return;
	case CANCREATEROOM:
		ombs.Write(8, 4);
		ombs.Write(1, 1);
		return;
	case CANNOTCREATEROOM:
		ombs.Write(8, 4);
		ombs.Write(0, 1);
		return;
	case SENDROOMLIST:
		ombs.Write(9, 4);
		numSales = _users.GetNumberGames();
		ombs.Write(numSales, 2);
		for (int i = 0;i < numSales;i++){
			ombs.Write(i, 2);
			ombs.WriteString(_users.GetName(i));
			ombs.Write(_users.getNumberUsersInRoom(i), 3);
			ombs.Write(_users.getMaxUsersInRoom(i), 3);
		}
		return;
	case CANJOINROOM:
		ombs.Write(10, 4);
		ombs.Write(1, 1);
		return;
	case CANNOTJOINROOM:
		ombs.Write(10, 4);
		ombs.Write(0, 1);
		return;
	case CHATMESSAGE:
			ombs.Write(11, 4);
			ombs.WriteString(tempInfo._chat);
		return;
	case CANSTARTGAME:
		ombs.Write(12, 4);
		return;
	case START:
		ombs.Write(13, 4);
		return;
	default:
		return;
	}
	return;
}

void Network::Receive() {
	//rebre missatges dels clients i descodificarlos
	while (!_exit) {
		sf::IpAddress rIP;
		unsigned short rPort;
		char data[MAX_BYTES];
		std::size_t received;
		memset(data, 0, sizeof data);
		socketClients.receive(data, MAX_BYTES, received, rIP, rPort);
		InputMemoryBitStream imbs(data, sizeof(data) * 8);
		Decode(imbs, rIP, rPort);
	}
}

void Network::addTank(NetworkInfo tempInfo, int IDGame){
	_gameElements = _users.GetGeometry(IDGame);
	for (int i = 0;i < _gameElements->getNumGameElements();i++){
		if (_gameElements->getGameElement(i)._ID == tempInfo._ID){
			_gameElements->getGameElement(i)._translate =
				(glm::vec3(ConvertGridToPositionX(tempInfo._movement[0].getPosX()),
					ConvertGridToPositionY(tempInfo._movement[0].getPosY()),
					0.0f));
			return;
		}
	}
	SpawnEnemy(tempInfo._ID, tempInfo._movement[0].getPosX(), tempInfo._movement[0].getPosY(), IDGame);
}

void Network::SpawnEnemy(int ID, int X, int Y, int IDGame){
	TankEnemy Tank;
	glm::vec3 _pos = glm::vec3(ConvertGridToPositionX(glm::abs(X)), ConvertGridToPositionY(glm::abs(Y)), 0.0f);
	Tank.InitTankEnemy(_pos, _pos, 0, 0, 5, 0, X, Y);
	_gameElements = _users.GetGeometry(IDGame);
	_gameElements->SpawnEnemy(_pos, ID);
	Tank.SetID(ID);
	_enemies = _users.GetEnemies(IDGame);
	_enemies->push_back(Tank);
}

float Network::ConvertGridToPositionY(float _X){
	return (_X / 2) + 0.25f;
}

float Network::ConvertGridToPositionX(float _X){
	return -(_X / 2) - 0.25f;
}

void Network::startGame(int IDGame) {
	NetworkInfo tempinfo1;
	OutputMemoryBitStream ombsStart;
	Code(STARTIMER, tempinfo1, ombsStart);
	std::vector<NetworkPlayer*>* chatUsers = _users.getPlayersInRoom(IDGame);
	for (int i = 0;i < chatUsers->size();i++){
		send(chatUsers->at(i)->getID(), ombsStart.GetBufferPtr(), ombsStart.GetByteLength());
		NetworkInfo tempInfo;
		tempInfo._missatge=ombsStart.GetBufferPtr();
		tempInfo._missatgeSize = ombsStart.GetByteLength();
		tempInfo._TYPE=STARTIMER;
		tempInfo._critical = true;
		tempInfo._life = 4;
		tempInfo.IDGame = IDGame;
		tempInfo._WHO_TO_SEND_ID = chatUsers->at(i)->getID();
		_lockqueueCritical.lock();
		CriticalMessages.push_back(tempInfo);
		_lockqueueCritical.unlock();
	}
	_users.startGame(IDGame);
}

void Network::endGame(int winner, int IDGame) {
	if (IDGame != 99){
		NetworkInfo tempinfo;
		OutputMemoryBitStream ombsWin;
		Code(ENDGAME_WIN, tempinfo, ombsWin);
		OutputMemoryBitStream ombsLose;
		Code(ENDGAME_LOSE, tempinfo, ombsLose);
		for (int i = 0; i < _users.getNumberUsersInRoom(IDGame); i++) {
			if (_users.getPlayersInRoom(IDGame)->at(i)->getID() == winner)
				send(_users.getPlayersInRoom(IDGame)->at(i)->getID(), ombsWin.GetBufferPtr(), ombsWin.GetByteLength());
			else send(_users.getPlayersInRoom(IDGame)->at(i)->getID(), ombsLose.GetBufferPtr(), ombsLose.GetByteLength());
		}
	}
}

void Network::endGame(int IDGame) {
	NetworkInfo tempinfo;
	OutputMemoryBitStream ombsEnd;
	Code(DISCONNECTED, tempinfo, ombsEnd);
	for (int i = 0; i < _users.getNumberUsersInRoom(IDGame); i++) {
		send(_users.getPlayersInRoom(IDGame)->at(i)->getID(), ombsEnd.GetBufferPtr(), ombsEnd.GetByteLength());
	}
	tempinfo._TYPE = DISCONNECTED;
	tempinfo.IDGame = IDGame;
	_lockqueueReceive.lock();
	_recievedMessages.push_back(tempinfo);
	_lockqueueReceive.unlock();
	_exit = true;
}

void Network::sendMoves(int IDGame){
		std::vector<NetworkInfo> moves_To_Send;
		std::vector<NetworkPlayer*>* _players = _users.getPlayersInRoom(IDGame);
		for (int i = 0;i < _players->size(); i++){
			NetworkInfo tempInfo;
			moves_To_Send.push_back(tempInfo);
		}
		std::vector<NetworkInfo> moves_To_collapse;
		std::vector<NetworkInfo> messages_to_return;
		std::map<int, int> IDsPlayers;
		for (int i = 0;i < _players->size();i++){
			IDsPlayers[i] = _players->at(i)->getID();
		}
		//carreguem la cua en un vector per deixarle lluirela el abans possible
		_lockqueueSend.lock();
		int queueSize = _sendMessages.size();
		for (int i = 0;i < queueSize;i++){
			if (_sendMessages.front()._TYPE == RECEIVEDATA && _sendMessages.front().IDGame==IDGame){
				NetworkInfo tempInfo = _sendMessages.front();
				moves_To_collapse.push_back(tempInfo);
				_sendMessages.pop_front();
			}
			else {
				messages_to_return.push_back(_sendMessages.front());
				_sendMessages.pop_front();
			}
		}
		for (int i = 0;i < messages_to_return.size();i++){
			_sendMessages.push_back(messages_to_return[i]);
		}
		_lockqueueSend.unlock();
		//entrem si hi ha mes o igual quantitat moviments a tractar que jugadors en partida
		if (moves_To_collapse.size() >= _players->size()){
			//ordenem el vector per nombre de Ids
			std::sort(moves_To_collapse.begin(), moves_To_collapse.end(), less_than_IdMove());
			//recorrem segons la quantitat de jugadors
			for (int i = 0; i < IDsPlayers.size(); i++){
				bool exit = false;
				std::vector<int> tempInputs;
				float posXinicial, posYinicial;
				bool once = true;
				//recorrem el vector a comprimir
				for (int j = 0;j < moves_To_collapse.size();j++){
					//mentre no sigui exit fem el for
					if (!exit){
						//si la ID del moviment que estem mirant es igual
						//a la del jugador que estem calculant
						if (moves_To_collapse[j]._ID == IDsPlayers[i]){
							//agafem la pos incial del primer moviment del jugador que estem calculant
							if (once){
								once = false;
								posXinicial = moves_To_collapse[j]._posXinicial;
								posYinicial = moves_To_collapse[j]._posYinicial;
							}
							//comprobem si esta permes el moviment
							if (!moves_To_collapse[j]._movementAllowed){
								//al no estar-ho guardem el moviment fals
								//i sortim
								moves_To_Send[i] = moves_To_collapse[j];
								exit = true;
							}
							//al ser permes guardem aquest moviment com el que fara el jugador
							//afegim els inputs del moviment correcte al vector de temporals
							for (int k = 0;k < moves_To_collapse[j].Inputs.size();k++){
								tempInputs.push_back(moves_To_collapse[j].Inputs[k]);
							}
							moves_To_Send[i] = moves_To_collapse[j];
						}
					}
				}
				if (!once){
					moves_To_Send[i].Inputs = tempInputs;
					moves_To_Send[i]._posXinicial = posXinicial;
					moves_To_Send[i]._posYinicial = posYinicial;
				}
			}
			//enviem els moviments collapsats a tots els jugadors
			for (int j = 0;j < moves_To_Send.size();j++){
				OutputMemoryBitStream ombsMove;
				Code(ANSWERCLIENT, moves_To_Send[j], ombsMove);
				for (int i = 0;i < IDsPlayers.size();i++){
					send(IDsPlayers[i], ombsMove.GetBufferPtr(), ombsMove.GetByteLength());
				}
			}
			return;
		}
		_lockqueueSend.lock();
		for (int i = 0;i < moves_To_collapse.size();i++){
			_sendMessages.push_back(moves_To_collapse[i]);
		}
		_lockqueueSend.unlock();
}

void Network::sendPing(int IDGame) {
		NetworkInfo tempInfo1;
		tempInfo1.IDGame = IDGame;
		OutputMemoryBitStream ombsPing;
		Code(PING, tempInfo1, ombsPing);
		for (int i = 0; i < _users.getNumberUsersInRoom(IDGame); i++) {
			int _ID = _users.getPlayersInRoom(IDGame)->at(i)->getID();
			send(_ID, ombsPing.GetBufferPtr(), ombsPing.GetByteLength());
			NetworkInfo tempInfo;
			tempInfo.IDGame = IDGame;
			tempInfo._TYPE = PING;
			tempInfo._missatge = ombsPing.GetBufferPtr();
			tempInfo._missatgeSize = ombsPing.GetByteLength();
			tempInfo._critical = true;
			tempInfo._life = 3;
			tempInfo._WHO_TO_SEND_ID = _ID;
			_lockqueueCritical.lock();
			CriticalMessages.push_back(tempInfo);
			_lockqueueCritical.unlock();
		}
}

void Network::SendShoots(int IDGame){
	//agafem els missatges de shoot a tractar
	std::vector<NetworkInfo> Shoots;
	std::vector<NetworkInfo> SendMessagesToReturn;
	_lockqueueSend.lock();
	int queueSize = _sendMessages.size();
	for (int i = 0;i < queueSize;i++){
		if (_sendMessages.front().IDGame == IDGame){
			if (_sendMessages.front()._TYPE == RECEIVESHOOT){
				NetworkInfo tempInfo = _sendMessages.front();
				Shoots.push_back(tempInfo);
				_sendMessages.pop_front();
			}
			else {
				SendMessagesToReturn.push_back(_sendMessages.front());
				_sendMessages.pop_front();
			}
		}
	}
	for (int i = 0;i < SendMessagesToReturn.size();i++){
		_sendMessages.push_back(SendMessagesToReturn[i]);
	}
	_lockqueueSend.unlock();

	for (int i = 0;i < Shoots.size();i++){
		OutputMemoryBitStream ombsShoot;
		Code(SENDSHOOT, Shoots[i], ombsShoot);
		for (int j = 0; j < _users.getNumberUsersInRoom(IDGame); j++) {
			send(_users.getPlayersInRoom(IDGame)->at(j)->getID(), ombsShoot.GetBufferPtr(), ombsShoot.GetByteLength());
			NetworkInfo tempInfo;
			tempInfo._missatge=ombsShoot.GetBufferPtr();
			tempInfo._missatgeSize = ombsShoot.GetByteLength();
			tempInfo._critical = true;
			tempInfo._TYPE = SENDSHOOT;
			tempInfo._life = 5;
			tempInfo.IDGame = IDGame;
			tempInfo._canShoot = Shoots[i]._canShoot;
			tempInfo._ID = Shoots[i]._ID;
			tempInfo._WHO_TO_SEND_ID = _users.getPlayersInRoom(IDGame)->at(j)->getID();
			_lockqueueCritical.lock();
			CriticalMessages.push_back(tempInfo);
			_lockqueueCritical.unlock();
		}
	}
}

void Network::CheckCriticals(){
	//repasar que nomes es borrin els que toca 
	_lockqueueCritical.lock();
	for (int i = 0; i < CriticalMessages.size(); i++) {
		if (CriticalMessages[i].shouldIDelete()) {
			CriticalMessages.erase(CriticalMessages.begin() + i);
		}
	} 
	_lockqueueCritical.unlock();
	resendTimePassed = ((clock() - resendTime) / CLOCKS_PER_SEC) * 60;
	if (resendTimePassed >= CRITICAL_TIME_MS) {
		//chekejar la cua de missatges critics,
		for (int i = 0; i < CriticalMessages.size(); i++) {
			if (CriticalMessages[i].shouldISend()) {
				if (CriticalMessages[i]._TYPE == PING){
					NetworkInfo tempInfo;
					tempInfo.IDGame = CriticalMessages[i].IDGame;
					OutputMemoryBitStream ombsPing;
					Code(PING, tempInfo, ombsPing);
					send(CriticalMessages[i]._WHO_TO_SEND_ID, ombsPing.GetBufferPtr(), ombsPing.GetByteLength());
				}
				else if (CriticalMessages[i]._TYPE == SENDSHOOT){
					OutputMemoryBitStream ombsShoot;
					Code(SENDSHOOT, CriticalMessages[i], ombsShoot);
					send(CriticalMessages[i]._WHO_TO_SEND_ID, ombsShoot.GetBufferPtr(), ombsShoot.GetByteLength());
				}
				else if (CriticalMessages[i]._TYPE == NEWPLAYER){
					NetworkInfo tempInfo;
					OutputMemoryBitStream ombsNP;
					tempInfo.IDGame = CriticalMessages[i].IDGame;
					Code(NEWPLAYER, tempInfo, ombsNP);
					send(CriticalMessages[i]._WHO_TO_SEND_ID, ombsNP.GetBufferPtr(), ombsNP.GetByteLength());
				}
				else if (CriticalMessages[i]._TYPE == STARTIMER){
					NetworkInfo tempinfo;
					OutputMemoryBitStream ombsStart;
					Code(STARTIMER, tempinfo, ombsStart);
					send(CriticalMessages[i]._WHO_TO_SEND_ID, ombsStart.GetBufferPtr(), ombsStart.GetByteLength());
				}
			}
		}
		
		//revisem si hi ha algun client que no ha contestat i l'eliminem
		for (int i = 0; i < CriticalMessages.size(); i++) {
			if (CriticalMessages[i].shouldDeleteClient()) {
				if (CriticalMessages[i]._WHO_TO_SEND_ID != 0){
					int _IDGame = CriticalMessages[i].IDGame;
					_enemies = _users.GetEnemies(_IDGame);
					//per borrar de la llista d'enemics
					for (int x = 0; x < _enemies->size(); x++) {
						if (_enemies->at(x).GetID() == CriticalMessages[i]._WHO_TO_SEND_ID) {
							_gameElements = _users.GetGeometry(_IDGame);
							for (int k = 0;k < _gameElements->getNumGameElements();k++){
								if (_gameElements->getGameElement(k)._ID == _enemies->at(x).GetID()) _gameElements->removeLastElement(k);
							}
							_lockqueueReceive.lock();
							int tempSize= _recievedMessages.size();
							for (int k = 0;k < tempSize;k++){
								NetworkInfo tempinfo = _recievedMessages.front();
								_recievedMessages.pop_front();
								if (tempinfo._ID != _enemies->at(x).GetID())_recievedMessages.push_back(tempinfo);
							}
							_lockqueueReceive.unlock();
							_enemies->erase(_enemies->begin() + x);
						}
					}
					for (int z = 0; z < _users.getNumberUsersInRoom(_IDGame); z++) {
						if (_users.getPlayersInRoom(_IDGame)->at(z)->getID() == CriticalMessages[i]._WHO_TO_SEND_ID) {
							//send desconexion message
							_users.deleteUser(CriticalMessages[i]._WHO_TO_SEND_ID);
							break;
						}
					}
				}
			}
		}
		
		_lockqueueCritical.lock();
		//repassar si hi ha missatges de jugadors que han sigut eliminats
		for (int k = 0; k < CriticalMessages.size(); k++) {
			bool deleteMsg = true;
			//mirem que tingui un jugador assignat, sino es que el missatge no te amo, per tant s'ha de borrar
			for (int f = 0; f < _users.getNumberUsers(); f++) {
				if (_users.getUserThroughPos(f)->getID() == CriticalMessages[k]._WHO_TO_SEND_ID)
					deleteMsg = false;
			}
			if (deleteMsg)
				CriticalMessages.erase(CriticalMessages.begin() + k);
		}
		_lockqueueCritical.unlock();
		resendTime = clock();
		resendTimePassed = clock();
	}
}

void Network::loop() {
	run = true;
	while (run){
		CheckCriticals();
		_users.update();
		for (int i = 0;i < _users.GetNumberGames();i++){
			if (_GamesStarted[i]){
				pingTimepassed = (clock() - pingtime);
				SendShoots(i);
				CheckQueue();
				if (pingTimepassed >= PINGTIME) {
					for (int j = 0;j < _users.GetNumberGames();j++){
						sendPing(j);
						sendMoves(j);
					}
					pingtime = clock();
					pingTimepassed = clock();
				}
			}
		}
	}
}

void Network::CheckQueue(){
	std::vector<NetworkInfo> States;
	std::vector<NetworkInfo> SendMessagesToReturn;
	_lockqueueSend.lock();	
	int queueSize = _sendMessages.size();
	for (int i = 0;i < queueSize;i++){
		if (_sendMessages.front()._TYPE == ENDGAME){
			NetworkInfo tempInfo = _sendMessages.front();
			States.push_back(tempInfo);
			_sendMessages.pop_front();
		}
		else {
			SendMessagesToReturn.push_back(_sendMessages.front());
			_sendMessages.pop_front();
		}
	}
	for (int i = 0;i < SendMessagesToReturn.size();i++){
		_sendMessages.push_back(SendMessagesToReturn[i]);
	}
	_lockqueueSend.unlock();

	for (int i = 0; i < States.size(); i++){
		endGame(States[i]._ID,_users.getUserRoom(States[i]._ID));
	}
}

//Funci� que se li passa el nom i la pass del jugador i checkeja si es correcte en el servidor
bool Network::CheckUserPass(std::string Name, std::string Pass){
	sql::Driver* driver;
	sql::Connection* con;
	sql::Statement* stmt;
	sql::ResultSet* resultSet;
	int num = 0;

	
	driver = get_driver_instance();
	con = driver->connect("tcp://localhost:3306", "root", "");
	stmt = con->createStatement();
	//ens conectem a la base de dades
	stmt->execute("USE tankwars");
	//fem la consulta
	std::string Consulta =  "select count(*) from PLAYERS where NAME='" + Name + "' and PASS = '" + Pass + "'";
	const sql::SQLString ConsultaSQL = Consulta.c_str();
	resultSet = stmt->executeQuery(ConsultaSQL);
	if (resultSet->next()) {
		num = resultSet->getInt(1);
	}
	delete resultSet;

	delete stmt;
	delete con;
	
	return num > 0;
}

void Network::AddGameSQL(std::string Name){
	sql::Driver* driver;
	sql::Connection* con;
	sql::Statement* stmt;
	

	driver = get_driver_instance();
	con = driver->connect("tcp://localhost:3306", "root", "");
	stmt = con->createStatement();
	//ens conectem a la base de dades
	stmt->execute("USE tankwars");
	try {
		//fem la consulta
		std::string Consulta = "UPDATE PLAYERS SET NUMBERGAMES = NUMBERGAMES +1 where NAME='" + Name + "'";
		const sql::SQLString ConsultaSQL = Consulta.c_str();
		stmt->executeQuery(ConsultaSQL);
	}
	catch (sql::SQLException e){
		std::cout << e.what();
	}

	delete stmt;
	delete con;
}

bool Network::send(int whotosend, char* message, size_t size) {
	unsigned short _sPort;
	sf::IpAddress _sIP;

	if(_users.getNumberUsers()>0 && _users.getUser(whotosend)!=nullptr)
		_sPort = _users.getUser(whotosend)->getPort();
	else return false;
	if (_users.getNumberUsers()>0 && _users.getUser(whotosend) != nullptr)
		_sIP = _users.getUser(whotosend)->getIp();
	else return false;
	socketClients.send(message, size, _sIP, _sPort);
	return true;
}
