#include "Game.h"

/**
* Constructor
* Note: It uses an initialization list to set the parameters
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS,
	int maxFPS, bool printFPS,std::deque<NetworkInfo>* recievedMessages,
	std::deque<NetworkInfo>* sendMessages, double _time) :
	_windowTitle(windowTitle), 
	_screenWidth(screenWidth), 
	_screenHeight(screenHeight),
	_gameState(GameState::INIT), 
	_fpsLimiter(enableLimiterFPS, maxFPS, printFPS){
	Shooted = false;
	TurnMovement = false;
	isinTurn = false;
	_gameStart = false;
	_angleBALA = 0.0f;
	Time = 2500.0f;
	score = 0;
	sessionTimer = false;
	_startSession = false;
	_secondsToDelaySession = _time*60;
	_recievedMessages = recievedMessages;
	_sendMessages = sendMessages;
	run();
}

/**
* Destructor
*/
Game::~Game(){
}

/*
* Game execution
*/
void Game::run() {
		//System initializations
	initSystems();
	_gameState = GameState::PLAY;
		//Start the game if all the elements are ready
}

void Game::startGame(){
	_gameStart = true;
}

/*
* Initializes all the game engine components
*/
void Game::initSystems() {
		//Load the current scenario
	_gameElements.createCube(BLUE_CUBE, glm::vec3(0, 0, 255));
	_gameElements.createCube(RED_CUBE, glm::vec3(255, 0, 0));
	_gameElements.createCube(WHITE_CUBE, glm::vec3(255, 255, 255));
	_gameElements.createPlane(PLANE, glm::vec3(200, 200, 200));
	_gameElements.LoadASE();
	_gameElements.createCube(7, glm::vec3(255, 255, 255));
	_gameElements.loadGameElements("./resources/scene2D_server.txt");
}

/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::update() {
	if (_gameState != GameState::EXIT){
		_fpsLimiter.startSynchronization();
		//Update the BB
		_gameElements.UpdateBB();
		if (_gameStart){
			_gameStart = false;
			if (!sessionTimer){
				sessionTimer = true;
				_startSession = true;
				_startTimeSession = clock();
			}
		}
		//Si entra aqu� vol dir que hi ha un missatge a tractar
		if (_gameState != GameState::EXIT){
			int queueSize = _recievedMessages->size();
			for (int i = 0;i < queueSize;i++){
				_fpsLimiter.startSynchronization();
				//Update the BB
				_gameElements.UpdateBB();
				//el servidor agafa el missatge en cua que 
				//ha rebut del client el tracta y
				//l'afegeix a la cua de sendmessages
				NetworkInfo tempInfo = _recievedMessages->front();
				//Comprovem si la network ens ha enviat un missatge de que s'ha desconectat
				//un client
				if (tempInfo._TYPE == DISCONNECTED){
					_recievedMessages->pop_front();
					_gameState = GameState::EXIT;
				}
				else if (tempInfo._TYPE == RECEIVESHOOT){
					//fer simulaci� del shoot
					for (int i = 0;i < _Enemies.size();i++){
						if (_Enemies[i].GetID() == tempInfo._ID){
							_Enemies[i].setShoot(true);
						}
					}
					tempInfo._canShoot = true;
					_sendMessages->push_back(tempInfo);

					_recievedMessages->pop_front();
				}
				//es que el missatge son dades que s'han de procesar
				else{
					doPhysics();
					checkBounds(tempInfo);
					//borrem el missatge rebut de la cua de rebuts
					_recievedMessages->pop_front();

					CheckShoot();
					//Comprova el temps de la sessio, una vegada s'ha donat que s'ha passat ho tanca tot
					// i notifica a la resta de jugadors que s'ha acabat i qui ha guanyat
					if (_startSession){
						_secondsPassedSession = (clock() - _startTimeSession) / CLOCKS_PER_SEC;
						if (_secondsPassedSession >= _secondsToDelaySession){
							int ID_winner = 0;
							int winner_score = 0;
							for (int i = 0; i < _Enemies.size(); i++){
								if (_Enemies[i].score > winner_score) ID_winner = _Enemies[i].GetID();
							}
							NetworkInfo tempInfo;
							tempInfo._TYPE = ENDGAME;
							tempInfo._ID = ID_winner;
							_gameState = GameState::EXIT;
							_sendMessages->push_back(tempInfo);
						}
					}
					//Force synchronization
					_fpsLimiter.forceSynchronization();
				}
			}
		}
		//Update de la network per a que fagi checks dels pings i missatges critics
		doPhysics();
		CheckShoot();

		//Comprova el temps de la sessio, una vegada s'ha donat que s'ha passat ho tanca tot
		// i notifica a la resta de jugadors que s'ha acabat i qui ha guanyat
		if (_startSession){
			_secondsPassedSession = (clock() - _startTimeSession) / CLOCKS_PER_SEC;
			if (_secondsPassedSession >= _secondsToDelaySession){
				int ID_winner = 0;
				int winner_score = 0;
				for (int i = 0; i < _Enemies.size(); i++){
					if (_Enemies[i].score > winner_score) ID_winner = _Enemies[i].GetID();
				}
				NetworkInfo tempInfo;
				tempInfo._TYPE = ENDGAME;
				tempInfo._ID = ID_winner;
				_gameState = GameState::EXIT;
				_sendMessages->push_back(tempInfo);
			}
		}
		//Force synchronization
		_fpsLimiter.forceSynchronization();
	}
}

std::vector<TankEnemy>* Game::getEnemies(){
	return &_Enemies;
}

void Game::checkBounds(NetworkInfo tempInfo){
	if(tempInfo._posY==0.0f || tempInfo._posX==0.0f) return;
	//comprovem si esta dintre del els limits del mapa Y
	if (tempInfo._posY > 0.0f && tempInfo._posY < 6.0f){
		//limits X
		if (tempInfo._posX > -6.0f && tempInfo._posX<0.0f){
			glm::vec3 _pos = glm::vec3(tempInfo._posX, tempInfo._posY, 0.0f);

			//li canviem la posicio en el server
			for (int i = 0;i < _Enemies.size();i++){
				if (_Enemies[i].GetID() == tempInfo._ID){
					if (!_gameElements.collision(_pos, _gameElements.getGameElementTankPos(tempInfo._ID))) {
						_Enemies[i].setPositionTOP(_pos);
						_Enemies[i].setPositionBOTTOM(_pos);
						_Enemies[i].setAngleTOP(tempInfo._angle);
						_Enemies[i].setAngleBOTTOM(tempInfo._angle);
						UpdateEnemyMovements();
					}
					else{
						tempInfo._movementAllowed = false;
						tempInfo._posX = _Enemies[i].getPositionBOTTOM().x;
						tempInfo._posY = _Enemies[i].getPositionBOTTOM().y;
						//les afegim a la cua de missatges a enviar
						_sendMessages->push_back(tempInfo);
						return;
					}
				}
			}

			tempInfo._movementAllowed = true;
			//les afegim a la cua de missatges a enviar
			_sendMessages->push_back(tempInfo);
			return;
		}
		else {
			tempInfo._movementAllowed = false;
			//les afegim a la cua de missatges a enviar
			_sendMessages->push_back(tempInfo);
			return;
		}
	}
	//en cas de no estra dintre el mapa diem que el moviment no esta perm�s
	//i no s'actualitza la posici� dintre el server.
	else {
		tempInfo._movementAllowed = false;
		//les afegim a la cua de missatges a enviar
		_sendMessages->push_back(tempInfo);
		return;
	}
}

bool Game::collisionBala(glm::vec3 movement, int i) {
	//calculem la BB en la posicio a on es moura el objecte
	glm::mat4 scaleMatrix, noRotateMatrix;
	glm::vec3 tempTrans = _gameElements.getGameElement(i)._translate + movement;
	noRotateMatrix = glm::translate(noRotateMatrix, tempTrans);

	scaleMatrix = glm::scale(scaleMatrix, _gameElements.getGameElement(i)._scale);
	noRotateMatrix = glm::scale(noRotateMatrix, _gameElements.getGameElement(i)._scale);
	
	//posicions al mon del objecte
	glm::vec4 centre = noRotateMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OCentre, 1);
	glm::vec4 extent = scaleMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OExtent, 1);

	glm::vec4 min = noRotateMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OMin, 1);
	glm::vec4 max = noRotateMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OMax, 1);

	//asignem valors a les BB segons els calculs
	_gameElements.getGameElement(i)._boundingAABB.Centre.x = centre.x;
	_gameElements.getGameElement(i)._boundingAABB.Centre.y = centre.y;
	_gameElements.getGameElement(i)._boundingAABB.Centre.z = centre.z;

	_gameElements.getGameElement(i)._boundingAABB.Min.x = min.x;
	_gameElements.getGameElement(i)._boundingAABB.Min.y = min.y;
	_gameElements.getGameElement(i)._boundingAABB.Min.z = min.z;

	_gameElements.getGameElement(i)._boundingAABB.Max.x = max.x;
	_gameElements.getGameElement(i)._boundingAABB.Max.y = max.y;
	_gameElements.getGameElement(i)._boundingAABB.Max.z = max.z;


	_gameElements.getGameElement(i)._boundingAABB.Extent.x = extent.x;
	_gameElements.getGameElement(i)._boundingAABB.Extent.y = extent.y;
	_gameElements.getGameElement(i)._boundingAABB.Extent.z = extent.z;

	for (int z = 0; z < _gameElements.getNumGameElements();z++) {
		if (_gameElements.getGameElement(z)._objectType != 4 || _gameElements.getGameElement(z)._objectType != 5 || _gameElements.getGameElement(i)._objectType != 1) {
			if (z != i) {
				if (_gameElements.getGameElement(i).OverlapAABB(_gameElements.getGameElement(z)._boundingAABB)) {					
						if (_gameElements.getGameElement(z)._objectType != 6) {
							if (getEnemyPosthroughID(_gameElements.getGameElement(i)._ID) != NULL){
								_Enemies[getEnemyPosthroughID(_gameElements.getGameElement(i)._ID)].isDone = true;
								_gameElements.removeLastElement(i);
								return true;
							}
							else return false;
						}
						else {
							//Amb aix� voldria dir que ha xocat amb un enemic(o amb ell mateix que es lo primer que filtrem)
							int IDSendShoot = _gameElements.getGameElement(i)._ID;//aixo es la bala
							int IDReceiveShoot = _gameElements.getGameElement(z)._ID;//el que rep
							for (int _i = 0; _i < _Enemies.size(); _i++) {
								//m'asseguro que el tank contra el que col�lisiona no es el mateix que dispara
								if (IDSendShoot != IDReceiveShoot) {
									if (_Enemies[_i].GetID() == IDReceiveShoot) {
										//aix� vol dir que la bala ha col�lisionat amb aquest enemic
										_Enemies[getEnemyPosthroughID(IDSendShoot)].score += 1;
										_Enemies[getEnemyPosthroughID(IDSendShoot)].shootAllowed = true;
										_Enemies[getEnemyPosthroughID(IDSendShoot)].isDone = true;
										//Basicament ens asegurem que el que borrem es
										//una bala i no tota la escena : )
										if (_gameElements.getGameElement(
											_gameElements.getNumGameElements() - 1)._objectType == 1)
											_gameElements.removeLastElement();
										return true;
									}
								}
							}
							
						}
						return false;
				}
			}
		}
	}
	//al no colisionar podem deixar la BB com a la de seguent pos.
	return false;
}

/*
* Executes the actions sent by the user by means of the keyboard and mouse
*/
void Game::CheckShoot() {
	for (int x = 0; x < _Enemies.size(); x++) {
		if (_Enemies[x].getShoot()) {
			//aix� voldr� dir que el enemic ha de disparar, per tant ja veure com ho fem
			for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
				if (_gameElements.getGameElement(i)._ID == _Enemies[x].GetID()) {
					_gameElements.BulletEnemy(_Enemies[x].GetID());
					_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._translate = _gameElements.getGameElement(i)._translate + glm::vec3(0.0f, 0.0f, 0.15f);
					_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._angle = _Enemies[x].getAngleBOTTOM();
					_Enemies[x].setShoot(false);
					return;
				}
			}
		}
	}
}

void Game::SpawnEnemy(int ID, int X, int Y){
	TankEnemy Tank;
	glm::vec3 _pos = glm::vec3(ConvertGridToPositionX(glm::abs(X)), ConvertGridToPositionY(glm::abs(Y)), 0.0f);
	Tank.InitTankEnemy(_pos, _pos, 0, 0, 5, 0, X, Y);
	_gameElements.SpawnEnemy(_pos, ID);
	Tank.SetID(ID);
	_Enemies.push_back(Tank);
}

void Game::InitTurn(){
	if (!isinTurn) {		
		TurnMovement = true;
		isinTurn = true;
		GameTurn = std::clock();
	}
}

void Game::addPlayer(NetworkInfo tempInfo){
	for (int i = 0;i < _gameElements.getNumGameElements();i++){
		if (_gameElements.getGameElement(i)._ID == tempInfo._ID){
			_gameElements.getGameElement(i)._translate =
				(glm::vec3(ConvertGridToPositionX(tempInfo._movement[0].getPosX()),
					ConvertGridToPositionY(tempInfo._movement[0].getPosY()),
					0.0f));
			_recievedMessages->pop_front();
			return;
		}
	}
	SpawnEnemy(tempInfo._ID, tempInfo._movement[0].getPosX(), tempInfo._movement[0].getPosY());
	_recievedMessages->pop_front();
}

void Game::EndGame(){
	if (TurnMovement) {
		if (std::clock() - GameTurn > Time) {
			//aqui canviariem a attack
			TurnMovement = false;
			GameTurn = std::clock();
			TurnAttack = true;
		}
	}
	else if (TurnAttack) {
		if (std::clock() - GameTurn > Time) {
			//aqui ja acabariem i enviariem dades
			isinTurn = false;
			TurnAttack = false;
		}
	}
}

void Game::UpdateEnemyMovements(){
	//moviment del objecte
	int position = 0;
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		if (_gameElements.getGameElement(i)._objectType == 6) {
			_gameElements.getGameElement(i)._angle = _Enemies[position].getAngleBOTTOM();
			_gameElements.getGameElement(i)._rotation.z = 1.0f;
			_gameElements.getGameElement(i)._translate = _Enemies[position].getPositionTOP();
			position++;
		}
	}
}

int Game::ConvertPositionToGridX(float _X){
	return glm::floor(_X*2);;
}

float Game::ConvertGridToPositionY(float _X){
	return (_X / 2) + 0.25f;
}

float Game::ConvertGridToPositionX(float _X){
		return -(_X / 2) - 0.25f;
}

/*
* Update the game objects based on the physics
*/
void Game::doPhysics() {
	//for que fa moure totes les bales, que tenen el type red_cube, les bales dels enemics s�n mes rapides que les del jugador, shit happens
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		if (_gameElements.getGameElement(i)._objectType == RED_CUBE) {
			glm::vec3 move = glm::vec3(glm::sin(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f, -glm::cos(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f, 0.0f);
			if (!collisionBala(move, i)) {
				_gameElements.getGameElement(i)._translate.x += glm::sin(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f;
				_gameElements.getGameElement(i)._translate.y -= glm::cos(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f;
			}
		}
	}
}

Geometry* Game::GetGeometry(){
	return &_gameElements;
}

std::string Game::GetName(){
	return _windowTitle;
}

int Game::getEnemyPosthroughID(int _ID){
	for (int i = 0;i < _Enemies.size();i++){
		if (_Enemies[i].GetID() == _ID) return i;
	}
}