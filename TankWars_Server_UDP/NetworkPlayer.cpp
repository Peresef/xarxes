#include "NetworkPlayer.h"



unsigned short NetworkPlayer::getPort(){
	return _port;
}

sf::IpAddress NetworkPlayer::getIp(){
	return _ip;
}

int NetworkPlayer::getID(){
	return _ID;
}

void NetworkPlayer::setPort(unsigned short port){
	_port = port;
}

void NetworkPlayer::setIP(sf::IpAddress ip){
	_ip = ip;
}

void NetworkPlayer::setID(int id){
	_ID = id;
}

void NetworkPlayer::setOwnershipRoom(bool state){
	_owner = state;
}

bool NetworkPlayer::getOwnershipRoom(){
	return _owner;
}

NetworkPlayer::NetworkPlayer(){
	_owner = false;
}


NetworkPlayer::~NetworkPlayer(){
}
