#pragma once
#include <vector>
#include <map>
#include "Game.h"
#include "NetworkInfo.h"
#include "GameConstants.h"

//Manages the rooms in the server
class GameManager{
public:
	GameManager(std::deque<NetworkInfo>* recievedMessages,
		std::deque<NetworkInfo>* sendMessages,
		std::mutex* lockqueueReceive,
		std::mutex* lockqueueSend);
	~GameManager();

	void update();
	void startGame(int IDGame);
	bool createGame(std::string Name, double _time);
	void destroyGame(int IDGame);
	std::vector<TankEnemy>* GetEnemies(int IDGame);
	Geometry* GetGeometry(int IDGame);
	std::string GetName(int IDGame);
	int GetNumberGames();

private:
	std::vector<std::deque<NetworkInfo>*> _gameQueIn;
	std::vector<std::deque<NetworkInfo>*> _gameQueOut;
	std::vector<Game*> _game;
	std::deque<NetworkInfo>* _recievedMessages;
	std::deque<NetworkInfo>* _sendMessages;

	std::mutex* _lockqueueReceive;
	std::mutex* _lockqueueSend;
	std::map<int, bool> _GameReady;
	void checkQueues();
	int IDGame;
};