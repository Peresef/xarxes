#pragma once
#include<string>
#include <vector>
#include <istream>
#include "Cell.h"
#include "GameConstants.h"

class NetworkInfo{
public:
	NetworkInfo();
	~NetworkInfo();
	char* _missatge;
	size_t _missatgeSize;
	int _ID;
	int _TYPE;
	bool _shoot;
	int _angle;
	std::vector<Cell> _movement;
	bool _canShoot;
	bool _movementAllowed;
	float _posX;
	float _posY;
	float _posXinicial;
	float _posYinicial;
	int _idMove;
	bool _winner;
	int _score;
	bool _critical;
	bool _ack;
	std::string _chat;
	//times to resend the critical message
	int _life;
	int IDGame;
	int _WHO_TO_SEND_ID;
	std::vector<int> Inputs;
	int _playersReady;

	bool _die;
	bool shouldISend();
	bool shouldIDelete();
	bool shouldDeleteClient();
private:

};

