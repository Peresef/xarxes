#include "UserManager.h"



UserManager::UserManager(std::deque<NetworkInfo>* recievedMessages,
	std::deque<NetworkInfo>* sendMessages,
	std::mutex* lockqueueReceive,
	std::mutex* lockqueueSend):
	_Games(recievedMessages,
		sendMessages,
		lockqueueReceive,
		lockqueueSend){
	_ID = 1;
	_IDGame = 0;
}


UserManager::~UserManager(){
	for (int i = 0;i < _users.size();i++){
		if (_users[i] != 0)
			delete _users[i];
	}
	for (int i = 0;i < _usersInAGame.size();i++){
		if (_usersInAGame[i] != 0)
			delete _usersInAGame[i];
	}
}

bool UserManager::addUser(sf::IpAddress IP, unsigned short Port){
	if (Port != 0){
		for (int i = 0; i < _users.size(); i++) {
			if (_users[i]->getPort() == Port) {
				if (_users[i]->getIp() == IP) {
					return false;
				}
			}
		}

		NetworkPlayer* tempPlayer = new NetworkPlayer;
		tempPlayer->setID(_ID);
		tempPlayer->setIP(IP);
		tempPlayer->setPort(Port);

		_ID++;
		_users.push_back(tempPlayer);
		return true;
	}
	else return false;
}

void UserManager::printUsers(){
	for (int i = 0;i < _users.size();i++){
		std::cout << _users[i]->getID() << std::endl;
	}
}

void UserManager::deleteUser(int ID){
	for (int i = 0; i < _users.size(); i++) {
		if (_users[i]->getID() == ID) {
			for (int j = 0;j < _usersInAGame[getUserRoom(ID)]->size();j++){
				if (_usersInAGame[getUserRoom(ID)]->at(j)->getID() == ID){
					_usersInAGame[getUserRoom(ID)]->erase(_usersInAGame[getUserRoom(ID)]->begin() + j);
					break;
				}
			}
			delete _users[i];
			_users.erase(_users.begin() + i);
		}
	}
}

NetworkPlayer* UserManager::getUser(int ID){
	for (int i = 0; i < _users.size(); i++) {
		if (_users[i]->getID() == ID) {
			return _users[i];
		}
	}
	return nullptr;
}

NetworkPlayer * UserManager::getUserThroughPos(int pos){
	return _users[pos];
}

NetworkPlayer* UserManager::getUser(sf::IpAddress IP, unsigned short Port){
	for (int i = 0; i < _users.size(); i++) {
		if (_users[i]->getIp() == IP) {
			if (_users[i]->getPort() == Port){
				return _users[i];
			}
		}
	}
	return nullptr;
}

bool UserManager::UserJoinsGame(int ID, int IDGame){
	if (IDGame > MAXGAMES) return false;
	if (_maxPlayers[IDGame] > _usersInAGame[IDGame]->size()){
		int positionOfUser = 0;;
		for (int i = 0; i < _users.size(); i++) {
			if (_users[i]->getID() == ID) {
				positionOfUser = i;
			}
		}
		_usersInAGame[IDGame]->push_back(_users[positionOfUser]);
		return true;
	}
	else return false;
}

bool UserManager::createRoom(int ID, std::string mssg, double _time){
	bool state = _Games.createGame(mssg, _time);
	if (state){
		std::vector<NetworkPlayer*>* tempUsers = new std::vector<NetworkPlayer*>;
		_usersInAGame.push_back(tempUsers);
		_maxPlayers[_IDGame] = 2;
		int positionOfUser = 0;;
		for (int i = 0; i < _users.size(); i++) {
			if (_users[i]->getID() == ID) {
				positionOfUser = i;
				_users[i]->setOwnershipRoom(true);
			}
		}
		_usersInAGame[_IDGame]->push_back(_users[positionOfUser]);
		_IDGame++;
		return true;
	}
	else return false;
}

void UserManager::deleteRoom(int IDGame){
	_Games.destroyGame(IDGame);
	_usersInAGame.erase(_usersInAGame.begin() + IDGame);
}

int UserManager::getNumberUsers(){
	return _users.size();
}

int UserManager::getNumberUsersInRoom(int IDGame){
	return _usersInAGame[IDGame]->size();
}

int UserManager::getMaxUsersInRoom(int IDGame){
	return _maxPlayers[IDGame];
}
std::vector<NetworkPlayer*>* UserManager::getPlayersInRoom(int IDGame){
	return _usersInAGame[IDGame];
}

int UserManager::getUserRoom(int ID){
	for (int i = 0;i < _usersInAGame.size();i++){
		for (int j = 0;j < _usersInAGame[i]->size();j++){
			if (_usersInAGame[i]->at(j)->getID() == ID)
				return i;
		}
	}
	return 99;
}

std::vector<TankEnemy>* UserManager::GetEnemies(int IDGame){
	return _Games.GetEnemies(IDGame);
}

Geometry* UserManager::GetGeometry(int IDGame){
	return _Games.GetGeometry(IDGame);
}

std::string UserManager::GetName(int IDGame){
	return _Games.GetName(IDGame);
}

int UserManager::GetNumberGames(){
	return _Games.GetNumberGames();
}

void UserManager::startGame(int IDGame){
	_Games.startGame(IDGame);
}

void UserManager::update(){
	_Games.update();
}

bool UserManager::isRoomFull(int IDGame){
	if (_maxPlayers[IDGame] <= _usersInAGame[IDGame]->size())return true;
	else return false;
}

NetworkPlayer* UserManager::getRoomOwner(int IDGame){
	for (int i = 0;i < _usersInAGame[IDGame]->size();i++){
		if (_usersInAGame[IDGame]->at(i)->getOwnershipRoom())return _usersInAGame[IDGame]->at(i);
	}
	return nullptr;
}