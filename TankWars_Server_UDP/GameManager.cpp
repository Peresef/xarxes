#include "GameManager.h"

GameManager::GameManager(std::deque<NetworkInfo>* recievedMessages,
	std::deque<NetworkInfo>* sendMessages,
	std::mutex* lockqueueReceive,
	std::mutex* lockqueueSend){
	_recievedMessages = recievedMessages;
	_sendMessages = sendMessages;
	_lockqueueReceive = lockqueueReceive;
	_lockqueueSend = lockqueueSend;
	IDGame = 0;
	for (int i = 0;i < MAXGAMES;i++){
		_GameReady[i] = false;
	}
}

GameManager::~GameManager(){
	for (int i = 0;i < _game.size();i++){
		if(_game[i]!=0)
		delete _game[i];
	}
	for (int i = 0;i < _gameQueIn.size();i++){
		if (_gameQueIn[i] != 0)
			delete _gameQueIn[i];
	}
	for (int i = 0;i < _gameQueOut.size();i++){
		if (_gameQueOut[i] != 0)
			delete _gameQueOut[i];
	}
}

void GameManager::update(){
	checkQueues();
	for (int i = 0;i < _game.size();i++){
		if (_GameReady[i]){
			_game[i]->update();
		}
	}
}

void GameManager::startGame(int IDGame){
	_game[IDGame]->startGame();
}

bool GameManager::createGame(std::string Name, double _time){
	if (_game.size() < MAXGAMES){
		std::deque<NetworkInfo>* tempQue= new std::deque<NetworkInfo>;
		std::deque<NetworkInfo>* tempQue2 = new std::deque<NetworkInfo>;
		Game* tempGame = new Game(Name, 700, 700, true, 60, false, tempQue,
			tempQue2, _time);
		_game.push_back(tempGame);
		_gameQueIn.push_back(tempQue);
		_gameQueOut.push_back(tempQue2);
		_GameReady[IDGame] = true;
		IDGame++;
		return true;
	}
	else return false;
}

void GameManager::destroyGame(int IDGame){
	delete _game[IDGame];
	_game.erase(_game.begin() + IDGame);
	delete _gameQueIn[IDGame];
	_gameQueIn.erase(_gameQueIn.begin() + IDGame);
	delete _gameQueOut[IDGame];
	_gameQueOut.erase(_gameQueOut.begin() + IDGame);
}

void GameManager::checkQueues(){
	if (_gameQueOut.size() > 0){
		std::vector<NetworkInfo> messages_to_sort;
		int queueSize = _recievedMessages->size();
		for (int i = 0;i < queueSize;i++){
			NetworkInfo tempInfo = _recievedMessages->front();
			messages_to_sort.push_back(tempInfo);
			_recievedMessages->pop_front();
		}
		for (int i = 0;i < messages_to_sort.size();i++){
			_gameQueIn[messages_to_sort[i].IDGame]->push_back(messages_to_sort[i]);
		}
		messages_to_sort.clear();
		
		std::vector<int> _queueSize;
		for (int i = 0;i < _gameQueOut.size();i++){
			_queueSize.push_back(_gameQueOut[i]->size());
		}

		for (int j = 0;j < _gameQueOut.size();j++){
			for (int i = 0;i < _queueSize[j];i++){
				NetworkInfo tempInfo = _gameQueOut[j]->front();
				tempInfo.IDGame = j;
				messages_to_sort.push_back(tempInfo);
				_gameQueOut[j]->pop_front();
			}
		}
		for (int i = 0;i < messages_to_sort.size();i++){
			_sendMessages->push_back(messages_to_sort[i]);
		}
	}
}

std::vector<TankEnemy>* GameManager::GetEnemies(int IDGame){
	return _game[IDGame]->getEnemies();
}

Geometry* GameManager::GetGeometry(int IDGame){
	return _game[IDGame]->GetGeometry();
}

std::string GameManager::GetName(int IDGame){
	return _game[IDGame]->GetName();
}

int GameManager::GetNumberGames(){
	return _game.size();
}
