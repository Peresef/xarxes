#pragma once
//Third-party libraries
#include <GL/glew.h>			//The OpenGL Extension Wrangler
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <queue>
#include <mutex>
#include <ctime>

#include "FPSLimiter.h"
#include "Vertex.h"
#include "Geometry.h"
#include "GameConstants.h"
#include "Tank.h"
#include "Timestep.h"
#include "Enemy.h"
#include "TankEnemy.h"
#include "NetworkInfo.h"

//Game has four possible states: INIT (Preparing environment), PLAY (Playing), EXIT (Exit from the game) or MENU (Game menu)
enum class GameState{INIT, PLAY, EXIT, MENU};

//This class manages the game execution
class Game {
	public:						
		Game(std::string windowTitle, int screenWidth, int screenHeight,
			bool enableLimiterFPS, int maxFPS, bool printFPS,
			std::deque<NetworkInfo>* recievedMessages,
			std::deque<NetworkInfo>* sendMessages, double _time);	//Constructor
		~Game();					//Destructor

		void update();
		std::vector<TankEnemy>* getEnemies();
		Geometry* GetGeometry();
		std::string GetName();
		int getEnemyPosthroughID(int _ID);
		void startGame();
	private:
			//Attributes turn
		std::clock_t GameTurn;
		bool isinTurn;//per saber si esta en mig d'un torn
		bool TurnMovement; //per saber si esta en el de moviment
		bool TurnAttack; // Per saber si esta en el d'atac
		bool Shooted;

		std::clock_t shottime;
		float Time;
		int score;
		std::clock_t spawntime;
		std::string _windowTitle;		//Window Title

		float _angleBALA;
		int _screenWidth;				//Screen width in pixels				
		int _screenHeight;				//Screen height in pixels				
		GameState _gameState;			//It describes the game state				
		FPSLimiter _fpsLimiter;			//Manage the synchronization between frame rate and refresh rate
		Geometry _gameElements;			//Manage the game elements
		Timestep _tp;

		std::deque<NetworkInfo>* _recievedMessages;
		std::deque<NetworkInfo>* _sendMessages;

		bool _startSession;
		bool sessionTimer;
		clock_t _startTimeSession;
		double _secondsPassedSession;
		double _secondsToDelaySession;

		bool _gameStart;
		
		//EnemyTank
		std::vector<TankEnemy> _Enemies;

			//Internal methods
		void initSystems();
		void checkBounds(NetworkInfo tempInfo);
		void doPhysics();
		bool collisionBala(glm::vec3 movement, int i);
		void CheckShoot();
		void moveTank();
		void SpawnEnemy(int ID, int X, int Y);
		void EndGame();
		void AddScore(int _i);
		void UpdateEnemyMovements();
		void CheckCell();
		int ConvertPositionToGridX(float _X);
		float ConvertGridToPositionY(float _X);
		float ConvertGridToPositionX(float _X);
		void InitTurn();
		void addPlayer(NetworkInfo info);
		void run();					//Game execution
};

