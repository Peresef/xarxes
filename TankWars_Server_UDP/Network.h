#pragma once
#include<string>
#include <vector>
#include <istream>
#include <queue>
#include <mutex>
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <iostream>
#include <list>
#include <time.h>
#include <algorithm>
#include "Cell.h"
#include "NetworkPlayer.h"
#include "NetworkInfo.h"
#include "TankEnemy.h"
#include "Geometry.h"
#include "GameConstants.h"
#include <Windows.h>
#include "OutputMemoryBitStream.h"
#include "InputMemoryBitStream.h"
#include "TankEnemy.h"
#include "UserManager.h"
#include <mysql_connection.h>
#include <cppconn\driver.h>
#include <cppconn\resultset.h>
#include <cppconn\statement.h>
#include <cppconn\exception.h>

class Network{
public:

	Network();
	~Network();
	void loop();

private:
	bool run;
	UserManager _users;
	//creem un socket per on entraran les conexion clients
	sf::UdpSocket socketClients;

	//timers per als resends de missatges critics
	std::clock_t resendTime;
	std::clock_t resendTimePassed;

	std::clock_t moveTimePassed;

	std::map<int, bool> _GamesStarted;
	//timers per als pings
	std::clock_t pingtime;
	std::clock_t pingTimepassed;

	sf::Thread listening;
	bool _exit;

	//cues de missatges de entrada i sortida entra els moduls de simulació i de network
	std::deque<NetworkInfo> _recievedMessages;
	std::deque<NetworkInfo> _sendMessages;
	//vector on es guarden els missatges critics
	std::vector<NetworkInfo> CriticalMessages;

	std::mutex _lockqueueReceive;
	std::mutex _lockqueueSend;
	std::mutex _lockqueueCritical;
	//punter al vector enemies del game per a poder fer le ping facilment
	std::vector<TankEnemy>* _enemies;
	Geometry* _gameElements;
	std::vector<NetworkInfo> _playersInGameReady;

	//Decodifica el missatge que rep dels clients i ho afegeix a la cua de missatges que tractar pel game
	void Decode(InputMemoryBitStream data, sf::IpAddress _rIP, unsigned short _rPort);
	//Retorna un String amb les dades a enviar segons el cas que sigui
	void Code(int _case, NetworkInfo tempInfo, OutputMemoryBitStream& ombs);
	bool send(int whotosend, char* message, size_t size);
	void Clear();
	void AssignPos(int IDGame, int ID);
	void Receive();
	void addTank(NetworkInfo tempInfo, int IDGame);
	void SpawnEnemy(int ID, int X, int Y, int IDGame);
	float ConvertGridToPositionY(float _X);
	float ConvertGridToPositionX(float _X);
	void startGame(int IDGame);
	//per a fer la desconexió quan algu es desconecta
	void endGame(int IDGame);
	void sendMoves(int IDGame);
	void sendPing(int IDGame);
	void SendShoots(int IDGame);
	void CheckCriticals();
	void CheckQueue();
	bool CheckUserPass(std::string Name, std::string Pass);//funció que checkeja que l'usuari i la contrasenya rebudes siguin correctes
	void AddGameSQL(std::string Name);
	//para el joc per a temps de sessio la cual es controla al gameloop
	void endGame(int winner, int IDGame);
};

