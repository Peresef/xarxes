#pragma once
#include "GameConstants.h"
#include <SFML\Network.hpp>
#include <SFML\Graphics.hpp>

class NetworkPlayer{
private:
	int _ID;
	unsigned short _port;
	sf::IpAddress _ip;
	bool _owner;
public:
	unsigned short getPort();
	sf::IpAddress getIp();
	int getID();

	void setPort(unsigned short port);
	void setIP(sf::IpAddress ip);
	void setID(int id);

	void setOwnershipRoom(bool state);

	bool getOwnershipRoom();

	NetworkPlayer();
	~NetworkPlayer();
};

