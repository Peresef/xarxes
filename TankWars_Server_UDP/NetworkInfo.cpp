#include "NetworkInfo.h"



NetworkInfo::NetworkInfo(){
	_life = 0;
	_die = false;
}


NetworkInfo::~NetworkInfo(){
}

bool NetworkInfo::shouldISend(){
	if (_ack)return false;
	if (_critical){
		if (_life > 0){
			_life--;
			_die = false;
			return true;
		}
		else{
			_die = true;
			return false;
		}
	}
	_die = false;
	return false;
}

bool NetworkInfo::shouldIDelete(){
	if (_die) return true;
	else {
		if (_ack){
			return true;
		}
		return false;
	}
}

bool NetworkInfo::shouldDeleteClient(){
	if (_TYPE == PING) {
		if (!_ack && _life <= 0)return true;
	}
	return false;
}
