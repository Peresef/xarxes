#pragma once
#include <vector>
#include <iostream>
#include "NetworkPlayer.h"
#include "NetworkInfo.h"
#include "GameManager.h"

class UserManager{
public:
	UserManager(std::deque<NetworkInfo>* recievedMessages,
		std::deque<NetworkInfo>* sendMessages,
		std::mutex* lockqueueReceive,
		std::mutex* lockqueueSend);
	~UserManager();
	bool addUser(sf::IpAddress _IP, unsigned short _Port);
	void printUsers();
	void deleteUser(int ID);
	NetworkPlayer* getUser(int _ID);
	NetworkPlayer* getUserThroughPos(int pos);
	NetworkPlayer* getUser(sf::IpAddress _IP, unsigned short _Port);

	bool UserJoinsGame(int _ID, int IDGame);
	bool createRoom(int ID, std::string mssg, double _time);
	void deleteRoom(int IDGame);
	
	//returns the number of users connected to the server
	int getNumberUsers();
	//returns the number of actual players in the room
	int getNumberUsersInRoom(int IDGame);
	//returns the max number of users in the room
	int getMaxUsersInRoom(int IDGame);
	//returns a pointer to the players in that room
	std::vector<NetworkPlayer*>* getPlayersInRoom(int IDGame);
	//returns in which room the player is
	int getUserRoom(int ID);

	std::vector<TankEnemy>* GetEnemies(int IDGame);
	Geometry* GetGeometry(int IDGame);
	std::string GetName(int IDGame);
	int GetNumberGames();
	void startGame(int IDGame);
	void update();
	bool isRoomFull(int IDGame);
	NetworkPlayer * getRoomOwner(int IDgame);
private:
	GameManager _Games;
	int _ID;
	int _IDGame;
	std::vector<NetworkPlayer*> _users;
	std::vector<std::vector<NetworkPlayer*>*> _usersInAGame;
	std::map<int, int> _maxPlayers;
};

