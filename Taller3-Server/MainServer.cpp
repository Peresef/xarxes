#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>
#include <list>

#define MAX_MENSAJES 30
#define MAX_BYTES 1300


int main() {
	sf::TcpSocket socket;
	bool notFinishedSending = false;
	
	//creem un listener
	sf::TcpListener listener;
	listener.listen(55001);

	//creem un vector de clients
	std::list<sf::TcpSocket*> clients;

	//creem el selector
	sf::SocketSelector selector;

	//afegim el listener
	selector.add(listener);

	std::string mensaje = " ";
	std::string splitmensaje;
	std::size_t BytesSent;

	sf::IpAddress ip = sf::IpAddress::getLocalAddress();
	bool exit = false;
	while(!exit) {
		//Acceptar conexions i guardarles
		//rebre missatges dels clients i reenviar
		//Revisar les conexions tancades i alliberades

		//fem que el selector esperi coses entrants
		if (selector.wait()) {
			if (selector.isReady(listener)) {
				sf::TcpSocket* client = new sf::TcpSocket;
				if (listener.accept(*client) == sf::Socket::Done) {
					clients.push_back(client);
					selector.add(*client);
					std::cout << "Cliente conectado" << std::endl;
				}
				else {
					delete client;
				}
			}
			else {		
				// The listener socket is not ready, test all other sockets (the clients)
				for (std::list<sf::TcpSocket*>::iterator it = clients.begin(); it != clients.end();) {
					sf::TcpSocket& client = **it;
					if (selector.isReady(client)) {
						char data[MAX_BYTES];
						std::size_t received;
						memset(data, 0, sizeof data);
						sf::Socket::Status stateR;
						stateR = client.receive(data, MAX_BYTES, received);
						switch (stateR){
						case sf::Socket::Done:
						{
							std::string DataStr(data);
							sf::Socket::Status state;
							for (std::list<sf::TcpSocket*>::iterator it2 = clients.begin(); it2 != clients.end();) {
								sf::TcpSocket& clientS = **it2;
								std::string DataStr2;
								if (it == it2) DataStr2 = ">" + DataStr;
								else DataStr2 = "<" + DataStr;
								state = clientS.send(DataStr2.c_str(), DataStr2.size(), BytesSent);
								switch (state) {
								case sf::Socket::Done:
									notFinishedSending = false;
									break;
								case sf::Socket::NotReady:
									break;
								case sf::Socket::Partial:
									std::cout << "S'est� enviant el misstage" << std::endl;
									notFinishedSending = true;
									splitmensaje = DataStr2;
									while (notFinishedSending) {
										sf::Socket::Status state;
										splitmensaje = splitmensaje.substr(BytesSent, DataStr2.size()-BytesSent);
										state = clientS.send(splitmensaje.c_str(), splitmensaje.size(), BytesSent);
										switch (state) {
										case sf::Socket::Done:
											notFinishedSending = true;
											break;
										case sf::Socket::NotReady:
											notFinishedSending = false;
											break;
										case sf::Socket::Partial:
											break;
										case sf::Socket::Disconnected:
											notFinishedSending = false;
											std::cout << "Desconectado" << std::endl;
											selector.remove(clientS);
											clientS.disconnect();
											it2=clients.erase(it2);
											delete &clientS;
											break;
										case sf::Socket::Error:
											notFinishedSending = false;
											std::cout << "Error al enviar" << std::endl;
											break;
										default:
											break;
										}
									}
									break;
								}
								if (it2 != clients.end())++it2;
							}
						}
							break;
						case sf::Socket::NotReady:
							break;
						case sf::Socket::Partial:
							break;
						case sf::Socket::Disconnected:
							selector.remove(client);
							client.disconnect();
							it=clients.erase(it);
							break;
						case sf::Socket::Error:
							break;
						default:
							break;
						}
					}
					if(it!=clients.end())++it;
				}
			}
		}
	}
	selector.clear();
	clients.clear();
	listener.close();
}