#pragma once
#include "Tank.h"
#include <iostream>
class TankEnemy :
	public Tank{

private:

	int NextPosX;//no crec que faci falta pero ho fico
	int NextPosY;//no crec que faci falta pero ho fico
	int MovX;//Per saber si s'ha de moure en X o no;
	int MovY;//per saber si s'ha de moure en Y o no;
	int ActCell;
	int ProxAngle;//proxim angle al qual s'haura de girar

	bool Turn;

	//dades que enviara el servidor
	bool WillShoot;
	int AngleServer;

	bool RotateAttack;
	bool Move;
	bool isRotating;
	bool isMoving;
	bool Shoot;//aix� vol dir que el game detectara que es pot disparar i activara la funcio d'instanciar la bala ^^
public:
	//fa el moviment envers un vector de cel�les 
	//A mode d'anotacio per tenir clar com es fara, en el game anira passant el updatemovement un cop se li donguin la senyal que s'ha de moure's, llavors 
	//s'anira checkejant si s'esta en la cel�la i si s'esta doncs pam canviem d'objectiu.
	//Per altre banda lo de automatic movement assigna a tempscells les cel�les que s'ha de moure.
	//S'ha de dir que aix� es el concepte, ja ho desenvolupare quan em surti del rabo
	void AutomaticMovement(std::vector<Cell> cells);
	bool CheckIsinCell(); //funci� que anira 
	void UpdateMovement();//posicio del mon i si s'ha arribat
	void LookAt();//funcio que determinara cap a on girar-se
	void UpdateAngleLookAt();
	void InitMovement(std::vector<Cell> Cells, bool shoot, int angle);
	void InitRotateAttack();
	void ResetVariables();
	glm::vec2 GoToinit();
	bool getShoot();
	void setShoot(bool boolea);
	bool getTurn();
	int getNextX();
	int getNextY();

	void InitTankEnemy(glm::vec3 positiontop, glm::vec3 positionbottom, float angleTOP, float angleBOTTOM, int health, int delay, int _ActPosX, int _ActPosY);
	TankEnemy();
	~TankEnemy();

	int score;

	bool moveAllowed;
	bool shootAllowed;
	bool isDone;
};

