#pragma once

//Player Tank objects

#define TOP_TANK 6
#define BOTTOM_TANK 6
#define BULLETOBJECT 7

//Type Lights
#define DIRLIGHT 0
#define POINTLIGHT 1
#define SPOTLIGHT 2

//Type Messages
#define	RECEIVED 0
#define STARTIMER 1
#define STOPTIMER 2
#define	ANSWERCLIENT 3
#define	PROPAGATECLIENTS 4
#define ENDGAME_WIN 5
#define ENDGAME_LOSE 6
#define DISCONNECTED 7

#define MAX_MENSAJES 30
#define MAX_BYTES 1300

#define MAX_CLIENTS 2
#define MAX_TIME_TURN 8
#define MIN_TIME_BETWEEN_SENDS 200
#define TIME_SESSION 300

#define DEBUG_RENDER_SERVER true