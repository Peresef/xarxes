#pragma once
#include <iostream>

class Cell{
private:
	int _PosX;
	int _PosY;
public:
	void setCellPosition(int posx, int posy);
	int getPosX();
	int getPosY();
	Cell();
	~Cell();

	friend std::ostream& operator<< (std::ostream& os, const Cell& p);
	friend std::istream& operator>> (std::istream & is, Cell & p);
};

