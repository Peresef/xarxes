//#pragma once
#include "Vertex.h"
#include <vector>
#include "GameObject.h"
#include <fstream>
#include "ObjectLoader.h"
#include "TextureManager.h"
#include "GameConstants.h"

#define BLUE_CUBE 0
#define RED_CUBE 1
#define WHITE_CUBE 2
#define PLANE 3
#define BULLET 6
#define NUMBASICOBJECTS 5
#define MAXVERTICESPYRAMID 18
#define MAXVERTICESSQUARE 36

//This class stores and manipulates all the objects loaded from the text file
class Geometry{
	std::vector<Vertex *> _verticesData;
	std::vector<int> _numVertices;
	std::vector <GameObject> _listOfObjects;
	ObjectLoader _loader;
	GameObject bullet;


public:
	Geometry();
	~Geometry();
	void loadGameElements(char fileName[100]);
	void findingBB();
	void findingBB(int i);
	Vertex * getData(int objectID);
	int getNumVertices(int objectID);
	int getNumGameElements();
	GameObject & getGameElement(int objectID);
	void removeLastElement(int id);
	void removeLastElement();
	void createCube(int ID, glm::vec3 RGB);
	void LoadASE();
	void createPyramid(int ID, glm::vec3 RGB);
	void Bullet(TextureManager &_T);
	void BulletEnemy(TextureManager & _T, int ID);
	//funcio que fare per a disparar una bala i que crei un objecte temporal
	void ChangeColor(glm::vec3 color, int ID);
	void SpawnEnemy(glm::vec3 translate, TextureManager &_T, int ID);
	void createPlane(int i, glm::vec3 RGB);
	void UpdateBB();
	bool collision(glm::vec3 movement, int position);
};

