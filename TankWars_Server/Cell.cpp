#include "Cell.h"



void Cell::setCellPosition(int posx, int posy){
	_PosX = posx;
	_PosY = posy;
}

int Cell::getPosX(){
	return _PosX;
}

int Cell::getPosY(){
	return _PosY;
}

Cell::Cell(){
}


Cell::~Cell(){
}

std::ostream & operator<<(std::ostream & os, const Cell & p){
	os << "Pos X: " << p._PosX << " Pos Y: " << p._PosY << " " <<std::endl;
	return os;
}
std::istream & operator>>(std::istream & is, Cell & p){
	is >> p._PosX;
	is >> p._PosY;
	return is;
}
