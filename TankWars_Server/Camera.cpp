#include "Camera.h"



Camera::Camera(){
}

Camera::Camera(int _screenwidth, int _screenHeight) :
	_near(0.1f),
	_far(50.0f),
	_projectionWidth(30.0f),
	_projectionHeight(30.0f),
	_cameraPos(5.0f, 0.0f, 0.0f),
	_cameraFront(0.0f, 0.0f, 0.0f),
	_cameraUp(0.0f, 0.0f, 1.0f),
	_FOV(45.0f),
	_aspecRatio((float)_screenwidth / (float)_screenHeight) {
	glm::vec3 cameraDirection = glm::normalize(_cameraPos - _cameraFront);
	glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));
	_cameraUp = glm::cross(cameraDirection, cameraRight);
	_viewMatrix = glm::lookAt(_cameraPos, _cameraFront, _cameraUp);
}

void Camera::init(int _screenwidth, int _screenHeight){
	_near = 0.1f;
	_far=30.0f;
	_projectionWidth = 30.0f;
	_projectionHeight =30.0f;
	_cameraPos = glm::vec3(10.0f, 0.0f, 0.0f);
	_cameraFront = glm::vec3(0.0f, 0.0f, 0.0f);
	_cameraUp = glm::vec3(0.0f, 0.0f, 1.0f);
	_FOV = 45.0f;
	_aspecRatio=((float)_screenwidth / (float)_screenHeight);
	glm::vec3 cameraDirection = glm::normalize(_cameraPos - _cameraFront);
	glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));
	_cameraUp = glm::cross(cameraDirection, cameraRight);
	_viewMatrix = glm::lookAt(_cameraPos, _cameraFront, _cameraUp);
}


Camera::~Camera(){
}

void Camera::perpespectiveMatrix(){
	_projectionMatrix = glm::perspective(_FOV, _aspecRatio, _near, _far);
}

void Camera::orthographic() {
	_projectionMatrix = glm::ortho(-_projectionWidth / 2, _projectionWidth / 2, -_projectionHeight / 2, _projectionHeight / 2, _near, _far);
}

void Camera::changePos(glm::vec3 cameraPos, glm::vec3 cameraFront){
	_cameraPos = cameraPos;
	_cameraFront = cameraFront;
	glm::vec3 cameraDirection = glm::normalize(_cameraPos - _cameraFront);
	glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));
	_cameraUp = glm::cross(cameraDirection, cameraRight);
	_viewMatrix = glm::lookAt(_cameraPos, _cameraFront, _cameraUp);
}

glm::vec3 Camera::getCamPos(){
	return _cameraPos;
}


