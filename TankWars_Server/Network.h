#pragma once
#include<string>
#include <vector>
#include <istream>
#include <queue>
#include <mutex>
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <iostream>
#include <list>
#include <time.h>
#include "Cell.h"
#include "NetworkInfo.h"
#include "GameConstants.h"
#include <Windows.h>

class Network{
public:
	Network(std::queue<NetworkInfo>* recievedMessages,
		std::queue<NetworkInfo>* sendMessages, bool* startGame,
		std::mutex* lockqueueReceive, std::mutex* lockqueueSend);
	~Network();
	void endTurn();
	void endGame(int winner);
	void sendAnswer();

private:
	bool FinishedSending;

	//creem un listener
	sf::TcpListener listener;

	//creem un vector de clients
	std::list<sf::TcpSocket*> clients;

	//creem el selector
	sf::SocketSelector selector;

	std::string mensaje;
	std::string splitmensaje;
	std::size_t BytesSent;
	std::vector<std::pair<sf::TcpSocket*,int>> players;
	sf::IpAddress ip;
	int ID;
	bool roomFull;
	bool _startGame;
	sf::Thread listening;
	bool _exit;
	bool* _StartGame;
	int activePlayer;
	int max_clients;

	std::queue<NetworkInfo>* _recievedMessages;
	std::queue<NetworkInfo>* _sendMessages;
	std::mutex* _lockqueueReceive;
	std::mutex* _lockqueueSend;
	
	//IDPlayer_BoolDisparo_Angle_CelaMov1_CelaMov2_
	void Decode(std::string missatge);
	std::string Code(int _case, NetworkInfo tempInfo);
	bool send(int player, std::string message);
	void Clear();
	void Receive();
	void startGame();
	void startTurn();
	void endGame();
};

