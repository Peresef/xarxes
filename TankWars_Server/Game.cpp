#include "Game.h"

/**
* Constructor
* Note: It uses an initialization list to set the parameters
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS) :
	_windowTitle(windowTitle), 
	_screenWidth(screenWidth), 
	_screenHeight(screenHeight),
	_gameState(GameState::INIT), 
	_fpsLimiter(enableLimiterFPS, maxFPS, printFPS),
	_camera(screenWidth, screenHeight),
	red(&_recievedMessages, &_sendMessages, &_gameStart,
		&lockqueueReceive, &lockqueueSend){
	Shooted = false;
	TurnMovement = false;
	isinTurn = false;
	_turnOn=false;
	_gameStart = false;
	FPCamera = true;
	_angleBALA = 0.0f;
	Time = 2500.0f;
	score = 0;
	Debug = false;
	UpdateEnemy = false;
	sessionTimer = false;
	_startSession = false;
	_secondsToDelay = MAX_TIME_TURN;
	_secondsToDelaySession = TIME_SESSION;
}

/**
* Destructor
*/
Game::~Game(){
	system("pause");
}

/*
* Game execution
*/
void Game::run() {
		//System initializations
	initSystems();
		//Start the game if all the elements are ready
	gameLoop();
}

/*
* Initializes all the game engine components
*/
void Game::initSystems() {
		//Create an Opengl window using SDL
	if (DEBUG_RENDER_SERVER)_window.create(_windowTitle, _screenWidth, _screenHeight, 0);
		//Compile and Link shader
	if (DEBUG_RENDER_SERVER)initShaders();
		//Set up the openGL buffers
	if (DEBUG_RENDER_SERVER)_openGLBuffers.initializeBuffers(_colorProgram);
		//Load the current scenario
	_gameElements.createCube(BLUE_CUBE, glm::vec3(0, 0, 255));
	_gameElements.createCube(RED_CUBE, glm::vec3(255, 0, 0));
	_gameElements.createCube(WHITE_CUBE, glm::vec3(255, 255, 255));
	//_gameElements.createPyramid(GREEN_PYRAMID, glm::vec3(255, 255, 255));
	_gameElements.createPlane(PLANE, glm::vec3(200, 200, 200));
	_gameElements.LoadASE();
	_gameElements.createCube(7, glm::vec3(255, 255, 255));
	_gameElements.loadGameElements("./resources/scene2D_server.txt");
	//Panzer.initTank(_gameElements.getGameElement(TOP_TANK)._translate, _gameElements.getGameElement(BOTTOM_TANK)._translate, _gameElements.getGameElement(TOP_TANK)._angle, _gameElements.getGameElement(BOTTOM_TANK)._angle, 5, 0);
	if (DEBUG_RENDER_SERVER)loadGameTextures();
	if (DEBUG_RENDER_SERVER)_materialManager.createMaterialDefinitions();
	
	//inicialitzem la camera
	_camera.orthographic(); 

	//init SpawnPoints
	SpawnPoint sp;
	sp._point = glm::vec3(-0.25f, 0.25f, 0.0f);
	SpawnPoints.push_back(sp);
	sp._point = glm::vec3(-5.75f, 0.25f, 0.0f);
	SpawnPoints.push_back(sp);
	sp._point = glm::vec3(-0.25f, 5.75f, 0.0f);
	SpawnPoints.push_back(sp);
	sp._point = glm::vec3(-5.75f, 5.75f, 0.0f);
	SpawnPoints.push_back(sp);

	//inicialitzem les llums
	if (DEBUG_RENDER_SERVER)initLights();
	SpawnEnemy();
}

/*
* Initialize the shaders:
* Compiles, sets the variables between C++ and the Shader program and links the shader program
*/
void Game::initShaders() {
		//Compile the shaders
	_colorProgram.addShader(GL_VERTEX_SHADER, "./resources/shaders/vertex-shader.txt");
	_colorProgram.addShader(GL_FRAGMENT_SHADER, "./resources/shaders/fragment-shader.txt");
	_colorProgram.compileShaders();
	
		//Attributes must be added before linking the code
	_colorProgram.addAttribute("vertexPositionGame");
	_colorProgram.addAttribute("vertexColor");
	_colorProgram.addAttribute("vertexUV");
	_colorProgram.addAttribute("vertexNormal");

		//Link the compiled shaders
	_colorProgram.linkShaders();
		//Bind the uniform variables. You must enable shaders before gettting the uniforme variable location
	_colorProgram.use();

	_textureDataLocation = _colorProgram.getUniformLocation("textureData");
	_textureScaleFactorLocation = _colorProgram.getUniformLocation("textureScaleFactor");

	//Set up the uniform material variables
	_materialAmbientUniform = _colorProgram.getUniformLocation("material.ambient");
	_materialDiffuseUniform = _colorProgram.getUniformLocation("material.diffuse");
	_materialSpecularUniform = _colorProgram.getUniformLocation("material.specular");
	_materialShininessUniform = _colorProgram.getUniformLocation("material.shininess");

	_colorProgram.unuse();
}

/*
funcio per a inicialitzar totes les llums
*/
void Game::initLights(){
	material currentMaterial = _materialManager.getMaterialComponents(3);
	Light tempLight;
	tempLight.setPosition(glm::vec3(-0.3f, -0.3f, 0.5f));
	tempLight.setDirection(glm::vec3(0.0f, 0.0f, -1.0f));
	tempLight.setCutOff(25.5f);
	tempLight.setType(DIRLIGHT);
	tempLight.setLinear(0.09f);
	tempLight.setConstant(1.0f);
	tempLight.setQuadratic(0.032f);
	tempLight.setAmbient(glm::vec3(6.0f,6.0f, 6.0f));
	tempLight.setDiffuse(currentMaterial.diffuse);
	tempLight.setSpecular(currentMaterial.specular);
	tempLight.setIsOn(true);

	_lights.push_back(tempLight);
	
	tempLight.setPosition(glm::vec3(-0.7f, -0.7f, 0.5f));
	tempLight.setDirection(glm::vec3(0.0f, -1.0f, -1.0f));
	tempLight.setCutOff(25.5f);
	tempLight.setType(POINTLIGHT);
	tempLight.setLinear(0.09f);
	tempLight.setConstant(1.0f);
	tempLight.setQuadratic(0.032f);
	tempLight.setAmbient(currentMaterial.ambient);
	tempLight.setDiffuse(currentMaterial.diffuse);
	tempLight.setSpecular(currentMaterial.specular);
	tempLight.setIsOn(true);
	_lights.push_back(tempLight);
	
	
	tempLight.setPosition(glm::vec3(-2.4f, 3.0f, 0.5f));
	tempLight.setDirection(glm::vec3(0.0f, 0.0f, -1.0f));
	tempLight.setCutOff(50.0f);
	tempLight.setType(SPOTLIGHT);
	tempLight.setLinear(0.09f);
	tempLight.setConstant(1.0f);
	tempLight.setQuadratic(0.032f);
	tempLight.setAmbient(currentMaterial.ambient);
	tempLight.setDiffuse(currentMaterial.diffuse);
	tempLight.setSpecular(currentMaterial.specular);
	tempLight.setIsOn(true);
	_lights.push_back(tempLight);	
}

/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {	
	_gameState = GameState::PLAY;
	while (_gameState != GameState::EXIT) {		
			//Start synchronization between refresh rate and frame rate
		//_tp.SemiFixedTimeStep();
		//funciona pero ens fiem mes del _fpsLimiter

		_fpsLimiter.startSynchronization();
			//Update the BB
		_gameElements.UpdateBB();
			//Process the input information (keyboard and mouse)
		processInput();
		if (_gameStart){
			if (!_turnOn){
				_startTime = clock(); //Start timer
				if (!sessionTimer){
					sessionTimer = true;
					_startSession = true;
					_startTimeSession = clock();
				}
				_turnOn = true;
			}
			_secondsPassed = (clock() - _startTime) / CLOCKS_PER_SEC;
			if (_secondsPassed >= _secondsToDelay){//s'ha acabat el torn
				_turnOn = false;
				_gameStart = false;
				red.endTurn();
			}
		}
		//Si entra aqu� vol dir que hi ha un missatge a tractar
		while (!_recievedMessages.empty()){
			//el servidor agafa el missatge en cua que 
			//ha rebut del client el tracta y
			//l'afegeix a la cua de sendmessages
			lockqueueReceive.lock();
			NetworkInfo tempInfo=_recievedMessages.front();
			lockqueueReceive.unlock();
			
			//Comprovem si la network ens ha enviat un missatge de que s'ha desconectat
			//un client
			if (tempInfo._ID == DISCONNECTED){
				lockqueueReceive.lock();
				_recievedMessages.pop();
				lockqueueReceive.unlock();
				_gameState = GameState::EXIT;
			}
			else{
				_Enemies[tempInfo._ID - 1].InitMovement(tempInfo._movement, tempInfo._shoot, tempInfo._angle);
				
				while (!_Enemies[tempInfo._ID - 1].isDone){
					UpdateEnemyMovements();
					executePlayerCommands();
					doPhysics();
					if (DEBUG_RENDER_SERVER)renderGame();
					EndGame();
				}
				
				tempInfo._canShoot = _Enemies[tempInfo._ID - 1].shootAllowed;
				tempInfo._movementAllowed = _Enemies[tempInfo._ID - 1].moveAllowed;
				tempInfo._score = _Enemies[tempInfo._ID - 1].score;

				lockqueueSend.lock();
				_sendMessages.push(tempInfo);
				lockqueueSend.unlock();

				_Enemies[tempInfo._ID - 1].isDone = false;

				lockqueueReceive.lock();
				_recievedMessages.pop();
				lockqueueReceive.unlock();
				red.sendAnswer();
			}
		}
			//Execute the player actions (keyboard and mouse)
			//Update the game status

			//Draw the objects on the screen
			//Spawn enemies

		//SpawnEnemy();
		//check if time is out
		//
		executePlayerCommands();
		if (DEBUG_RENDER_SERVER)renderGame();
		//Comprova el temps de la sessio, una vegada s'ha donat ho tanca tot
		// i notifica a la resta de jugadors que s'ha acabat i qui ha guanyat
		if (_startSession){
			_secondsPassedSession = (clock() - _startTimeSession) / CLOCKS_PER_SEC;
			if (_secondsPassedSession >= _secondsToDelaySession){
				int ID_winner = 0;
				int winner_score = 0;
				for (int i = 0; i < _Enemies.size(); i++){
					if (_Enemies[i].score > winner_score) ID_winner = _Enemies[i].GetID();
				}
				red.endGame(ID_winner);
				_gameState = GameState::EXIT;
			}
		}
			//Force synchronization
		_fpsLimiter.forceSynchronization();
	}
}

/*
* Processes input with SDL
*/
void Game::processInput() {
	_inputManager.update();
	//Review https://wiki.libsdl.org/SDL_Event to see the different kind of events
	//Moreover, table show the property affected for each event type
	SDL_Event evnt;
	//Will keep looping until there are no more events to process
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			_inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
			break;
		case SDL_KEYDOWN:
			_inputManager.pressKey(evnt.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.releaseKey(evnt.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			_inputManager.pressKey(evnt.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.releaseKey(evnt.button.button);
			break;
		default:
			break;
		}
	}
}

bool Game::collisionBala(glm::vec3 movement, int i) {
	//calculem la BB en la posicio a on es moura el objecte
	glm::mat4 scaleMatrix, noRotateMatrix;
	glm::vec3 tempTrans = _gameElements.getGameElement(i)._translate + movement;
	noRotateMatrix = glm::translate(noRotateMatrix, tempTrans);

	scaleMatrix = glm::scale(scaleMatrix, _gameElements.getGameElement(i)._scale);
	noRotateMatrix = glm::scale(noRotateMatrix, _gameElements.getGameElement(i)._scale);
	
	//posicions al mon del objecte
	glm::vec4 centre = noRotateMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OCentre, 1);
	glm::vec4 extent = scaleMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OExtent, 1);

	glm::vec4 min = noRotateMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OMin, 1);
	glm::vec4 max = noRotateMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OMax, 1);

	//asignem valors a les BB segons els calculs
	_gameElements.getGameElement(i)._boundingAABB.Centre.x = centre.x;
	_gameElements.getGameElement(i)._boundingAABB.Centre.y = centre.y;
	_gameElements.getGameElement(i)._boundingAABB.Centre.z = centre.z;

	_gameElements.getGameElement(i)._boundingAABB.Min.x = min.x;
	_gameElements.getGameElement(i)._boundingAABB.Min.y = min.y;
	_gameElements.getGameElement(i)._boundingAABB.Min.z = min.z;

	_gameElements.getGameElement(i)._boundingAABB.Max.x = max.x;
	_gameElements.getGameElement(i)._boundingAABB.Max.y = max.y;
	_gameElements.getGameElement(i)._boundingAABB.Max.z = max.z;


	_gameElements.getGameElement(i)._boundingAABB.Extent.x = extent.x;
	_gameElements.getGameElement(i)._boundingAABB.Extent.y = extent.y;
	_gameElements.getGameElement(i)._boundingAABB.Extent.z = extent.z;

	for (int z = 0; z < _gameElements.getNumGameElements();z++) {
		if (_gameElements.getGameElement(i)._objectType == 1) {
			if (z != i) {
				if (_gameElements.getGameElement(i).OverlapAABB(_gameElements.getGameElement(z)._boundingAABB)) {
						
						if (_gameElements.getGameElement(z)._objectType != 6) {
							_Enemies[_gameElements.getGameElement(i)._ID - 1].isDone = true;
							_gameElements.removeLastElement(i);
							return true;
						}
						else {
							//Amb aix� voldria dir que ha xocat amb un enemic(o amb ell mateix que es lo primer que filtrem)
							int IDSendShoot = _gameElements.getGameElement(i)._ID;//aixo es la bala
							int IDReceiveShoot = _gameElements.getGameElement(z)._ID;//el que rep
							for (int _i = 0; _i < _Enemies.size(); _i++) {
								//m'asseguro que el tank contra el que col�lisiona no es el mateix que dispara
								if (IDSendShoot != IDReceiveShoot) {
									if (_Enemies[_i].GetID() == IDReceiveShoot) {
										//aix� vol dir que la bala ha col�lisionat amb aquest enemic
										//OMPLIRAQUI
										//setejar que s'ha tocat a algun jugador
										//baixar el score del jugador al qual s'ha tocat, aix� no se si ho vols fer, per� ho he deixat preparat per si et ve de gust fer-ho
										//pujar el score del jugador que ha disparat, si necessites trobar-lo ho pots fer a traves de la bala, ja que he fet que agafi la mateix id
										//std::cout << "he tocat un enemic muajajajaaj";
										_Enemies[IDSendShoot-1].score += 1;
										_Enemies[IDSendShoot-1].shootAllowed = true;
										_Enemies[IDSendShoot - 1].isDone = true;
										//Basicament ens asegurem que el que borrem es
										//una bala i no tota la escena : )
										if(_gameElements.getGameElement(
											_gameElements.getNumGameElements()-1)._objectType==1)
											_gameElements.removeLastElement();
										return true;
									}
								}
							}
							
						}
						return false;
				}
			}
		}

	}
	//al no colisionar podem deixar la BB com a la de seguent pos.
	return false;
}

/*
* Executes the actions sent by the user by means of the keyboard and mouse
*/
void Game::executePlayerCommands() {
	//Attack bullet
	if (_inputManager.isKeyDown(SDLK_f)) {
		UpdateEnemy = true;
		Cell cell1;
		std::vector<Cell> TempsCells;
		cell1.setCellPosition(0, 0);
		TempsCells.push_back(cell1);
		cell1.setCellPosition(1, 0);
		TempsCells.push_back(cell1);
		cell1.setCellPosition(2, 0);
		TempsCells.push_back(cell1);
		cell1.setCellPosition(2, 1);
		TempsCells.push_back(cell1);
		_Enemies[0].InitMovement(TempsCells, true, 270);
	}
	if (_inputManager.isKeyDown(SDLK_d)) {
		UpdateEnemy = true;
		Cell cell1;
		std::vector<Cell> TempsCells;
		cell1.setCellPosition(2, 1);
		TempsCells.push_back(cell1);
		cell1.setCellPosition(3, 1);
		TempsCells.push_back(cell1);
		cell1.setCellPosition(3, 0);
		TempsCells.push_back(cell1);
		_Enemies[0].InitMovement(TempsCells, true, 270);
	}

	if (_inputManager.isKeyDown(SDLK_s)) {
		UpdateEnemy = true;
		Cell cell1;
		std::vector<Cell> TempsCells;
		cell1.setCellPosition(3, 0);
		TempsCells.push_back(cell1);
		cell1.setCellPosition(4, 0);
		TempsCells.push_back(cell1);
		cell1.setCellPosition(5, 0);
		TempsCells.push_back(cell1);
		_Enemies[0].InitMovement(TempsCells, true, 270);
	}

	for (int x = 0; x < _Enemies.size(); x++) {
		if (_Enemies[x].getShoot()) {
			//aix� voldr� dir que el enemic ha de disparar, per tant ja veure com ho fem
			for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
				if (_gameElements.getGameElement(i)._ID == _Enemies[x].GetID()) {
					_gameElements.BulletEnemy(_textureManager, _Enemies[x].GetID());
					_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._translate = _gameElements.getGameElement(i)._translate + glm::vec3(0.0f, 0.0f, 0.15f);
					float newXX = _gameElements.getGameElement(1)._translate.x - _gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._translate.x;
					float newYY = _gameElements.getGameElement(1)._translate.y - _gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._translate.y;
					float anglebala = glm::atan(newYY, newXX);
					anglebala = anglebala * 180.0f / 3.14f;
					//std::cout << _Enemies[x].getAngleTOP();
					_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._angle = _Enemies[x].getAngleTOP();
					_Enemies[x].setShoot(false);
					return;
				}
			}
		}
	}
	if (_inputManager.isKeyPressed(SDLK_ESCAPE)) {
		_gameState = GameState::EXIT;
	}
	//Cameras
	
	if (FPCamera) {
		FPCamera = false;
		TPCamera = false;
		_camera.perpespectiveMatrix();
		_camera.changePos(glm::vec3(-3.0f, 4.0f, 8.0f), glm::vec3(-3.0f, 2.5f, -8.0f));
	}
}

/*
funcio que treballa amb un delay de 5 segons en el qual va fent un spawn d'enemics i vigilar que no hi hagi cap enemic en aquell lloc,
potser que al fer el random assigni un valor d'un espai ocupat, per� preferia aix� abans que ocupar un espai de memoria, amb els llocs buits,
donat que seria un espai for�a in�til, ja que si es juga normal, no haurien de coincidir els tancs.
*/
void Game::SpawnEnemy(){
	//invocara en una posici� random
	for (unsigned int i = 0; i < SpawnPoints.size(); i++) {
		TankEnemy Tank;
		Tank.InitTankEnemy(SpawnPoints[i]._point, SpawnPoints[i]._point, 0, 0, 5, 0, ConvertPositionToGridX(glm::abs(SpawnPoints[i]._point.x)), ConvertPositionToGridX(SpawnPoints[i]._point.y));
		//aqui la i hauria de ser la ID
		_gameElements.SpawnEnemy(SpawnPoints[i]._point, _textureManager, i+1);
		//Tank.InitMovement();
		Tank.SetID(i+1);
		_Enemies.push_back(Tank);
		//REVISAR assignar una ID correcte
		//REVISAR el sistema d'spawneig
	}
}

void Game::InitTurn(){
	if (!isinTurn) {		
		TurnMovement = true;
		isinTurn = true;
		GameTurn = std::clock();
	}
}



void Game::EndGame(){
	if (TurnMovement) {
		if (std::clock() - GameTurn > Time) {
			//aqui canviariem a attack
			TurnMovement = false;
			GameTurn = std::clock();
			TurnAttack = true;
		}
	}
	else if (TurnAttack) {
		if (std::clock() - GameTurn > Time) {
			//aqui ja acabariem i enviariem dades
			isinTurn = false;
			TurnAttack = false;
		}
	}
}

void Game::loadGameTextures(){
	GameObject currentGameObject;
	//Load the game textures			
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		currentGameObject = _gameElements.getGameElement(i);

		(_gameElements.getGameElement(i))._textureID = _textureManager.getTextureID(currentGameObject._textureFile);
	}
}


void Game::UpdateEnemyMovements(){

	//Aix� es per moure els enemics//de moment nomes es el primer perque sino fara patapum
	for (int i = 0; i < _Enemies.size(); i++) {
		if (_Enemies[i].getTurn()) {
			for (int x = 0; x < _gameElements.getNumGameElements(); x++) {
				if (_gameElements.getGameElement(x)._ID == _Enemies[i].GetID()) {
					if (_Enemies[i].getNextX() != -1 && _Enemies[i].getNextY() != -1) {
						if (!_gameElements.collision(glm::vec3((-_Enemies[i].getNextX()*0.5f) - 0.25f, _Enemies[i].getNextY()*0.5f + 0.25f, 0.0f), x)) {
							_Enemies[i].UpdateMovement();
							
						}
						else {
							//vol dir que el jugador esta fent trampes i el moviment no es correcte
							//el tornem a la posicio inicial
							//AQUISETEJEMDADES DE QUE FA TRAMPES
							glm::vec2 pos = _Enemies[i].GoToinit();
							_gameElements.getGameElement(x)._translate = glm::vec3(pos.x*0.5f - 0.25f, pos.y*0.5f + 0.25f,  0.0f);
							_Enemies[i].setPositionTOP(glm::vec3(pos.x*0.5f - 0.25f, pos.y*0.5f + 0.25f, 0.0f));
							std::cout << "moviment incorrecte" << std::endl;
							_Enemies[i].moveAllowed = false;
							_Enemies[i].isDone = true;
						}
					}
					//aix� vol dir que ser� el ultim
					else _Enemies[i].UpdateMovement();
				}
			}
		}
	}

	//moviment del objecte
	int position = 0;
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		if (_gameElements.getGameElement(i)._objectType == 6) {
			_gameElements.getGameElement(i)._angle = _Enemies[position].getAngleBOTTOM();
			_gameElements.getGameElement(i)._rotation.z = 1.0f;
			_gameElements.getGameElement(i)._translate = _Enemies[position].getPositionTOP();
			position++;
		}
	}
}

int Game::ConvertPositionToGridX(float _X){
	return glm::floor(_X*2);;
}

/*
* Update the game objects based on the physics
*/
void Game::doPhysics() {
	//for que fa moure totes les bales, que tenen el type red_cube, les bales dels enemics s�n mes rapides que les del jugador, shit happens
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		if (_gameElements.getGameElement(i)._objectType == RED_CUBE) {
			glm::vec3 move = glm::vec3(glm::sin(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f, -glm::cos(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f, 0.0f);
			if (!collisionBala(move, i)) {
				_gameElements.getGameElement(i)._translate.x += glm::sin(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f;
				_gameElements.getGameElement(i)._translate.y -= glm::cos(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f;
			}
		}
	}
}

/**
* Draw the sprites on the screen
*/
void Game::renderGame() {
		//Temporal variable
	GameObject currentRenderedGameElement;

		//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//Bind the GLSL program. Only one code GLSL can be used at the same time
	_colorProgram.use();

	std::string _score;
	_score = "Score: " + std::to_string(score);
	_textManager.renderTextOnScreen(_score, _openGLBuffers, _colorProgram, _gameElements.getData(PLANE));//s'ha fet el pintat de text en una funcio externa per evitar haver de fer el render game mes gran

	GLuint viewMatrixUniform = _colorProgram.getUniformLocation("viewMatrix");
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, glm::value_ptr(_camera._viewMatrix));

	GLuint projectionMatrixUniform = _colorProgram.getUniformLocation("projectionMatrix");
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(_camera._projectionMatrix));
	glActiveTexture(GL_TEXTURE0);
	
	//For each one of the elements: Each object MUST BE RENDERED based on its position, rotation and scale data
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {	
		currentRenderedGameElement = _gameElements.getGameElement(i);	
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		
		glm::mat4 modelMatrix;
		modelMatrix = glm::translate(modelMatrix, currentRenderedGameElement._translate);

		if (currentRenderedGameElement._angle != 0) {
			modelMatrix = glm::rotate(modelMatrix, glm::radians(currentRenderedGameElement._angle), currentRenderedGameElement._rotation);
		}
		modelMatrix = glm::scale(modelMatrix, currentRenderedGameElement._scale);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, currentRenderedGameElement._textureID);
		
		//fer que si un es mes gran que l'altre s'equiparin, per aixi fer que la textura quedi igual(?)
		float terme1;
		if (currentRenderedGameElement._scale.x > currentRenderedGameElement._scale.y) {
			terme1 = currentRenderedGameElement._scale.x;
		}
		else {
			terme1 = currentRenderedGameElement._scale.y;
		}
		
		if(currentRenderedGameElement._objectType==RED_CUBE)glUniform2f(_textureScaleFactorLocation, 1.0f, 1.0f);
		else glUniform2f(_textureScaleFactorLocation, terme1, currentRenderedGameElement._scale.z);
		
		//Bind the uniform variables with the contet
		material currentMaterial;
		currentMaterial = _materialManager.getMaterialComponents(currentRenderedGameElement._materialType);
		glUniform3fv(_materialAmbientUniform, 1, glm::value_ptr(currentMaterial.ambient));
		glUniform3fv(_materialDiffuseUniform, 1, glm::value_ptr(currentMaterial.diffuse));
		glUniform3fv(_materialSpecularUniform, 1, glm::value_ptr(currentMaterial.specular));
		glUniform1f(_materialShininessUniform, currentMaterial.shininess);
		
		//for que recorre el vector de llums i assigna els valors de cada llum al array del shader per a fer el calcul de multiples llums
		for (int i = 0; i < (int)_lights.size();i++) {
			//Set up the uniform light variables 
			_lightAmbientUniform = _colorProgram.getUniformLocation("pointLights["+std::to_string(i) + "].ambient");
			_lightDiffuseUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].diffuse");
			_lightSpecularUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].specular");
			_lightTypeUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].type");
			_lightDirUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].direction");
			_lightCutOffUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].cutOff");
			_lightConstantUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].constant");
			_lightLinearUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].linear");
			_lightQuadraticUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].quadratic");
			_lightPosition = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].position");
			_lightOn = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].isOn");

			//Bind the uniform variables with the contet of the light material
			glUniform3fv(_lightAmbientUniform, 1, glm::value_ptr(_lights[i].getAmbient()));
			glUniform3fv(_lightDiffuseUniform, 1, glm::value_ptr(_lights[i].getDiffuse()));
			glUniform3fv(_lightSpecularUniform, 1, glm::value_ptr(_lights[i].getSpecular()));
			glUniform1i(_lightTypeUniform, _lights[i].getType());
			glUniform3fv(_lightDirUniform, 1, glm::value_ptr(_lights[i].getDirection()));
			glUniform1f(_lightCutOffUniform, glm::cos(glm::radians(_lights[i].getCutOff())));
			glUniform1f(_lightConstantUniform, _lights[i].getConstant());
			glUniform1f(_lightLinearUniform, _lights[i].getLinear());
			glUniform1f(_lightQuadraticUniform, _lights[i].getQuadratic());
			glUniform3fv(_lightPosition, 1, glm::value_ptr(_lights[i].getPosition()));
			if (_lights[i].getIsOn())glUniform1i(_lightOn, 1);
			else glUniform1i(_lightOn, 0);
		}
		GLint textureLocation = _colorProgram.getUniformLocation("textureData");
		glUniform1f(textureLocation, 0);

		GLuint modelMatrixUniform = _colorProgram.getUniformLocation("modelMatrix");
		glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));

		//aixo es per si el objecte actual li afecta la llum de l'escenari o no 
		GLint lightEffect = _colorProgram.getUniformLocation("affectedByLight");
		glUniform1i(lightEffect, 1);
		
		//Passem la posicio de la camera per a fer els calculs necesaris
		GLuint _viewerPosition = _colorProgram.getUniformLocation("viewerPosition");
		glUniform3fv(_viewerPosition, 1, glm::value_ptr(_camera.getCamPos()));

		_openGLBuffers.sendDataToGPU(_gameElements.getData(currentRenderedGameElement._objectType), _gameElements.getNumVertices(currentRenderedGameElement._objectType));

		 glBindTexture(GL_TEXTURE_2D, 0);
	}

	//Unbind the program
	_colorProgram.unuse();

	//Swap the display buffers (displays what was just drawn)
	_window.swapBuffer();
}