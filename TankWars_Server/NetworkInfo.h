#pragma once
#include<string>
#include <vector>
#include <istream>
#include "Cell.h"

class NetworkInfo{
public:
	NetworkInfo();
	~NetworkInfo();
	std::string _missatge;
	int _ID;
	bool _shoot;
	int _angle;
	std::vector<Cell> _movement;
	bool _canShoot;
	bool _movementAllowed;
	bool _winner;
	int _score;
private:

};

