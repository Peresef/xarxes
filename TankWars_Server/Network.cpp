#include "Network.h"



Network::Network(std::queue<NetworkInfo>* recievedMessages,
	std::queue<NetworkInfo>* sendMessages, bool* startGame,
	std::mutex* lockqueueReceive, std::mutex* lockqueueSend):
	listening(&Network::Receive,this){
		_recievedMessages = recievedMessages;
		_sendMessages = sendMessages;
		FinishedSending = false;
		listener.listen(55001);
		selector.add(listener);
		ip = sf::IpAddress::getLocalAddress();
		ID = 1;
		roomFull = false;
		_startGame = false;
		_exit = false;
		_StartGame = startGame;
		activePlayer = 0;
		_lockqueueReceive = lockqueueReceive;
		_lockqueueSend = lockqueueSend;
		listening.launch();
}


Network::~Network(){
	Clear();
}

void Network::Clear(){
	_exit = true;
	_lockqueueReceive->lock();
	while (!_recievedMessages->empty())_recievedMessages->pop();
	_lockqueueReceive->unlock();
	_lockqueueSend->lock();
	while (!_sendMessages->empty())_sendMessages->pop();
	_lockqueueSend->unlock();
	selector.clear();
	for (std::list<sf::TcpSocket*>::iterator it = clients.begin(); it != clients.end();) {
		sf::TcpSocket& client = **it;
		client.disconnect();
		it = clients.erase(it);
		delete &client;
		if (it != clients.end())++it;
	}
	clients.clear();
	listener.close();
}

//Decodifica el missatge que rep dels clients i ho afegeix a la cua de missatges que tractar pel game
void Network::Decode(std::string missatge){
	NetworkInfo tempInfo;
	//ID
	tempInfo._ID = std::atoi(&missatge[0]);
	//ahoot
	if (missatge[2] == 'T') tempInfo._shoot = true;
	else  false;
	//angle
	tempInfo._angle = std::stoi(missatge.substr(4, 3));
	//vector cell
	for (int i = 7;i < missatge.size();i++){		
		if (missatge[i]!='_'&&missatge[i]!='/'){
			Cell tempCell;
			tempCell.setCellPosition(std::stoi(missatge.substr(i, 2)), std::stoi(missatge.substr(i + 2, 2)));
			tempInfo._movement.push_back(tempCell);
			i += 3;
		}
		else if (missatge[i] == '/')	{
			_lockqueueReceive->lock();
			_recievedMessages->push(tempInfo);
			_lockqueueReceive->unlock();
			return;
		}
	}
	return;
}

std::string Network::Code(int _case, NetworkInfo tempInfo){
	std::string missatge;
	switch (_case){
	case STARTIMER:
		missatge += "1/";
		return missatge;
	case STOPTIMER:
		missatge += "2/";
		return missatge;
	case ANSWERCLIENT:
		missatge += "3_";

		if (tempInfo._movementAllowed == true) missatge += "T_";
		else missatge += "F_";

		if (tempInfo._canShoot == true) missatge += "T_";
		else missatge += "F_";

		if (tempInfo._score<10) missatge += "0" + std::to_string(tempInfo._score);
		else missatge += std::to_string(tempInfo._score);

		missatge += "/";
		return missatge;
	case PROPAGATECLIENTS:
		missatge += "4_";
		
		missatge += std::to_string(tempInfo._ID) + "_";

		if(tempInfo._angle<10) missatge +="00"+ std::to_string(tempInfo._angle) + "_";
		else if (tempInfo._angle<100) missatge +="0"+ std::to_string(tempInfo._angle) + "_";
		else missatge += std::to_string(tempInfo._angle) + "_";

		if (tempInfo._shoot == true) missatge += "T_";
		else missatge += "F_";

		if(tempInfo._score<10) missatge +="0"+ std::to_string(tempInfo._score) + "_";
		else missatge += std::to_string(tempInfo._score) + "_";

		for (int i = 0; i < tempInfo._movement.size(); i++){
			if (tempInfo._movement[i].getPosX()<10)
				missatge += "0" + std::to_string(tempInfo._movement[i].getPosX());
			else missatge += std::to_string(tempInfo._movement[i].getPosX());
			if (tempInfo._movement[i].getPosY()<10)
				missatge += "0" + std::to_string(tempInfo._movement[i].getPosY());
			else missatge += std::to_string(tempInfo._movement[i].getPosY());
			if (i < tempInfo._movement.size() - 1) missatge += "_";
		}
		missatge += '/';
		return missatge;
	case ENDGAME_WIN:
		missatge += "5_T/";
		return missatge;
	case ENDGAME_LOSE:
		missatge += "5_F/";
		return missatge;
	case DISCONNECTED:
		missatge += "6/";
		return missatge;
	default:
		return nullptr;
	}
	return nullptr;
}

void Network::Receive(){
	std::cout << std::endl<< "How many players will there be?"<<std::endl;
	std::cin >> max_clients;
	while (max_clients!=2 && max_clients != 3 && max_clients != 4){
		std::cin.clear();
		std::cout << "Please enter a number between 2 and 4." << std::endl;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin >> max_clients;
	}
		//Acceptar conexions i guardarles
		//rebre missatges dels clients i reenviar
		//Revisar les conexions tancades i alliberades
		//fem que el selector esperi coses entrants
	while (!_exit){
		std::cout << "Waiting for players" << std::endl;
		if (selector.wait()) {
			if (selector.isReady(listener)) {
				if (!roomFull){
					sf::TcpSocket* client = new sf::TcpSocket;
					if (listener.accept(*client) == sf::Socket::Done) {
						clients.push_back(client);
						selector.add(*client);
						players.push_back(std::make_pair(client, ID));
						std::cout << "Cliente conectado: " << ID << std::endl;;
						std::string mmsg = "0_"+ std::to_string(ID) + "/";
						client->send(mmsg.c_str(), mmsg.size());
						ID++;
						if (players.size() >= max_clients){
							roomFull = true;
							_startGame = true;
						}
					}
					else {
						delete client;
					}
				}
			}
			//test all other sockets (the clients)
			if (roomFull){
				if (_startGame)startGame();
				for (std::list<sf::TcpSocket*>::iterator it = clients.begin(); it != clients.end();) {
					sf::TcpSocket& client = **it;
					if (selector.isReady(client)) {
						char data[MAX_BYTES];
						std::size_t received;
						memset(data, 0, sizeof data);
						sf::Socket::Status stateR;
						stateR = client.receive(data, MAX_BYTES, received);
						switch (stateR){
						case sf::Socket::Done:
							Decode(data);
							break;
						case sf::Socket::NotReady:
							break;
						case sf::Socket::Partial:
							break;
						case sf::Socket::Disconnected:
							selector.remove(client);
							client.disconnect();
							it = clients.erase(it);
							delete &client;
							endGame();
							break;
						case sf::Socket::Error:
							break;
						default:
							break;
						}
					}
					if (it != clients.end())++it;
				}
			}
		}
		else{
			return;
		}
	}
}


void Network::startGame(){
	NetworkInfo tempinfo;
	_startGame = false;
	std::string mssg = Code(STARTIMER, tempinfo);
	send(activePlayer, mssg);
	*_StartGame = true;
}

void Network::startTurn(){
	activePlayer++;
	if (activePlayer >= max_clients)activePlayer = 0;
	clock_t _startTime = clock();
	double _milisecondsPassed;
	double _milisecondsToDelay = MIN_TIME_BETWEEN_SENDS;
	bool done = false;
	while (!done){
		_milisecondsPassed = ((clock() - _startTime) / CLOCKS_PER_SEC) * 60;
		if (_milisecondsPassed >= _milisecondsToDelay){
			done = true;
			NetworkInfo tempinfo;
			std::string mssg = Code(STARTIMER, tempinfo);
			send(activePlayer, mssg);
			*_StartGame = true;
		}
	}
}

void Network::endTurn(){
	NetworkInfo tempinfo;
	std::string mssg = Code(STOPTIMER, tempinfo);
	send(activePlayer, mssg);
}
void Network::endGame(int winner){
	NetworkInfo tempinfo;
	for (int i = 0; i < players.size(); i++){
		if(i==winner-1) while(!send(i, Code(ENDGAME_WIN, tempinfo)));
		else while (!send(i, Code(ENDGAME_LOSE, tempinfo)));
	}
	Sleep(200);
	_exit = true;
}

void Network::endGame(){
	NetworkInfo tempinfo;
	for (int i = 0; i < players.size(); i++){
		send(i, Code(DISCONNECTED, tempinfo));
	}
	tempinfo._ID = DISCONNECTED;
	_lockqueueReceive->lock();
	_recievedMessages->push(tempinfo);
	_lockqueueReceive->unlock();
	_exit = true;
}

void Network::sendAnswer(){
	_lockqueueSend->lock();
	NetworkInfo tempInfo = _sendMessages->front();
	_lockqueueSend->unlock();
	for (int i = 0; i < players.size(); i++){
		if(i== activePlayer)send(i, Code(ANSWERCLIENT, tempInfo));
		else{
			send(i, Code(PROPAGATECLIENTS, tempInfo));
		}
	}
	_lockqueueSend->lock();
	_sendMessages->pop();
	_lockqueueSend->unlock();
	startTurn();
}

bool Network::send(int player, std::string message){
	sf::Socket::Status state;
	bool exit = false;
	while (!exit){
		state = players[player].first->send(message.c_str(), message.size(), BytesSent);
		switch (state) {
		case sf::Socket::Done:
			exit= true;
			break;
		case sf::Socket::NotReady:
			exit = true;
			break;
		case sf::Socket::Partial:
			std::cout << "S'est� enviant el misstage" << std::endl;
			FinishedSending = false;
			splitmensaje = message;
			while (!FinishedSending) {
				sf::Socket::Status state;
				splitmensaje = splitmensaje.substr(BytesSent, message.size() - BytesSent);
				state = players[player].first->send(splitmensaje.c_str(), splitmensaje.size(), BytesSent);
				switch (state) {
				case sf::Socket::Done:
					FinishedSending = true;
					break;
				case sf::Socket::NotReady:
					FinishedSending = true;
					break;
				case sf::Socket::Partial:
					break;
				case sf::Socket::Disconnected:
					std::cout << "Desconectado" << std::endl;
					selector.remove(*players[player].first);
					players[player].first->disconnect();
					delete players[player].first;
					players.erase(players.begin()+player);
					FinishedSending = true;
					break;
				case sf::Socket::Error:
					FinishedSending = true;
					exit = true;
					std::cout << "Error al enviar" << std::endl;
					break;
				default:
					FinishedSending = true;
					break;
				}
			}
			exit = true;
			break;
		case sf::Socket::Disconnected:
			std::cout << "Desconectado" << std::endl;
			selector.remove(*players[player].first);
			players[player].first->disconnect();
			clients.remove(players[player].first);
			delete players[player].first;
			endGame();
			exit = true;
			break;
		case sf::Socket::Error:
			exit = true;
			break;
		default:
			exit = true;
			break;
		}
	}
	return true;
}
