#pragma once

#include <GL/glew.h>			//The OpenGL Extension Wrangler
#include <SDL/SDL.h>
#include <SDL_ttf/SDL_ttf.h>
#include <string>
#include <vector>
#include "GameObject.h"
#include "OpenGLBuffers.h"
#include "GLSLProgram.h"



class TextManager
{
	TTF_Font* _font;
	std::vector <GLuint> _textureData;
	std::vector <std::string> _listOfTexts;
	

public:
	TextManager();
	~TextManager();
	GLuint createText(std::string message, SDL_Color color);
	GLuint TextManager::getTextureID(std::string message, SDL_Color color);
	void renderTextOnScreen(std::string text,  OpenGLBuffers & buffers, GLSLProgram & colorProgram, Vertex * data);

};
