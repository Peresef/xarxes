#version 150

struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

struct Light {
	vec3 position;
	vec3 direction; 
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
	
	int type;

	float cutOff;
    float constant;
    float linear;
    float quadratic;

	int isOn;
};  

#define NR_POINT_LIGHTS 3

//The fragment shader operates on each fragment in a given polygon and it has to output the final color
//The final color is a combination of the object color, the texture and the Phong reflectance model based on the draw mode
//The fragment shader receives the interpolated RGBA color, the UV texture coordinates, the normal and the current position sent from the vertex shader 

in vec4 fragmentColor;
in vec2 fragmentTextureCoordinates;
in vec3 fragmentNormal;
in vec3 fragmentPosition;

uniform vec3 viewerPosition;
uniform sampler2D textureData;
uniform Material material;
uniform Light pointLights[NR_POINT_LIGHTS];

out vec4 finalColor;

// Calculates the color when using a directional light.
vec3 CalcDirLight(Light light, vec3 normal, vec3 viewDir){
    vec3 lightDir = normalize(-light.direction);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // Combine results
    vec3 ambient=material.ambient*light.ambient;
    vec3 diffuse = light.diffuse * diff * material.diffuse;
    vec3 specular = light.specular * spec * material.specular;
    return (ambient + diffuse + specular);
}

// Calculates the color when using a point light.
vec3 CalcPointLight(Light light, vec3 normal, vec3 fragPos, vec3 viewDir){
    vec3 lightDir = normalize(light.position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    // Combine results
    vec3 ambient = light.ambient * vec3(texture(textureData, fragmentTextureCoordinates));
    vec3 diffuse = light.diffuse * diff * vec3(texture(textureData, fragmentTextureCoordinates));
    vec3 specular = light.specular * spec * vec3(texture(textureData, fragmentTextureCoordinates));
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
}

vec3 CalcSpotlight(Light light, vec3 norm, vec3 fragPos, vec3 viewDir){
	vec3 ambient=material.ambient*light.ambient;
	vec3 lightDir = normalize(light.position -fragPos);
	
	float theta = dot(lightDir, normalize(-light.direction)); 
	if(theta > light.cutOff) { 
		float diff = max(dot(norm, lightDir), 0.0);
		vec3 diffuse = light.diffuse* diff * material.diffuse;

			//Specular component
		vec3 reflectDir = reflect(-lightDir, norm);  
		float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
		vec3 specular = light.specular * spec * material.specular;  
							
			//attenuation component
		float distance = length(light.position - fragmentPosition);    
		float attenuation = 1.0f / (light.constant
		+ light.linear * distance
		+ light.quadratic * (distance * distance));
		ambient*=attenuation;
		diffuse*=attenuation;
		specular*=attenuation;

		return (ambient+diffuse+specular);
	}
	return vec3(0.25f,0.25f,0.25f);
}


void main() {	
	vec4 tempColor;	
	finalColor = fragmentColor*texture(textureData,fragmentTextureCoordinates);
	for(int i = 0; i < NR_POINT_LIGHTS; i++){	
		if (pointLights[i].isOn==1) {		
			vec3 norm = normalize(fragmentNormal);
			vec3 viewDir = normalize(viewerPosition - fragmentPosition);
			
			if(pointLights[i].type==0) 
			tempColor += vec4(CalcDirLight(pointLights[i], norm, viewDir),1);
			if (pointLights[i].type==1)
			tempColor += vec4(CalcPointLight(pointLights[i], norm, fragmentPosition, viewDir),1);
			if(pointLights[i].type==2){
			//finalColor += vec4(CalcSpotlight(pointLights[i], norm, fragmentPosition, viewDir),1);
				vec3 ambient=material.ambient*pointLights[i].ambient;
				vec3 lightDir = normalize(pointLights[i].position -fragmentPosition);

				float theta = dot(lightDir, normalize(-pointLights[i].direction)); 
				if(theta < pointLights[i].cutOff) { 
					float diff = max(dot(norm, lightDir), 0.0);
					vec3 diffuse = pointLights[i].diffuse* diff * material.diffuse;

						//Specular component
					vec3 viewDir = normalize(viewerPosition - fragmentPosition);
					vec3 reflectDir = reflect(-lightDir, norm);  
					float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
					vec3 specular = pointLights[i].specular * spec * material.specular;  
							
						//attenuation component
					float distance = length(pointLights[i].position - fragmentPosition);    
					float attenuation = 1.0f / (pointLights[i].constant
					+ pointLights[i].linear * distance
					+ pointLights[i].quadratic * (distance * distance));
					ambient*=attenuation;
					diffuse*=attenuation;
					specular*=attenuation;

					finalColor+=vec4((ambient+diffuse+specular),1)*finalColor;
				}
				else{
					// else, use ambient light so scene isn't completely dark outside the spotlight. 
					finalColor += vec4(0.75f,0.75f,0.75f, 1.0f);	
				}
			}
			finalColor = tempColor*finalColor; 
			/*
			if(pointLights[i].type==0) {//Directional light 
				lightDir = normalize(-pointLights[i].direction);

				float diff = max(dot(norm, lightDir), 0.0);
				vec3 diffuse = pointLights[i].diffuse* diff * material.diffuse;

					//Specular component
				vec3 viewDir = normalize(viewerPosition - fragmentPosition);
				vec3 reflectDir = reflect(-lightDir, norm);  
				float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
				vec3 specular = pointLights[i].specular * spec * material.specular;  

					//attenuation component
				float distance = length(pointLights[i].position - fragmentPosition);    
				float attenuation = 1.0f / (pointLights[i].constant
				+ pointLights[i].linear * distance
				+ pointLights[i].quadratic * (distance * distance));
				ambient*=attenuation;
				diffuse*=attenuation;
				specular*=attenuation;

				finalColor=vec4((ambient+diffuse+specular),1)*finalColor;
			}
			else if (pointLights[i].type==1) { //Spotlight  
				lightDir = normalize(pointLights[i].position -fragmentPosition);

				float theta = dot(lightDir, normalize(-pointLights[i].direction)); 
				if(theta < pointLights[i].cutOff) { 
					float diff = max(dot(norm, lightDir), 0.0);
					vec3 diffuse = pointLights[i].diffuse* diff * material.diffuse;

						//Specular component
					vec3 viewDir = normalize(viewerPosition - fragmentPosition);
					vec3 reflectDir = reflect(-lightDir, norm);  
					float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
					vec3 specular = pointLights[i].specular * spec * material.specular;  
							
						//attenuation component
					float distance = length(pointLights[i].position - fragmentPosition);    
					float attenuation = 1.0f / (pointLights[i].constant
					+ pointLights[i].linear * distance
					+ pointLights[i].quadratic * (distance * distance));
					ambient*=attenuation;
					diffuse*=attenuation;
					specular*=attenuation;

					finalColor=vec4((ambient+diffuse+specular),1)*finalColor;
				}
				else{
				// else, use ambient light so scene isn't completely dark outside the spotlight. 
				finalColor = vec4((pointLights[i].ambient * vec3(texture(textureData, fragmentTextureCoordinates))), 1.0f);	
				}
			}*/
		}
		else{
			finalColor = fragmentColor*texture(textureData,fragmentTextureCoordinates);
		}
	}
}

