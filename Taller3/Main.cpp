#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>

#define MAX_MENSAJES 30
#define MAX_BYTES 1300





int main() {
	sf::TcpSocket socket;
	bool notFinishedSending = false;

	std::string mensaje = " ";
	std::string splitmensaje;
	std::vector<std::string> aMensajes;
	std::size_t BytesSent;

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf")) {
		std::cout << "Can't load the font file" << std::endl;
	}

	sf::Text chattingText(mensaje, font, 14);
	chattingText.setFillColor(sf::Color(0, 160, 0));
	chattingText.setStyle(sf::Text::Bold);


	sf::Text text(mensaje, font, 14);
	text.setFillColor(sf::Color(0, 160, 0));
	text.setStyle(sf::Text::Bold);
	text.setPosition(0, 560);

	sf::RectangleShape separator(sf::Vector2f(800, 5));
	separator.setFillColor(sf::Color(200, 200, 200, 255));
	separator.setPosition(0, 550);

	sf::IpAddress ip = sf::IpAddress::getLocalAddress();
	char mode;

	//El client es conecta
	socket.connect(ip, 55001);
	socket.setBlocking(false);
	std::cout << "Conectado cliente" << std::endl;
	mode = 'r';
	

	while (window.isOpen()) {
		sf::Event evento;
		while (window.pollEvent(evento)) {
			switch (evento.type) {
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (evento.key.code == sf::Keyboard::Escape) {
					window.close();
				}
				else if (evento.key.code == sf::Keyboard::Return) {
					std::string DataSend =  mensaje;
					sf::Socket::Status state;
					state = socket.send(mensaje.c_str(), mensaje.size(), BytesSent);
					switch (state) {
					case sf::Socket::Done:
						std::cout << "Enviado" << std::endl;
						notFinishedSending = false;
						break;
					case sf::Socket::NotReady:
						std::cout << "NotReady" << std::endl;
						break;
					case sf::Socket::Partial:
						std::cout << "S'est� enviant el misstage" << std::endl;
						notFinishedSending = true;
						splitmensaje = mensaje;
						while (notFinishedSending) {
							sf::Socket::Status state;
							splitmensaje = splitmensaje.substr(BytesSent, mensaje.size());
							state = socket.send(splitmensaje.c_str(), splitmensaje.size(), BytesSent);
							switch (state) {
							case sf::Socket::Done:
								notFinishedSending = false;
								break;
							case sf::Socket::NotReady:
								notFinishedSending = false;
								break;
							case sf::Socket::Partial:
								break;
							case sf::Socket::Disconnected:
								notFinishedSending = false;
								std::cout << "Desconectado" << std::endl;
								aMensajes.push_back("El otro usuario se ha desconectado.");
								socket.disconnect();
								break;
							case sf::Socket::Error:
								notFinishedSending = false;
								std::cout << "Error al enviar" << std::endl;
								break;
							default:
								break;
							}

						}
						break;
					case sf::Socket::Disconnected:
						std::cout << "Desconectado" << std::endl;
						aMensajes.push_back("El otro usuario se ha desconectado.");
						socket.disconnect();
						break;
					case sf::Socket::Error:
						std::cout << "Error al enviar" << std::endl;
						break;
					default:
						break;
					}

					if (aMensajes.size() > 25) {
						aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
					}
					mensaje = " ";
				}
				break;
			case sf::Event::TextEntered:
				if (evento.text.unicode >= 32 && evento.text.unicode <= 126)
					mensaje += (char)evento.text.unicode;
				else if (evento.text.unicode == 8 && mensaje.size() > 0)
					mensaje.erase(mensaje.size() - 1, mensaje.size());
				break;
			}
		}

		char data[MAX_BYTES];

		std::size_t received;
		memset(data, 0, sizeof data);
		if (socket.receive(data, MAX_BYTES, received) == sf::Socket::Done) {
			std::string DataStr(data);
			std::string DataRec = DataStr;
			aMensajes.push_back(DataRec);
		}


		window.draw(separator);
		for (size_t i = 0; i < aMensajes.size(); i++) {
			std::string chatting = aMensajes[i];
			chattingText.setPosition(sf::Vector2f(0, (float)20 * i));
			chattingText.setString(chatting);
			window.draw(chattingText);
		}

		std::string mensaje_ = mensaje + "_";
		text.setString(mensaje_);
		window.draw(text);

		window.display();
		window.clear();
	}
	socket.disconnect();
}