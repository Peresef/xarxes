#include "Game.h"


/**
* Constructor
* Note: It uses an initialization list to set the parameters
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS) :
	_windowTitle(windowTitle), 
	_screenWidth(screenWidth), 
	_screenHeight(screenHeight),
	_gameState(GameState::INIT), 
	_fpsLimiter(enableLimiterFPS, maxFPS, printFPS),
	_camera(screenWidth, screenHeight),
	_lobby(screenWidth, screenHeight){
	FPCamera = true;
	_angleBALA = 0.0f;
	Time = 2500.0f;
	score = 0;
	_Client = new NetworkClient(this);
	_renderState = RenderState::GAME;
}

/**
* Destructor
*/
Game::~Game(){
	delete _Client;
}

/*
* Game execution
*/
void Game::run() {
	_lobby.InitLobby();
	while(_programState == Lobby::ProgramState::LOBBY) {
		_fpsLimiter.startSynchronization();
		_lobby.UpdateLobby(_programState);
		if (_lobby.CheckSendToNetwork()) {
			//agafem el ID de missatge que te el lobby i l'enviem
			_Client->CreateLobbyMessage(_lobby.GetMessageToSend());
		}
		_fpsLimiter.forceSynchronization();
		_Client->Update();
	}
	_Client->SendHelloBoolInit = true;
	//System initializations
	initSystems();
	//Start the game if all the elements are ready
	gameLoop();
}

/*
* Initializes all the game engine components
*/
void Game::initSystems() {
		//Create an Opengl window using SDL
	_window.create(_windowTitle, _screenWidth, _screenHeight, 0);		
		//Compile and Link shader
	initShaders();
		//Set up the openGL buffers
	_openGLBuffers.initializeBuffers(_colorProgram);
		//Load the current scenario
	_gameElements.createCube(BLUE_CUBE, glm::vec3(0, 0, 255));
	_gameElements.createCube(RED_CUBE, glm::vec3(255, 0, 0));
	_gameElements.createCube(WHITE_CUBE, glm::vec3(255, 255, 255));
	//_gameElements.createPyramid(GREEN_PYRAMID, glm::vec3(255, 255, 255));
	_gameElements.createPlane(PLANE, glm::vec3(200, 200, 200));
	_gameElements.LoadASE();
	_gameElements.createCube(7, glm::vec3(255, 255, 255));
	_gameElements.loadGameElements("./resources/scene2D.txt");
	Panzer.initTank(_gameElements.getGameElement(TOP_TANK)._translate, _gameElements.getGameElement(BOTTOM_TANK)._translate, _gameElements.getGameElement(TOP_TANK)._angle, _gameElements.getGameElement(BOTTOM_TANK)._angle, 5, 0);
	loadGameTextures();
	_materialManager.createMaterialDefinitions();
	
	//inicialitzem la camera
	_camera.orthographic(); 

	//init SpawnPoints
	SpawnPoint sp;
	sp._point = glm::vec3(-0.25f, 0.25f, 0.0f);
	SpawnPoints.push_back(sp);
	sp._point = glm::vec3(-5.75f, 0.25f, 0.0f);
	SpawnPoints.push_back(sp);
	sp._point = glm::vec3(-0.25f, 5.75f, 0.0f);
	SpawnPoints.push_back(sp);
	sp._point = glm::vec3(-5.75f, 5.75f, 0.0f);
	SpawnPoints.push_back(sp);

	//inicialitzem les llums
	initLights();

	AddMovement();
	//InitNetwork
	//_Client->InitConnection(&socket);
}

/*
* Initialize the shaders:
* Compiles, sets the variables between C++ and the Shader program and links the shader program
*/
void Game::initShaders() {
		//Compile the shaders
	_colorProgram.addShader(GL_VERTEX_SHADER, "./resources/shaders/vertex-shader.txt");
	_colorProgram.addShader(GL_FRAGMENT_SHADER, "./resources/shaders/fragment-shader.txt");
	_colorProgram.compileShaders();
	
		//Attributes must be added before linking the code
	_colorProgram.addAttribute("vertexPositionGame");
	_colorProgram.addAttribute("vertexColor");
	_colorProgram.addAttribute("vertexUV");
	_colorProgram.addAttribute("vertexNormal");

		//Link the compiled shaders
	_colorProgram.linkShaders();
		//Bind the uniform variables. You must enable shaders before gettting the uniforme variable location
	_colorProgram.use();

	_textureDataLocation = _colorProgram.getUniformLocation("textureData");
	_textureScaleFactorLocation = _colorProgram.getUniformLocation("textureScaleFactor");

	//Set up the uniform material variables
	_materialAmbientUniform = _colorProgram.getUniformLocation("material.ambient");
	_materialDiffuseUniform = _colorProgram.getUniformLocation("material.diffuse");
	_materialSpecularUniform = _colorProgram.getUniformLocation("material.specular");
	_materialShininessUniform = _colorProgram.getUniformLocation("material.shininess");

	_colorProgram.unuse();
}
/*
funcio per a inicialitzar totes les llums
*/
void Game::initLights(){
	material currentMaterial = _materialManager.getMaterialComponents(3);
	Light tempLight;
	tempLight.setPosition(glm::vec3(-0.3f, -0.3f, 0.5f));
	tempLight.setDirection(glm::vec3(0.0f, 0.0f, -1.0f));
	tempLight.setCutOff(25.5f);
	tempLight.setType(DIRLIGHT);
	tempLight.setLinear(0.09f);
	tempLight.setConstant(1.0f);
	tempLight.setQuadratic(0.032f);
	tempLight.setAmbient(glm::vec3(6.0f,6.0f, 6.0f));
	tempLight.setDiffuse(currentMaterial.diffuse);
	tempLight.setSpecular(currentMaterial.specular);
	tempLight.setIsOn(true);

	//_lights.push_back(tempLight);
	
	tempLight.setPosition(glm::vec3(-0.7f, -0.7f, 0.5f));
	tempLight.setDirection(glm::vec3(0.0f, -1.0f, -1.0f));
	tempLight.setCutOff(25.5f);
	tempLight.setType(POINTLIGHT);
	tempLight.setLinear(0.09f);
	tempLight.setConstant(1.0f);
	tempLight.setQuadratic(0.032f);
	tempLight.setAmbient(currentMaterial.ambient);
	tempLight.setDiffuse(currentMaterial.diffuse);
	tempLight.setSpecular(currentMaterial.specular);
	tempLight.setIsOn(true);
	//_lights.push_back(tempLight);
	
	
	tempLight.setPosition(glm::vec3(-2.4f, 3.0f, 0.5f));
	tempLight.setDirection(glm::vec3(0.0f, 0.0f, -1.0f));
	tempLight.setCutOff(550.0f);
	tempLight.setType(SPOTLIGHT);
	tempLight.setLinear(0.09f);
	tempLight.setConstant(1.0f);
	tempLight.setQuadratic(0.032f);
	tempLight.setAmbient(currentMaterial.ambient);
	tempLight.setDiffuse(currentMaterial.diffuse);
	tempLight.setSpecular(currentMaterial.specular);
	tempLight.setIsOn(true);
	_lights.push_back(tempLight);
	
	
}

/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {	
	_gameState = GameState::PLAY;
	while (_gameState != GameState::EXIT) {		
			//Start synchronization between refresh rate and frame rate
		//_tp.SemiFixedTimeStep();
		//funciona pero ens fiem mes del _fpsLimiter

		_fpsLimiter.startSynchronization();
			//Update the BB
		_gameElements.UpdateBB();
			//Process the input information (keyboard and mouse)
		processInput();
			//Execute the player actions (keyboard and mouse)
		executePlayerCommands();
			//Update the game status
		doPhysics();
			//Draw the objects on the screen
		renderGame();
			//Spawn enemies
		UpdateEnemyMovements();

		EndGame();

		_Client->Update();
		CheckEndGame();
		//Force synchronization
		_fpsLimiter.forceSynchronization();

		
	}
}

/*
* Processes input with SDL
*/
void Game::processInput() {
	_inputManager.update();
	//Review https://wiki.libsdl.org/SDL_Event to see the different kind of events
	//Moreover, table show the property affected for each event type
	SDL_Event evnt;
	//Will keep looping until there are no more events to process
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			_inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
			break;
		case SDL_KEYDOWN:
			_inputManager.pressKey(evnt.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.releaseKey(evnt.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			_inputManager.pressKey(evnt.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.releaseKey(evnt.button.button);
			break;
		default:
			break;
		}
	}
}

bool Game::collisionBala(glm::vec3 movement, int i) {
	//calculem la BB en la posicio a on es moura el objecte
	glm::mat4 scaleMatrix, noRotateMatrix;
	glm::vec3 tempTrans = _gameElements.getGameElement(i)._translate + movement;
	noRotateMatrix = glm::translate(noRotateMatrix, tempTrans);

	scaleMatrix = glm::scale(scaleMatrix, _gameElements.getGameElement(i)._scale);
	noRotateMatrix = glm::scale(noRotateMatrix, _gameElements.getGameElement(i)._scale);
	
	//posicions al mon del objecte
	glm::vec4 centre = noRotateMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OCentre, 1);
	glm::vec4 extent = scaleMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OExtent, 1);

	glm::vec4 min = noRotateMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OMin, 1);
	glm::vec4 max = noRotateMatrix*glm::vec4(_gameElements.getGameElement(i)._boundingAABB.OMax, 1);

	//asignem valors a les BB segons els calculs
	_gameElements.getGameElement(i)._boundingAABB.Centre.x = centre.x;
	_gameElements.getGameElement(i)._boundingAABB.Centre.y = centre.y;
	_gameElements.getGameElement(i)._boundingAABB.Centre.z = centre.z;

	_gameElements.getGameElement(i)._boundingAABB.Min.x = min.x;
	_gameElements.getGameElement(i)._boundingAABB.Min.y = min.y;
	_gameElements.getGameElement(i)._boundingAABB.Min.z = min.z;

	_gameElements.getGameElement(i)._boundingAABB.Max.x = max.x;
	_gameElements.getGameElement(i)._boundingAABB.Max.y = max.y;
	_gameElements.getGameElement(i)._boundingAABB.Max.z = max.z;


	_gameElements.getGameElement(i)._boundingAABB.Extent.x = extent.x;
	_gameElements.getGameElement(i)._boundingAABB.Extent.y = extent.y;
	_gameElements.getGameElement(i)._boundingAABB.Extent.z = extent.z;

	for (int z = 0; z < _gameElements.getNumGameElements();z++) {
		//la primera part de l'if es per les col�lisions amb la bala del jugador
		if (_gameElements.getGameElement(z)._objectType != 4 && _gameElements.getGameElement(z)._objectType != 5 && _gameElements.getGameElement(i)._objectType != 1) {
			if (z != i) {
				if (_gameElements.getGameElement(i).OverlapAABB(_gameElements.getGameElement(z)._boundingAABB)) {
					//Aixo pot donar problemes en cas de que fem couts per a comprobar collisions.
					if (_gameElements.getGameElement(z)._objectType == 6) {
						//AddScore(z);
						//_gameElements.removeLastElement(z - 1); no cal per que hi ha un if al update que elimina la bala
						Panzer.setCanMove(true);
					}
					else {
						if(_gameElements.getGameElement(i)._objectType==6)_gameElements.removeLastElement(i);
						Panzer.setCanMove(true);
					}
					return true;
				}
			}
		}
		else {//la segona part de l'if es per a la bala dels enemics
			if (_gameElements.getGameElement(i)._objectType == 1) {
				if (z != i) {
					if (_gameElements.getGameElement(i).OverlapAABB(_gameElements.getGameElement(z)._boundingAABB)) {
						if (_gameElements.getGameElement(z)._objectType == 4 || _gameElements.getGameElement(z)._objectType == 5) {
							//if (Panzer.getCanMove())score--;
						}
						if (_gameElements.getGameElement(z)._objectType != 6) {
							if (_gameElements.getGameElement(i)._objectType == 1)_gameElements.removeLastElement(i);
							return true;
						}
						return false;
					}
				}
			}
		}

	}
	//al no colisionar podem deixar la BB com a la de seguent pos.
	return false;
}

/*
* Executes the actions sent by the user by means of the keyboard and mouse
*/
void Game::executePlayerCommands() {
	if (_inputManager.isKeyPressed(SDL_BUTTON_LEFT)) {
		Shooted = true;
		if (Panzer.canFire()) {
			_Client->SendShot();
			Panzer.SHOOT = true;
			Panzer.Fire();
			_gameElements.Bullet(_textureManager);
			_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._translate = _gameElements.getGameElement(TOP_TANK)._translate + glm::vec3(0.0f, 0.0f, 0.125f);
			_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._scale = glm::vec3(0.02f, 0.02f, 0.02f);
		}
		
	}
	//aix� seria per gestionar si s'ha disparat una bala
	for (int x = 0; x < _Enemies.size(); x++) {
		if (_Enemies[x].getShoot()) {
			//aix� voldr� dir que el enemic ha de disparar, per tant ja veure com ho fem
			for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
				if (_gameElements.getGameElement(i)._ID == _Enemies[x].GetID()) {
					_gameElements.BulletEnemy(_textureManager);
					_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._translate = _gameElements.getGameElement(i)._translate + glm::vec3(0.0f, 0.0f, 0.15f);
					_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._angle = _Enemies[x].getAngleBOTTOM();
					_Enemies[x].setShoot(false);
				}

			}
		}
	}

	///moviment del tanc
	if (TurnMovement) {
		if (_inputManager.isKeyDown(SDLK_w)) {
			if (Panzer.getCanMove()) {
				glm::vec3 movement = glm::vec3(glm::sin(glm::radians(Panzer.getAngleBOTTOM()))*0.015f, -(glm::cos(glm::radians(Panzer.getAngleBOTTOM())))*0.015f, 0);
				//if (!_gameElements.collision(movement)) {
					Panzer.moveTank((glm::sin(glm::radians(Panzer.getAngleBOTTOM())))*0.015f, -(glm::cos(glm::radians(Panzer.getAngleBOTTOM())))*0.015f);
					moveTank();
					if (_Movements.size() == 0)
						AddMovement();
					_Movements[_Movements.size() - 1].AddMove(0);
				//}
			}
		}

		else if (_inputManager.isKeyDown(SDLK_d)) {
			if (Panzer.getCanMove()) {
				Panzer.rotateTank(-2);
				moveTank();
				if (_Movements.size() == 0)
					AddMovement();
				_Movements[_Movements.size() - 1].AddMove(2);
			}
		}
		else if (_inputManager.isKeyDown(SDLK_a)) {
			if (Panzer.getCanMove()) {
				Panzer.rotateTank(2);
				moveTank();
				if (_Movements.size() == 0)
					AddMovement();
				_Movements[_Movements.size() - 1].AddMove(3);
			}
		}

		else if (_inputManager.isKeyDown(SDLK_s)) {
			if (Panzer.getCanMove()) {
				glm::vec3 movement = glm::vec3(-glm::sin(glm::radians(Panzer.getAngleBOTTOM()))*0.015f, (glm::cos(glm::radians(Panzer.getAngleBOTTOM())))*0.015f, 0);
				//if (!_gameElements.collision(movement)) {
					Panzer.moveTank(-(glm::sin(glm::radians(Panzer.getAngleBOTTOM())))*0.015f, (glm::cos(glm::radians(Panzer.getAngleBOTTOM())))*0.015f);
					moveTank();
					if (_Movements.size()==0)
						AddMovement();
					_Movements[_Movements.size() - 1].AddMove(1);
				//}
			}
		}

	}
	//Gir de la torreta
	//De moment comentat ja es veura si s'implementa, perque els models dels enemics s�n tot un
	/*
	if (TurnMovement) {
		if (_inputManager.isKeyDown(SDLK_q)) {
			if (Panzer.getCanMove()) {
				Panzer.setAngleTOP(2);
				moveTank();

			}
		}

		if (_inputManager.isKeyDown(SDLK_e)) {
			if (Panzer.getCanMove()) {
				Panzer.setAngleTOP(-2);
				moveTank();
			}
		}
	}*/
	//Canvia el tipus de llum de la primera llum
	if (_inputManager.isKeyDown(SDLK_l)) {
		if (_lights[0].getType() == SPOTLIGHT) {
			_lights[0].setAmbient(glm::vec3(6.0f, 6.0f, 6.0f));
			_lights[0].setType(DIRLIGHT);
		}
		else if (_lights[0].getType() == DIRLIGHT) {
			_lights[0].setAmbient(_materialManager.getMaterialComponents(3).ambient);
			_lights[0].setType(POINTLIGHT);
		}
		else {
			_lights[0].setAmbient(_materialManager.getMaterialComponents(3).ambient);
			_lights[0].setType(SPOTLIGHT);
		}	
	}

	if (_inputManager.isKeyPressed(SDLK_ESCAPE)) {
		_gameState = GameState::EXIT;
	}
	//Cameras
	//Camera que te la perspectiva desde adalt, aquesta es fara servir per a que el jugador faci el moviment
	if (_inputManager.isKeyPressed(SDLK_1)) {
		FPCamera = false;
		TPCamera = false;
		_camera.perpespectiveMatrix();
		_camera.changePos(glm::vec3(-3.0f, 4.0f, 8.0f), glm::vec3(-3.0f, 2.5f, -8.0f));
	}

	if (_inputManager.isKeyPressed(SDLK_3)) {
		TPCamera = !TPCamera;
		FPCamera = false;
		if (!TPCamera) {
			_camera.perpespectiveMatrix();
			_camera.changePos(glm::vec3(2.0f, 3.0f, 4.0f), glm::vec3(0.0f, 0.0f, 0.0f));
		}
	}
	if (FPCamera) {
		FPCamera = false;
		TPCamera = false;
		_camera.perpespectiveMatrix();
		_camera.changePos(glm::vec3(-3.0f, 4.0f, 8.0f), glm::vec3(-3.0f, 2.5f, -8.0f));
	}

	if (TPCamera) {
		float newX = sin(glm::radians(-_gameElements.getGameElement(TOP_TANK)._angle - 180));
		float newY = cos(glm::radians(-_gameElements.getGameElement(TOP_TANK)._angle - 180));
		_camera.perpespectiveMatrix();
		_camera.changePos(_gameElements.getGameElement(TOP_TANK)._translate + glm::vec3(-newX, -newY, 0.50f), _gameElements.getGameElement(TOP_TANK)._translate + glm::vec3(0.0f, 0.0f, 0.0f) + glm::vec3(newX, newY, 0.0f));

	}
}

void Game::moveTank() {
	//Actualitzem la posici� del tank
	_gameElements.getGameElement(TOP_TANK)._angle = Panzer.getAngleTOP();
	_gameElements.getGameElement(TOP_TANK)._rotation.z = 1.0f;
	_gameElements.getGameElement(BOTTOM_TANK)._angle = Panzer.getAngleBOTTOM();
	_gameElements.getGameElement(BOTTOM_TANK)._rotation.z = 1.0f;
	_gameElements.getGameElement(TOP_TANK)._translate = Panzer.getPositionTOP();
	_gameElements.getGameElement(BOTTOM_TANK)._translate = Panzer.getPositionBOTTOM();
	CheckCell();

}
/*
funcio que invoca els tanks i els hi assigna una ID
*/


float Game::ConvertGridToPositionY(float _X) {
	return (_X / 2)+0.25f;
}

float Game::ConvertGridToPositionX(float _X) {
	return -(_X / 2)-0.25f;
}

void Game::AddMovement(){
	MovementPlayer Mp;
	Mp.AddIDMove(Moves);
	Moves++;
	//if (_Movements.size()<5) {
		//_Movements.push_back(Mp);
	//}
	//else {
		//mirem si tenim gaires acumulats i eliminem el primer
		//_Movements.erase(_Movements.begin());
	_Movements.push_back(Mp);
	//}
}

void Game::EraseMovements(int IDMove){
	//std::cout << "Eliminem amb la id " << IDMove << std::endl;
	for (int i = 0; i < _Movements.size(); i++) {
		if (_Movements[i].getID() <= IDMove) {
			_Movements.erase(_Movements.begin() + i);
		}
	}
}

void Game::CheckEndGame(){
	if (_renderState == RenderState::WIN) {
		system("Pause");
		Ranking();
	}
	else if (_renderState == RenderState::LOSE) {
		system("Pause");
		Ranking();
	}
	else if (_renderState == RenderState::RANKING) {
		system("Pause");
		_gameState = GameState::EXIT;
	}
}


void Game::SpawnEnemy(int ID, int PosX, int PosY){

	TankEnemy Tank;
	glm::vec3 _pos = glm::vec3(ConvertGridToPositionX(glm::abs(PosX)), ConvertGridToPositionY(glm::abs(PosY)), 0.0f);
	Tank.InitTankEnemy(_pos, _pos, 0, 0, 5, 0, PosX, PosY);
	//aqui la i hauria de ser la ID
	_gameElements.SpawnEnemy(_pos, _textureManager, ID);
	//Tank.InitMovement();
	Tank.SetID(ID);
	_Enemies.push_back(Tank);

	//REVISAR assignar una ID correcte
	//REVISAR el sistema d'spawneig
	/*for (unsigned int i = 0; i < SpawnPoints.size(); i++) {
		if (i + 1 != ID) {
			TankEnemy Tank;
			Tank.InitTankEnemy(SpawnPoints[i]._point, SpawnPoints[i]._point, 0, 0, 5, 0, ConvertPositionToGridX(glm::abs(SpawnPoints[i]._point.x)), ConvertPositionToGridX(SpawnPoints[i]._point.y));
			_gameElements.SpawnEnemy(SpawnPoints[i]._point, _textureManager, i + 1);
			Tank.SetID(i + 1);
			_Enemies.push_back(Tank);
		}
	}*/

}


//comen�em el torn
void Game::InitTurn(){
	if (!isinTurn) {
		TurnMovement = true;
		GameTurn = std::clock();
		std::cout << "START GAME!!" << std::endl;
		Panzer.UpdateLastPos();
		//Cell cell;
		//cell.setCellPosition(Panzer.getCellX(), Panzer.getCellY());
		//Panzer.TempsCells.push_back(cell);
	}
}

//fi del joc
void Game::EndServerGame(bool win){
	if (win) {
		_renderState = RenderState::WIN;
	}
	else {
		_renderState = RenderState::LOSE;
	}
	renderGame();
}

//funci� per a la desconexio i perdua amb el servidor
void Game::Disconnected(){
	std::cout << "You have lost the connection to the server" << std::endl;
	system("Pause");
	_gameState = GameState::EXIT;
}

void Game::Ranking(){
	//std::cout << "Your Score: " << score << std::endl;
	for (int i = 0; i < _Enemies.size(); i++) {
		_ScoreEnemies.push_back(_Enemies[i].getScore());
		_IDEnemies.push_back(_Enemies[i].GetID());
	}
	_renderState = RenderState::RANKING;
	//system("Pause");
	//_gameState = GameState::EXIT;
}




void Game::EndGame(){
	/*if (TurnMovement) {
		if (std::clock() - GameTurn > Time) {
			//aqui canviariem a attack
			TurnMovement = false;
			GameTurn = std::clock();
			TurnAttack = true;
		}
	}
	else if (TurnAttack) {
		if (std::clock() - GameTurn > Time) {
			//aqui ja acabariem i enviariem dades	
			isinTurn = false;
			TurnAttack = false;
			std::cout << "End of Turn" << std::endl;
		}
	}*/
}
/*
funcio per afegir el score, que a part d'aix� eliminem el enemic que ha sigut derrotat i li diem al spawn point que ha quedat lliure
*/
void Game::AddScore(int _i){
	/*for (unsigned int i = 0; i < SpawnPoints.size(); i++) {
		if (SpawnPoints[i]._point.x == _gameElements.getGameElement(_i)._translate.x) {
			SpawnPoints[i]._spawned = false;
			spawntime = std::clock();
		}
	}
	_gameElements.removeLastElement(_i);*/
	//REVISAR, aqui a partir de l'id del gamelements, podem extreure la id del tanc amb el qual s'ha col�lisionat i avisar
	//aqui hauriem de vigilar el del server i lo que sea
	score++; //el score l'ha de gestionar el servidor suposu
	//per tant el client nom�s hauria d'eliminar la bala, el score que es gestioni en el servidor
}


void Game::loadGameTextures(){
	GameObject currentGameObject;
	//Load the game textures			
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		currentGameObject = _gameElements.getGameElement(i);

		(_gameElements.getGameElement(i))._textureID = _textureManager.getTextureID(currentGameObject._textureFile);

	}
}

void Game::UpdateEnemyMovements(){
	//Aix� es per moure els enemics//de moment nomes es el primer perque sino fara patapum
	for (int i = 0; i < _Enemies.size(); i++) {
		_Enemies[i].UpdateMovement();
	}

	//moviment del objecte
	int position = 0;
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		if (_gameElements.getGameElement(i)._objectType == 6) {
			if (_Enemies.size() > 0){
				if (_Enemies[position].GetID() == _gameElements.getGameElement(i)._ID){
					_gameElements.getGameElement(i)._angle = _Enemies[position].getAngleBOTTOM();
					_gameElements.getGameElement(i)._rotation.z = 1.0f;
					_gameElements.getGameElement(i)._translate = _Enemies[position].getPositionTOP();
					//std::cout << "posicion del enemigo" << _Enemies[position].getPositionTOP().x << " - " << _Enemies[position].getPositionTOP().y << std::endl;
				}
				if(_Enemies.size()<position+1)position++;
			}
		}
	}
}

void Game::CheckCell(){
	//std::cout << Panzer.getPositionBOTTOM().y << "\n";
	bool change = false;
	if (Panzer.getPositionBOTTOM().x < -(Panzer.getCellX()*0.5f) - 0.5f) {
		//Aix� vol dir que ha passat a un cel�la de X+1
		Panzer.setCellX(Panzer.getCellX() + 1);
		change = true;
	}
	else if (Panzer.getPositionBOTTOM().x > -(Panzer.getCellX()*0.5f)) {
		//Aix� voldr� dir que ha passat a una cel�la de X-1
		Panzer.setCellX(Panzer.getCellX() - 1);
		change = true;
	}
	if (Panzer.getPositionBOTTOM().y > Panzer.getCellY()*0.5f + 0.5f) {
		//aix� vol dir que s'ha tirat cap avall
		Panzer.setCellY(Panzer.getCellY() + 1);
		change = true;
	}
	else if (Panzer.getPositionBOTTOM().y < Panzer.getCellY()*0.5f) {
		//aix� vol dir que s'ha tirat cap amunt
		Panzer.setCellY(Panzer.getCellY() - 1);
		change = true;
	}
	//si hi ha un canvi afegim la nova cel�la al vector, despr�s el que haurem de passar es el vector aquest de cel�les
	if (change) {
		Cell cell;
		cell.setCellPosition(Panzer.getCellX(), Panzer.getCellY());
		Panzer.TempsCells.push_back(cell);
	}

}

int Game::ConvertPositionToGridX(float _X)
{
	return glm::floor(_X*2);;
}

/*
* Update the game objects based on the physics
*/
void Game::doPhysics() {
	/*
	funcio que fa moure la bala i a traves de la funcio collision bala comprova si en el seguent pas colisionar o no
	*/
	if (!Panzer.canFire()) {//voldra dir que s'ha disparat la bala
		for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
			if (_gameElements.getGameElement(i)._objectType == BULLETOBJECT) {
				//Calcul per a la orientacio de la camara per a poder reposicionarla per a seguir la bala
				float newX = sin(glm::radians(-Panzer.getAngleBala() - 180));
				float newY = cos(glm::radians(-Panzer.getAngleBala() - 180));
				//canviem la pos de la camera al radere de la bala per a poder seguirla
				//_camera.changePos(_gameElements.getGameElement(i)._translate - glm::vec3(newX*0.25f, newY*0.25f, 0.0f), _gameElements.getGameElement(i)._translate + glm::vec3(0.0f, 0.0f, 0.0f) + glm::vec3(newX, newY, 0.0f));
				glm::vec3 move = glm::vec3(glm::sin(glm::radians(Panzer.getAngleBala()))*0.04f, -glm::cos(glm::radians(Panzer.getAngleBala()))*0.04f, 0.0f);
				if (!collisionBala(move, i)) {
					_gameElements.getGameElement(i)._translate.x += glm::sin(glm::radians(Panzer.getAngleBala()))*0.06f;
					_gameElements.getGameElement(i)._translate.y -= glm::cos(glm::radians(Panzer.getAngleBala()))*0.06f;
				}
			}
		}
	}
		//REVISAR
	if (Panzer.ShotActive()) {
		for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
			if (_gameElements.getGameElement(i)._objectType == BULLETOBJECT) {
				_gameElements.removeLastElement(i);
				Panzer.setCanMove(true);
			}
		}
	} 
	/*
		if que fa que els enemics disparin, es un sistema una mica semblant al spawneig de monstres, cada 2 segons un tanc dispara i es tria
		random quin es el tanc que ho fara. Passa el mateix que en la funcio d'spawn, poques vegades hi haur� dos o mes tancs.
		Quan es dispari se li assignara el angle a la bala perque despres vagi en la direccio adequada
	*/
	/*if (std::clock() - shottime > 2000) {
		int shotnum = rand() % 4;
		if (SpawnPoints[shotnum]._spawned) {
			_gameElements.BulletEnemy(_textureManager);
			_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._translate = SpawnPoints[shotnum]._point + glm::vec3(0.0f, 0.0f, 0.15f);
			float newXX = _gameElements.getGameElement(TOP_TANK)._translate.x - _gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._translate.x;
			float newYY = _gameElements.getGameElement(TOP_TANK)._translate.y - _gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._translate.y;
			float anglebala = glm::atan(newYY, newXX);
			anglebala = anglebala * 180.0f / 3.14f;
			_gameElements.getGameElement(_gameElements.getNumGameElements() - 1)._angle = anglebala + 90.0f;
			shottime = std::clock();
		}
	}*/
	//for que fa moure totes les bales, que tenen el type red_cube, les bales dels enemics s�n mes rapides que les del jugador, shit happens
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		if (_gameElements.getGameElement(i)._objectType == RED_CUBE) {
			glm::vec3 move = glm::vec3(glm::sin(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f, -glm::cos(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f, 0.0f);
			if (!collisionBala(move, i)) {
				_gameElements.getGameElement(i)._translate.x += glm::sin(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f;
				_gameElements.getGameElement(i)._translate.y -= glm::cos(glm::radians(_gameElements.getGameElement(i)._angle))*0.06f;
			}
		}
	}

}

/**
* Draw the sprites on the screen
*/
void Game::renderGame() {
		//Temporal variable
	GameObject currentRenderedGameElement;

		//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//Bind the GLSL program. Only one code GLSL can be used at the same time
	_colorProgram.use();

	std::string _score;
	_score = "Score: " + std::to_string(score);
	if(_renderState== RenderState::GAME)_textManager.renderTextOnScreen(_score, _openGLBuffers, _colorProgram, _gameElements.getData(PLANE), glm::vec3(0.0,0.86,0.0), glm::vec3(0.4, 0.2, 1.0));
	else if(_renderState == RenderState::WIN)_textManager.renderTextOnScreen("YOU WIN!!", _openGLBuffers, _colorProgram, _gameElements.getData(PLANE), glm::vec3(0.0, 0.0, 0.0), glm::vec3(1.6, 0.8, 1.0));
	else if (_renderState == RenderState::LOSE) _textManager.renderTextOnScreen("YOU LOSE!!", _openGLBuffers, _colorProgram, _gameElements.getData(PLANE), glm::vec3(0.0, 0.0, 0.0), glm::vec3(1.6, 0.8, 1.0));
	else if (_renderState == RenderState::RANKING) {
			
		_textManager.renderTextOnScreen("Carmen: " + std::to_string(score), _openGLBuffers, _colorProgram, _gameElements.getData(PLANE), glm::vec3(0.0, 0.5, 0.0), glm::vec3(0.8, 0.25, 1.0));
		_textManager.renderTextOnScreen("Player" + std::to_string(_IDEnemies[0]) +  ": " + std::to_string(_ScoreEnemies[0]), _openGLBuffers, _colorProgram, _gameElements.getData(PLANE), glm::vec3(0.0, 0.25, 0.0), glm::vec3(0.8, 0.25, 1.0));
		if(_IDEnemies.size()>1)_textManager.renderTextOnScreen("Player" + std::to_string(_IDEnemies[1]) + ": " + std::to_string(_ScoreEnemies[1]), _openGLBuffers, _colorProgram, _gameElements.getData(PLANE), glm::vec3(0.0, 0.25, 0.0), glm::vec3(0.8, 0.25, 1.0));
		if (_IDEnemies.size()>2)_textManager.renderTextOnScreen("Player" + std::to_string(_IDEnemies[2]) + ": " + std::to_string(_ScoreEnemies[2]), _openGLBuffers, _colorProgram, _gameElements.getData(PLANE), glm::vec3(0.0, 0.25, 0.0), glm::vec3(0.8, 0.25, 1.0));
	}
	//s'ha fet el pintat de text en una funcio externa per evitar haver de fer el render game mes gran
	GLuint viewMatrixUniform = _colorProgram.getUniformLocation("viewMatrix");
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, glm::value_ptr(_camera._viewMatrix));

	GLuint projectionMatrixUniform = _colorProgram.getUniformLocation("projectionMatrix");
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(_camera._projectionMatrix));
	glActiveTexture(GL_TEXTURE0);
	
	//For each one of the elements: Each object MUST BE RENDERED based on its position, rotation and scale data
	
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {	

		currentRenderedGameElement = _gameElements.getGameElement(i);	
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		
		glm::mat4 modelMatrix;
		modelMatrix = glm::translate(modelMatrix, currentRenderedGameElement._translate);

		if (currentRenderedGameElement._angle != 0) {
			modelMatrix = glm::rotate(modelMatrix, glm::radians(currentRenderedGameElement._angle), currentRenderedGameElement._rotation);
		}
		modelMatrix = glm::scale(modelMatrix, currentRenderedGameElement._scale);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, currentRenderedGameElement._textureID);
		
		//fer que si un es mes gran que l'altre s'equiparin, per aixi fer que la textura quedi igual(?)
		float terme1;
		if (currentRenderedGameElement._scale.x > currentRenderedGameElement._scale.y) {
			terme1 = currentRenderedGameElement._scale.x;
		}
		else {
			terme1 = currentRenderedGameElement._scale.y;
		}
		
		if(currentRenderedGameElement._objectType==RED_CUBE && currentRenderedGameElement._objectType==6)glUniform2f(_textureScaleFactorLocation, 1.0f, 1.0f);
		else glUniform2f(_textureScaleFactorLocation, terme1, currentRenderedGameElement._scale.z);
		
		//Bind the uniform variables with the contet
		material currentMaterial;
		currentMaterial = _materialManager.getMaterialComponents(currentRenderedGameElement._materialType);
		glUniform3fv(_materialAmbientUniform, 1, glm::value_ptr(currentMaterial.ambient));
		glUniform3fv(_materialDiffuseUniform, 1, glm::value_ptr(currentMaterial.diffuse));
		glUniform3fv(_materialSpecularUniform, 1, glm::value_ptr(currentMaterial.specular));
		glUniform1f(_materialShininessUniform, currentMaterial.shininess);
		
		//for que recorre el vector de llums i assigna els valors de cada llum al array del shader per a fer el calcul de multiples llums
		for (int i = 0; i < (int)_lights.size();i++) {
			//Set up the uniform light variables 
			_lightAmbientUniform = _colorProgram.getUniformLocation("pointLights["+std::to_string(i) + "].ambient");
			_lightDiffuseUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].diffuse");
			_lightSpecularUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].specular");
			_lightTypeUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].type");
			_lightDirUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].direction");
			_lightCutOffUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].cutOff");
			_lightConstantUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].constant");
			_lightLinearUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].linear");
			_lightQuadraticUniform = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].quadratic");
			_lightPosition = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].position");
			_lightOn = _colorProgram.getUniformLocation("pointLights["+ std::to_string(i) + "].isOn");

			//Bind the uniform variables with the contet of the light material
			glUniform3fv(_lightAmbientUniform, 1, glm::value_ptr(_lights[i].getAmbient()));
			glUniform3fv(_lightDiffuseUniform, 1, glm::value_ptr(_lights[i].getDiffuse()));
			glUniform3fv(_lightSpecularUniform, 1, glm::value_ptr(_lights[i].getSpecular()));
			glUniform1i(_lightTypeUniform, _lights[i].getType());
			glUniform3fv(_lightDirUniform, 1, glm::value_ptr(_lights[i].getDirection()));
			glUniform1f(_lightCutOffUniform, glm::cos(glm::radians(_lights[i].getCutOff())));
			glUniform1f(_lightConstantUniform, _lights[i].getConstant());
			glUniform1f(_lightLinearUniform, _lights[i].getLinear());
			glUniform1f(_lightQuadraticUniform, _lights[i].getQuadratic());
			glUniform3fv(_lightPosition, 1, glm::value_ptr(_lights[i].getPosition()));
			if (_lights[i].getIsOn())glUniform1i(_lightOn, 1);
			else glUniform1i(_lightOn, 0);
		}
		GLint textureLocation = _colorProgram.getUniformLocation("textureData");
		glUniform1f(textureLocation, 0);

		GLuint modelMatrixUniform = _colorProgram.getUniformLocation("modelMatrix");
		glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));

		//aixo es per si el objecte actual li afecta la llum de l'escenari o no 
		GLint lightEffect = _colorProgram.getUniformLocation("affectedByLight");
		glUniform1i(lightEffect, 1);
		
		//Passem la posicio de la camera per a fer els calculs necesaris
		GLuint _viewerPosition = _colorProgram.getUniformLocation("viewerPosition");
		glUniform3fv(_viewerPosition, 1, glm::value_ptr(_camera.getCamPos()));

		_openGLBuffers.sendDataToGPU(_gameElements.getData(currentRenderedGameElement._objectType), _gameElements.getNumVertices(currentRenderedGameElement._objectType));

		 glBindTexture(GL_TEXTURE_2D, 0);
	}

	//Unbind the program
	_colorProgram.unuse();

	//Swap the display buffers (displays what was just drawn)
	_window.swapBuffer();
}



