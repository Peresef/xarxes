#pragma once
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <string>

#define ROCK 0
#define GOLD 1 
#define METAL 2
#define LLUM 3
#define NUMMATERIALS 4

struct material {
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	float shininess;
};

class MaterialManager{
	material _materialData[NUMMATERIALS];
	std::string _materialType[NUMMATERIALS];
public:
	MaterialManager();
	~MaterialManager();
	material getMaterialComponents(int materialID);
	int getMaterialID(std::string materialName);
	void createMaterialDefinitions();
};