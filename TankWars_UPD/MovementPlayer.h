#pragma once
#include "GLSLProgram.h"

class MovementPlayer
{

private:
	std::vector<int> _Movements;
	int _ID = 0;

	//afegirem les posicions, de moment no cal perque les defineix el server
public:
	MovementPlayer();
	~MovementPlayer();
	void AddMove(int move);
	void AddIDMove(int ID);
	int getID();
	int getSize();
	int getMove(int pos);
};

