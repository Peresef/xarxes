#include "Lobby.h"



void Lobby::InitLobby(){
	if(SQLLOGIN)_windowState = WindowState::INTRODUCEUSER;
	else _windowState = WindowState::TITLE;


	sf::Vector2i screenDimensions(_screenWidth, _screenHeight);
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Lobby");


	if (!font.loadFromFile("courbd.ttf")) {
		std::cout << "Can't load the font file" << std::endl;
	}
	//Creem la font del t�tol
	sf::Text TempTitleText(mensaje, font, 80);
	TitleText = TempTitleText;
	TitleText.setFillColor(sf::Color(0, 160, 0));
	TitleText.setStyle(sf::Text::Bold);
	//Creem la font de subtitol
	sf::Text TempSubTitleText(mensaje, font, 40);
	SubTitleText = TempSubTitleText;
	SubTitleText.setFillColor(sf::Color(0, 160, 0));
	SubTitleText.setStyle(sf::Text::Bold);
	//creem la font del text
	sf::Text TempText(mensaje, font, 15);
	Text = TempText;
	Text.setFillColor(sf::Color(0, 160, 0));
	Text.setStyle(sf::Text::Regular);
	//Separador pel xat
	sf::RectangleShape Tempseparator(sf::Vector2f(_screenWidth, 5));
	Separator = Tempseparator;
	Separator.setFillColor(sf::Color(200, 200, 200, 255));
	Separator.setPosition(0, _screenHeight-100);
	//separador pel xat vertical
	sf::RectangleShape Tempseparatorvertical(sf::Vector2f(5, _screenHeight-100));
	SeparatorVertical = Tempseparatorvertical;
	SeparatorVertical.setFillColor(sf::Color(200, 200, 200, 255));
	SeparatorVertical.setPosition(_screenWidth - 250, 0);
}

void Lobby::UpdateLobby(ProgramState &_programState){
	RenderWindow();
	CheckInput();
	if (_windowState == WindowState::EXIT)_programState = ProgramState::GAME;
}


////////RENDER//////////
void Lobby::RenderWindow(){
	//RenderTitle();
	if (_windowState == WindowState::INTRODUCEUSER)	RenderIntroduceUser(); 
	else if(_windowState == WindowState::INTRODUCEPASS)RenderIntroducePass();
	else if (_windowState == WindowState::TITLE) RenderFirstScreen();
	else if (_windowState == WindowState::INTRODUCENAME)RenderIntroduceName();
	else if (_windowState == WindowState::CHOOSEOPTIONS)RenderOptions(); 
	else if (_windowState == WindowState::CHAT)RenderChat();
	else if (_windowState == WindowState::LISTGAMES)RenderListGames();

	window.display();
	window.clear();
}
//PER PINTAR ELS TITOLS, NOM�S N'HI HA UN 
void Lobby::RenderTitle(){
	
	TitleText.setPosition(sf::Vector2f(_screenWidth/5, _screenHeight/5));
	TitleText.setString("Tank Wars");
	window.draw(TitleText);
	
}
//PER PINTAR ELS SUBTITOLS SEGONS CONVINGUI
void Lobby::RenderSubTitle(std::string _subtitle, float X, float Y){
	SubTitleText.setPosition(sf::Vector2f(X, Y));
	SubTitleText.setString(_subtitle);
	window.draw(SubTitleText);
}

void Lobby::RenderText(std::string _text, float X, float Y){
	Text.setPosition(sf::Vector2f(X, Y));
	Text.setString(_text);
	window.draw(Text);
}


void Lobby::RenderChat(){
	window.draw(Separator);
	window.draw(SeparatorVertical);
	//pintem el que estem escribint
	RenderText(mensaje, 25.0f, _screenHeight - 75.0f);
	//pintem la llista de missatge
	int position = 0;
	for (int i = _ListMensajes.size()-1; i >= 0 ; i--) {
		RenderText(_ListMensajes[position], 25.0f, _screenHeight - 150.0f - (i*35.0f));
		position++;
	}
	if(ReadyButton)
	RenderSubTitle("START", 500.0f, 625.0f);
}

void Lobby::RenderOptions(){
	RenderTitle();
	RenderSubTitle("Introduce Match Minutes", 50.0f, 300.0f);
	RenderSubTitle("2, 3 or 4", 50.0f, 350.0f);
	window.draw(Separator);
	RenderText(mensaje, 25.0f, _screenHeight - 75.0f);


}

void Lobby::RenderIntroduceName(){
	RenderTitle();
	RenderSubTitle("Introduce Name", 175.0f, 300.0f);
	window.draw(Separator);
	//pintarem la part inferior del que escribim
	RenderText(mensaje, 25.0f, _screenHeight-75.0f);
}

void Lobby::RenderIntroduceUser(){
	RenderTitle();
	RenderSubTitle("Introduce User name", 175.0f, 300.0f);
	window.draw(Separator);
	//pintarem la part inferior del que escribim
	RenderText(mensaje, 25.0f, _screenHeight - 75.0f);
}

void Lobby::RenderIntroducePass(){
	RenderTitle();
	RenderSubTitle("Introduce Pass", 175.0f, 300.0f);
	window.draw(Separator);
	//pintarem la part inferior del que escribim
	RenderText(mensaje, 25.0f, _screenHeight - 75.0f);
}



void Lobby::RenderFirstScreen(){
	RenderTitle();
	RenderSubTitle("1. CREATE GAME", 175.0f, 300.0f);
	RenderSubTitle("2. JOIN GAME", 175.0f, 400.0f);
}


void Lobby::RenderListGames(){
	RenderSubTitle("List of Games", 175.0f, 50.0f);
	//pintem la llista
	for (int i = 0; i < _ListRooms.size(); i++) {
		std::string text;
		text = std::to_string(_ListRooms[i].ID);
		text += ". " + _ListRooms[i].Name;
		text += "   " + std::to_string(_ListRooms[i].ActualPlayers) + " / " + std::to_string(_ListRooms[i].MaxPlayers);
		RenderText(text, 150.0f, 150.0f + (i*25.0f));
	}
	window.draw(Separator);
	//pintarem la part inferior del que escribim
	RenderText(mensaje, 25.0f, _screenHeight - 75.0f);
}

void Lobby::CheckInput(){
	sf::Event evento;
	sf::Vector2f MousePosition(sf::Mouse::getPosition(window));
	while (window.pollEvent(evento)) {
		switch (evento.type) {
		case sf::Event::Closed:
			window.close();
			break;
		case sf::Event::MouseButtonPressed:
			CheckReady((int)MousePosition.x, (int)MousePosition.y);
			break;
		case sf::Event::KeyPressed:

			if (evento.key.code == sf::Keyboard::Return) {
				CheckPress();
			}
			break;
		case sf::Event::TextEntered:
			if (evento.text.unicode >= 32 && evento.text.unicode <= 126){
				CheckActions((char)evento.text.unicode);
			}
			break;
		}
	}
}

void Lobby::CheckActions(char Action){
	//Pantalla de t�tol
	if (_windowState == WindowState::TITLE) {
		switch (Action) {
			case '1':
				_windowState = WindowState::INTRODUCENAME;
				break;
			case '2':
				//Demanar la llista de partides disponibles
				MessageIDSend = SENDLIST;
				Sending = true;

				break;
		}
	}
	else if (_windowState == WindowState::INTRODUCENAME) {
		//guardem la introducci� del name
		mensaje += Action;
	}
	else if (_windowState == WindowState::INTRODUCEUSER) {
		//guardem la introducci� del user name
		mensaje += Action;
	}
	else if (_windowState == WindowState::INTRODUCEPASS) {
		//guardem la introducci� del pass
		mensaje += Action;
	}
	else if (_windowState == WindowState::CHOOSEOPTIONS) {
		mensaje += Action;
	}
	else if (_windowState == WindowState::CHAT) {
		//guardem la introducci� del name
		mensaje += Action;
	}
	else if (_windowState == WindowState::LISTGAMES) {
		mensaje += Action;
	}

}



///checkejar quan en fa el enter
void Lobby::CheckPress(){
	if (_windowState == WindowState::INTRODUCENAME) {
		NameRoom = mensaje;
		_windowState = WindowState::CHOOSEOPTIONS;
		mensaje.clear();
	}
	else if (_windowState == WindowState::INTRODUCEUSER) {
		UserName = mensaje;
		_windowState = WindowState::INTRODUCEPASS;
		mensaje.clear();
	}
	else if (_windowState == WindowState::INTRODUCEPASS) {
		Pass = mensaje;
		MessageIDSend = SENDLOGIN;
		Sending = true;
		mensaje.clear();
	}
	else if (_windowState == WindowState::CHAT) {
		if (mensaje != "") {
			MessageToSend = mensaje;
			mensaje.clear();
			MessageIDSend = SENDCHATTEXT;
			Sending = true;
		}
	}
	else if (_windowState == WindowState::CHOOSEOPTIONS) {
		if (mensaje == "2") {
			MaxPlayersRoom = 2;
			MessageIDSend = SENDCREATEROOM;
			Sending = true;
		}
		else if (mensaje == "3") {
			MaxPlayersRoom = 3;
			MessageIDSend = SENDCREATEROOM;
			Sending = true;
		}
		else if (mensaje == "4") {
			MaxPlayersRoom = 4;
			MessageIDSend = SENDCREATEROOM;
			Sending = true;
		}
		mensaje.clear();
	}
	else if (_windowState == WindowState::LISTGAMES) {
		int value = std::atoi(mensaje.c_str());
		if (value < _ListRooms.size()) {
			IDRoomAcces = _ListRooms[value].ID;
			MessageIDSend = SENDENTERROOM;
			Sending = true;
		}
		mensaje.clear();
	}
}
////Aquesta funci� gestionara tot el referent al chat, cada vegada que es rebi un missatge s'assignara aqui, o quan es creii propiament tamb�
void Lobby::AddMessageChat(std::string Message){
	_ListMensajes.push_back(Message);
	
}

void Lobby::SetID(int _id){
	_ID = _id;
}

//Funci� que activa el joc despr�s d'apretar el bot� ready
void Lobby::InitGame(){
	_windowState = WindowState::EXIT;
}


//Funci� que rep l'av�s de Ready i que ja es pot activar el bot�
void Lobby::ReceiveReady(){
	ReadyButton = true;
}

//Funci� que checkeja si s'apreta el boto de ready i es correcte apretarlo
void Lobby::CheckReady(int X, int Y){
	if (ReadyButton) {
		if (X > 450 && Y > 450) {
			MessageIDSend = SENDINITGAME;
			Sending = true;
			//InitGame();
		}
	
	}
}



void Lobby::ListRooms(InputMemoryBitStream imbs){
	int ID = 0; int MaxPl = 0; int Pl = 0; std::string Name = "";
	int numberRooms = 0;
	imbs.Read(&numberRooms, 2);
	for (int i = 0; i < numberRooms; i++) {
		imbs.Read(&ID, 2);
		imbs.ReadString(&Name);
		imbs.Read(&Pl, 3);
		imbs.Read(&MaxPl, 3);
		Room tempRoom;
		tempRoom.Name = Name;
		tempRoom.ActualPlayers = Pl;
		tempRoom.ID = ID;
		tempRoom.MaxPlayers = MaxPl;
		_ListRooms.push_back(tempRoom);
	}
	_windowState = WindowState::LISTGAMES;
}





//retornara true si hi ha un missatge a enviar al networkclient
bool Lobby::CheckSendToNetwork(){
	if (Sending) {
		Sending = false;
		return true;
	}
	return false;
}



int Lobby::GetMessageToSend(){
	Sending = false;
	return MessageIDSend;
}

void Lobby::ACKLogin(bool boolean){
	if(boolean)	_windowState = WindowState::TITLE;
	else _windowState = WindowState::INTRODUCEUSER;
}



//funcio que rep la contestacio del servidor a l'hora de crear sala
void Lobby::ACKCreateRoom(bool boolean){
	if (boolean) {
		_windowState = WindowState::CHAT;
	}
	else {
		_windowState = WindowState::TITLE;
	}
}



Lobby::Lobby(int ScreenWidth, int ScreenHeight){
	_screenWidth = ScreenWidth;
	_screenHeight = ScreenHeight;

}




Lobby::~Lobby()
{
}
