#pragma once
#include "Game.h"
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

class NetworkClient{
	friend class Game;
private:
	bool Debug = false;
	bool SendHelloBool = true;
	bool SendHelloBoolInit = false;
	bool Welcomed = false;
	bool Loged = false;
	sf::UdpSocket socket;
	Game* _game;
	std::clock_t TimeHello;
	std::clock_t TimeHelloInit;
	std::clock_t TimeSendData;
	std::clock_t TimeSendShoot;
	bool exitreceive = false;
	unsigned int Move;
	std::map<int,int> players;
	int player;
	bool isInGame = false;
	std::map<int, glm::vec3> LastOkPos;

	bool ResendingShoot;
	int TimesToResend;

public:
	bool WelcomedInit = false;
	friend std::string &operator<<(std::string &out, Cell cell);
	friend std::string &operator>> (std::string & is, Cell & cell);

	void UpdatePosition(glm::vec3 pos, int angle, int IDMove, int id, bool GoodMov, std::vector<int> Movements, bool shot, bool signeX);
	void SpawnPanzer(int PosX,int PosY);
	void Update();
	void ReceiveSelfData(InputMemoryBitStream & data);
	void ReceiveShoot(InputMemoryBitStream & data);
	void SendData();
	void Send(char* data, size_t size);
	void SpawnTanks(int ID);
	void SendHello();
	void SendHelloInit();
	void CooldownSendData();
	void ReceiveNewPlayer(InputMemoryBitStream & data);
	void ReceivePing(InputMemoryBitStream & data);
	void ChangePosEnemyTank(int PosX, int PosY, int ID, int posEnem);
	void SendShot();
	void CheckSendShot();
	void CreateLobbyMessage(int ID);
	NetworkClient(Game * game);
	~NetworkClient();
};

