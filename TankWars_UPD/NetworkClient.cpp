#include "NetworkClient.h"
#include <string>

#define MAX_BYTES 1300
//Taller7 moviment
void NetworkClient::UpdatePosition(glm::vec3 pos,int angle, int IDMove, int id, bool GoodMov, std::vector<int> Movements, bool shot, bool signeX){
	//voldra dir que el moviment no es correcte i per tant li diem que s'envagi a la posici� que ens marca el server
	if (!GoodMov) {
		if (id == _game->Panzer.GetID()) {
			_game->EraseMovements(IDMove);
			glm::vec3 positionTop;
			glm::vec3 positionBottom;
			if (!signeX){
				//position top
				positionTop = pos;
				//Positionbottom
				positionBottom = pos;
			}
			else{
				positionTop = glm::vec3(-pos.x, pos.y, pos.z);
				positionBottom = glm::vec3(-pos.x, pos.y, pos.z);
			}
			//Actualitzem model
			_game->Panzer.setPositionBOTTOM(positionBottom);
			_game->Panzer.setPositionTOP(positionTop);
			_game->_gameElements.getGameElement(TOP_TANK)._angle = _game->Panzer.getAngleTOP();
			_game->_gameElements.getGameElement(TOP_TANK)._rotation.z = 1.0f;
			_game->_gameElements.getGameElement(BOTTOM_TANK)._angle = angle;
			_game->_gameElements.getGameElement(BOTTOM_TANK)._rotation.z = 1.0f;
			_game->_gameElements.getGameElement(TOP_TANK)._translate = positionTop;
			_game->_gameElements.getGameElement(BOTTOM_TANK)._translate = positionBottom;

		}
		else {
			for (int i = 0; i < _game->_Enemies.size(); i++) {
				if (_game->_Enemies[i].GetID() == id) {
					//position top
					glm::vec3 positionTop= LastOkPos[id];
					_game->_Enemies[i].setPositionTOP(positionTop);
					//Positionbottom
					glm::vec3 positionBottom = LastOkPos[id];
					_game->_Enemies[i].setPositionBOTTOM(positionBottom);
					_game->_Enemies[i].setAngBOTTOMAbs(angle);
				}
			}
		}
	}
	else {
		if (id == _game->Panzer.GetID()){
			LastOkPos[id] = _game->Panzer.getPositionBOTTOM();
		}
		for (int i = 0; i < _game->_Enemies.size(); i++) {
			if (_game->_Enemies[i].GetID() == id) {
				LastOkPos[id] = _game->_Enemies[i].getPositionBOTTOM();
			}
		}
		//hauriem de mirar si es player o enemic i a partir d'aqui simplement eliminar el moviment si es el player, o fer la execuci� d'esdeveniments
		if (id == _game->Panzer.GetID()) 
			_game->EraseMovements(IDMove);
		else {
			for (int i = 0; i < _game->_Enemies.size(); i++) {
				if (_game->_Enemies[i].GetID() == id) {
					if (!signeX){
						glm::vec3 tempPos = glm::vec3(pos.x, pos.y,pos.z);
						_game->_Enemies[i].InitMovement(Movements, tempPos, angle);
					}
					else{
						glm::vec3 tempPos = glm::vec3(-pos.x, pos.y, pos.z);
						_game->_Enemies[i].InitMovement(Movements, tempPos, angle);
					}				
					_game->_Enemies[i].setShoot(shot);
					
				}
			}
		}
	}
}

void NetworkClient::SpawnPanzer(int PosX, int PosY) {
	glm::vec3 positionTop;
	positionTop = _game->Panzer.getPositionTOP();
	positionTop.x = PosX*(-0.5f) - 0.25f;
	positionTop.y = PosY*0.5f + 0.25;
	_game->Panzer.setPositionTOP(positionTop);
	//Positionbottom
	glm::vec3 positionBottom;
	positionBottom = _game->Panzer.getPositionBOTTOM();
	positionBottom.x = PosX*(-0.5f) - 0.25f;
	positionBottom.y = PosY*0.5f + 0.25f;
	_game->Panzer.setPositionBOTTOM(positionBottom);
	//cel�les
	_game->Panzer.setCellX(PosX);
	_game->Panzer.setCellY(PosY);

	//falta pintar la nova posicio a trav�s del render game i el gameleements(actualitzar el game elements)
	//moure el model
	_game->_gameElements.getGameElement(TOP_TANK)._angle = _game->Panzer.getAngleTOP();
	_game->_gameElements.getGameElement(TOP_TANK)._rotation.z = 1.0f;
	_game->_gameElements.getGameElement(BOTTOM_TANK)._angle = _game->Panzer.getAngleBOTTOM();
	_game->_gameElements.getGameElement(BOTTOM_TANK)._rotation.z = 1.0f;
	_game->_gameElements.getGameElement(TOP_TANK)._translate = _game->Panzer.getPositionTOP();
	_game->_gameElements.getGameElement(BOTTOM_TANK)._translate = _game->Panzer.getPositionBOTTOM();

	_game->numberPlayers++;

}

//comprovem si es rep alguna cosa, i a partir d'aqui gestionem tot
void NetworkClient::Update() {
	char data[MAX_BYTES];
	std::size_t received;
	memset(data, 0, sizeof data);
	sf::IpAddress ipAddr;
	unsigned short port;
	if (socket.receive(data, MAX_BYTES, received, ipAddr, port) == sf::Socket::Done) {
		std::string DataStr(data);
		std::string DataRec = DataStr;
		std::string posX;
		std::string posY;
		std::string  msg = "";
		int TypeMissatge = 0; int IDPl = 0; int XPl = 0; int YPl = 0; //s�ha d�igualar a 0 d�un bon principi
		int boolWin= 0;
		int Shoot = 0; int IDShoot = 0;
		InputMemoryBitStream imbs(data, sizeof(data) * 8);
		OutputMemoryBitStream ombs;
		imbs.Read(&TypeMissatge, 4);
		switch (TypeMissatge) {
			case WELCOME:
				Welcomed = true;
				break;
			case INITGAME:
				_game->_lobby.InitGame();
				ombs.Write(SENDINITTURN, 4);
				Send(ombs.GetBufferPtr(), ombs.GetByteLength());
				break;
			case INITURN:
				//Comen�ar el countdown		
				_game->InitTurn(); //Li diem que pot comen�ar el torn
				TimeSendData = std::clock();
				isInGame = true;
				break;
			case SPAWNCLIENT:
				WelcomedInit = true;
				imbs.Read(&IDPl, 3);
				imbs.Read(&XPl, 4);
				imbs.Read(&YPl, 4);
				_game->Panzer.SetID(IDPl);
				SpawnPanzer(XPl, YPl);
				players[player] = IDPl;
				player++;
				_game->Panzer.UpdateLastPos();
				//sera per a spawnejar players;
				break;
			case SELFMOV:
				ReceiveSelfData(imbs);
				//aqui es faria el rebre dades del teu tank
				//ReceiveSelfData(DataStr);
				//msg = std::to_string(SENDITSFINE) + "_/";
				//Send(msg);
				break;
	
			case RECEIVESHOOT:
				ReceiveShoot(imbs);
				break;
			case ENDGAME:
				//Donem avis de que s'ha acabat el game
				imbs.Read(&boolWin, 1);
				if (boolWin == 1)_game->EndServerGame(true);
				else _game->EndServerGame(false);
				if (SQLLOGIN) {
					ombs.Write(SENDADDGAMESQL, 4);
					ombs.WriteString(_game->_lobby.UserName);
					Send(ombs.GetBufferPtr(), ombs.GetByteLength());
				}
				//Donem av�s al servidor del nostre nom per a que updateji les partides jugades
				break;
			case NEWPLAYER:
				ReceiveNewPlayer(imbs);
				ombs.Write(SENDNEWPL, 4);
				Send(ombs.GetBufferPtr(), ombs.GetByteLength());
				break;
			case PING:
				//mirem si tot esta correcte
				ReceivePing(imbs);
				ombs.Write(SENDPING, 4);
				Send(ombs.GetBufferPtr(), ombs.GetByteLength());
				break;
				/////////////////LOBBY///////////////
			case ACKCREATEROOM:
				//constestacio de si la creacio de salas ha sigut correcte o no
				imbs.Read(&boolWin, 1);
				_game->_lobby.ACKCreateRoom(boolWin);
				break;
			case LISTROOMS:
				//Contestacio a la peticio de sales amb la llista de les sales disponibles
				_game->_lobby.ListRooms(imbs);
				//type_numsales_id_nom_pl_maxpl
				break;
			case ACKACCESSROOM:
				//Contestacio a si el jugador es pot unir a la sala solicitada
				imbs.Read(&boolWin, 1);
				_game->_lobby.ACKCreateRoom(boolWin);
				break;
			case RECEIVETEXT:
				//Porpagar missatge
				imbs.ReadString(&msg);
				_game->_lobby.AddMessageChat( msg);
				break;
			case READYGAME:
				//Notificar al propietari de la sala que esta amb max pl i pot comen�ar la partida
				_game->_lobby.ReceiveReady();
				break;
			case ACKLOGIN:
				imbs.Read(&boolWin, 1);
				_game->_lobby.ACKLogin(boolWin);
				Loged = boolWin;
				//li diem el resultat al lobby
				break;
			default:
				break;
			}
		}
	if (!SQLLOGIN) {
		SendHello();
		SendHelloInit();
		CooldownSendData();
		CheckSendShot();
	}
	else {
		//mirem si el define esta a true i llavors comprovem si esta logejat
		if (Loged) {
	
			SendHello();
			SendHelloInit();
			CooldownSendData();
			CheckSendShot();
		}
	}
}

//Funci� que rep dades de s� mateix
void NetworkClient::ReceiveSelfData(InputMemoryBitStream & data) {
	std::vector<int> Movements;
	int Move = 0; int NumberMov = 0;
	bool shoot = 0, positiu = 0;
	bool MovOK = 0; int IDMove = 0; int IDPl = 0; int XPl = 0; int YPl = 0; int angle = 0;//s�ha d�igualar a 0 d�un bon principi
	data.Read(&IDMove, 14);
	data.Read(&IDPl, 3);
	data.Read(&angle, 9);
	data.Read(&MovOK, 1);
	data.Read(&positiu, 1);
	data.Read(&XPl, 13);
	data.Read(&YPl, 13);
	data.Read(&shoot, 1);
	data.Read(&NumberMov, 8);
	for (int i = 0; i < NumberMov; i++) {
		data.Read(&Move, 3);
		Movements.push_back(Move);
		//fiquem a 0 per a que el bitstrem no es queixi
		Move = 0;
	}
	UpdatePosition(glm::vec3(((float)XPl / 1000.0f), ((float)YPl / 1000.0f), 0.15f), angle, IDMove, IDPl, MovOK, Movements, shoot, positiu);
	_game->moveTank();

}

void NetworkClient::ReceiveShoot(InputMemoryBitStream & data){
	int IDPl = 0; int IDShoot = 0; int Shoot = 0;
	data.Read(&IDPl, 3);
	data.Read(&Shoot, 1);
	if (IDPl == _game->Panzer.GetID()) {
		//Fem que el enviament constant de missatges del shoot pari
		ResendingShoot = false;
		TimesToResend = 0;
		OutputMemoryBitStream ombs;
		ombs.Write(SENDACKSHOOT, 4);
		ombs.Write(_game->Panzer.GetID(), 2);
		Send(ombs.GetBufferPtr(), ombs.GetByteLength());
	}
	else {
		for (int i = 0; i < _game->_Enemies.size(); i++) {
			if (Shoot == 1) {
				if (_game->_Enemies[i].GetID() == IDPl) {
					_game->_Enemies[i].setShoot(true);
					OutputMemoryBitStream ombs;
					ombs.Write(SENDACKSHOOT, 4);
					ombs.Write(_game->Panzer.GetID(), 2);
					Send(ombs.GetBufferPtr(), ombs.GetByteLength());
				}
			}
		}
	}
}

//s'haura de canviar per a que funcioni amb el moviment nou, no toco res perque es canviara
void NetworkClient::SendData() {
	//updatejem ultima posicio
	_game->Panzer.UpdateLastPos();
	for (int i = 0; i < _game->_Enemies.size(); i++) {
		_game->_Enemies[i].UpdateLastPos();
	}
	OutputMemoryBitStream ombs;
	//Tipus del missatge, 4 bits
	ombs.Write(SENDDATA, 4);
	//IDMove
	//Passem 16 bits perque la partida durara 5 minuts
	//REVISAR agafar el move de game
	ombs.Write(_game->Moves, 14);
	//IDPlayer 3 bits
	ombs.Write(_game->Panzer.GetID(), 3);

	ombs.Write((int)glm::mod(_game->Panzer.getAngleBOTTOM(), 360.0f), 9);
	//signe
	if (_game->Panzer.LastPosX < 0.0f)
		ombs.Write(0, 1);
	else ombs.Write(1, 1);
	//passem la posicio de X
	int posX = (int)(glm::abs((_game->Panzer.LastPosX*1000.0f)));
	ombs.Write((uint16_t)posX, 13);
	//passem la posic� de Y
	int posY = (int)(_game->Panzer.LastPosY*1000.0f);
	ombs.Write((uint16_t)posY, 13);
	//agafem la ultima posicio coneguda
	int NumberMov = _game->_Movements[_game->_Movements.size() - 1].getSize();
	ombs.Write(NumberMov, 8);
	for (int i = 0; i < NumberMov; i++) {
		ombs.Write(_game->_Movements[_game->_Movements.size() - 1].getMove(i),3);
	}
	_game->AddMovement();
	//enviem les dades
	Send(ombs.GetBufferPtr(), ombs.GetByteLength());

	

}

void NetworkClient::Send(char* data, size_t size) {
	sf::Socket::Status state;
	sf::IpAddress ip = sf::IpAddress::getLocalAddress();
	socket.send(data, size, ip, 55000);
}

void NetworkClient::SpawnTanks(int ID) {
	//Ho fiquem aqui per no embrutar m�s el Game.
	//Spawnejar el player
	//es la ID de 1-4, per tant id-1 per trobar posici�
	glm::vec3 positionTop;
	positionTop = _game->Panzer.getPositionTOP();
	positionTop.x = _game->SpawnPoints[ID - 1]._point.x;
	positionTop.y = _game->SpawnPoints[ID - 1]._point.y;
	_game->Panzer.setPositionTOP(positionTop);
	//Positionbottom
	glm::vec3 positionBottom;
	positionBottom = _game->Panzer.getPositionBOTTOM();
	positionBottom.x = _game->SpawnPoints[ID - 1]._point.x;
	positionBottom.y = _game->SpawnPoints[ID - 1]._point.y;
	_game->Panzer.setPositionBOTTOM(positionBottom);
	//cel�les

	_game->Panzer.setCellX(_game->ConvertPositionToGridX(glm::abs(_game->SpawnPoints[ID - 1]._point.x)));
	_game->Panzer.setCellY(_game->ConvertPositionToGridX(_game->SpawnPoints[ID - 1]._point.y));
	_game->Panzer.SetID(ID);
	//moure el model
	_game->_gameElements.getGameElement(TOP_TANK)._angle = _game->Panzer.getAngleTOP();
	_game->_gameElements.getGameElement(TOP_TANK)._rotation.z = 1.0f;
	_game->_gameElements.getGameElement(BOTTOM_TANK)._angle = _game->Panzer.getAngleBOTTOM();
	_game->_gameElements.getGameElement(BOTTOM_TANK)._rotation.z = 1.0f;
	_game->_gameElements.getGameElement(TOP_TANK)._translate = _game->Panzer.getPositionTOP();
	_game->_gameElements.getGameElement(BOTTOM_TANK)._translate = _game->Panzer.getPositionBOTTOM();

	//Els enemics
	//_game->SpawnEnemy(ID);

}

void NetworkClient::SendHello() {
	if (SendHelloBool) {
		if (!Welcomed) {
			if (std::clock() - TimeHello > 2000) {
				OutputMemoryBitStream ombs;
				ombs.Write(SENDHELLO, 4);
				Send(ombs.GetBufferPtr(), ombs.GetByteLength());
				TimeHello = std::clock();                                                                                                                            
			}
		}
	}
}

void NetworkClient::SendHelloInit() {
	if (SendHelloBoolInit) {
		if (!WelcomedInit) {
			if (std::clock() - TimeHelloInit > 2000) {
				OutputMemoryBitStream ombs;
				ombs.Write(SENDHELLOINIT, 4);
				Send(ombs.GetBufferPtr(), ombs.GetByteLength());
				TimeHello = std::clock();
			}
		}
	}
}

void NetworkClient::CooldownSendData(){
	if (isInGame) {
		if ((std::clock() - TimeSendData) > 100) {
			SendData();
			TimeSendData = std::clock();
		}
	}
}

void NetworkClient::ReceiveNewPlayer(InputMemoryBitStream & data) {
	int Size = 0; int IDPl = 0; int XPl = 0; int YPl = 0; //s�ha d�igualar a 0 d�un bon principi
	data.Read(&Size, 3);
	for (int i = 0; i < Size; i++) {
		data.Read(&IDPl, 3);
		data.Read(&XPl, 4);
		data.Read(&YPl, 4);
		bool trobat = false;
		if (IDPl != _game->Panzer.GetID()) {
			for (int i = 0; i < _game->_Enemies.size(); i++) {
				if (_game->_Enemies[i].GetID() == IDPl)trobat = true;
			}
		}
		else trobat = true;
		if (!trobat) {
			//spawnejem tanc
			players[player] = IDPl;
			player++;
			_game->SpawnEnemy(IDPl, XPl, YPl);
		}
	}
	
}

void NetworkClient::ReceivePing(InputMemoryBitStream & data) {
	std::map <int, bool> _IDOK;
	int Size = 0; int IDPl = 0; int Score = 0; //s�ha d�igualar a 0 d�un bon principi
	int numberPl = 0;
	std::vector<int> _IDS;
	data.Read(&Size, 3);
	for (int i = 0; i < Size; i++) {
		data.Read(&IDPl, 3);
		data.Read(&Score, 7);
		numberPl++;
		//Receive new player
		bool notrobat = true;
		if (IDPl != _game->Panzer.GetID()) {
			for (int i = 0; i < _game->_Enemies.size(); i++) {
				if (_game->_Enemies[i].GetID() == IDPl){
					_game->_Enemies[i].setScore(Score);
					notrobat = false;
				}
			}
		}
		else{
			_game->score = Score;
			notrobat = false;
		}
		if (notrobat) {
			//spawnejem tanc
			players[player] = IDPl;
			player++;
		}
		_IDS.push_back(IDPl);
	}
	//si la quantitat de jugadors que tenim guardats es major que la cuantitat que ens transmet el ping
	if (player > Size){
		//incialitzem el map que conte si hem rebut info en aquest ping de els jugadors que tenim guardats que hi ha en partida
		//en false
		for (int i = 0;i < player;i++){
			_IDOK[players[i]] = false;
		}
		//recorrem el mapa on tenim guardat la id dels jugadors segons un index qualsevol
		for (int i = 0; i < players.size(); i++) {
			//recorrem el vector de _ids per comparar amb les ids del mapa de players
			for (int j = 0;j < _IDS.size();j++){
				//comprobem si la id dels jugadors que ens han arribat al ping
				//es la mateixa que la que tenim guardada al mapa de jugadors
				if (players[i] == _IDS[j]) _IDOK[players[i]] = true;
			}
		}

		//recorrem el mapa i si trobem alguna ID en fals es que en el ultim ping no hem rebut cap informacio de ell
		//per tant s'ha de borrar del joc
		for (int i = 0;i < _IDOK.size();i++){
			if (!_IDOK[players[i]]){
				for (int j = 0; j < _game->_gameElements.getNumGameElements(); j++) {
					//recorrem game elements per trobar el tank amb la id que s'ha de borrar i ho borrem.
					if (_game->_gameElements.getGameElement(j)._ID == players[i]){
						_game->_gameElements.removeLastElement(j);
						player--;
					}
				}
				for (int j = 0; j < _game->_Enemies.size(); j++) {
					//recorrem enemies i borrem el tank que toca
					if(_game->_Enemies[j].GetID()== players[i])
					_game->_Enemies.erase(_game->_Enemies.begin() + j);
				}
			}
		}
	}
}

void NetworkClient::ChangePosEnemyTank(int PosX, int PosY, int ID, int posEnem) {
	for (int i = 0; i < _game->_gameElements.getNumGameElements(); i++) {
		if (_game->_gameElements.getGameElement(i)._ID == ID) {
			glm::vec3 pos;
			pos.x = _game->ConvertGridToPositionX(PosX);
			pos.y = _game->ConvertGridToPositionY(PosY);
			_game->_Enemies[posEnem].setPositionBOTTOM(pos);
			_game->_Enemies[posEnem].setPositionTOP(pos);
			_game->_Enemies[posEnem].setCellX(PosX);
			_game->_Enemies[posEnem].setCellY(PosY);
			_game->_gameElements.getGameElement(i)._translate = pos;
		}
	}
}

void NetworkClient::SendShot(){
	if (isInGame){
		//no cal fer cua de missatges perque no enviara mes de 5 vegades en total 100 ms, i el delay de disparo es mes gran
		ResendingShoot = true;
		TimesToResend = 5;
		_game->Panzer.SHOOT = false;
		TimeSendShoot = std::clock();
	}
}

void NetworkClient::CheckSendShot(){
	if (TimesToResend <= 5) {
		if (ResendingShoot) {
			if ((std::clock() - TimeSendShoot) > 50) {
				OutputMemoryBitStream ombs;
				//Tipus del missatge, 4 bits
				ombs.Write(SENDSHOOT, 4);
				ombs.Write(_game->Panzer.GetID(), 3);
				Send(ombs.GetBufferPtr(), ombs.GetByteLength());
				TimeSendShoot = std::clock();
				TimesToResend--;
				if (TimesToResend == 0)ResendingShoot = false;
			}
		}
	}
}

void NetworkClient::CreateLobbyMessage(int ID){
	OutputMemoryBitStream ombs;
	int Type = 0; std::string nameSala = ""; int MaxPl = 0;
	switch (ID) {
	case SENDCREATEROOM:
		ombs.Write(SENDCREATEROOM, 4);
		ombs.WriteString(_game->_lobby.NameRoom);
		ombs.Write(_game->_lobby.MaxPlayersRoom, 3);
		break;
	case SENDLIST:
		ombs.Write(SENDLIST, 4);
		break;
	case SENDENTERROOM:
		ombs.Write(SENDENTERROOM, 4);
		ombs.Write(_game->_lobby.IDRoomAcces, 3);
		break;
	case SENDCHATTEXT:
		ombs.Write(SENDCHATTEXT, 4);
		ombs.WriteString(_game->_lobby.MessageToSend);
		break;
	case SENDINITGAME:
		ombs.Write(SENDINITGAME, 4);
		break;
	case SENDLOGIN:
		ombs.Write(SENDLOGIN, 4);
		ombs.WriteString(_game->_lobby.UserName);
		ombs.WriteString(_game->_lobby.Pass);
		break;
	}
	Send(ombs.GetBufferPtr(), ombs.GetByteLength());
}



NetworkClient::NetworkClient(Game* game) {
	if (socket.bind(55001) != sf::Socket::Done) {
		if (socket.bind(55002) != sf::Socket::Done) {
			if (socket.bind(55003) != sf::Socket::Done) {
				if (socket.bind(55004) != sf::Socket::Done) {
					std::cout << "Error de conexion" << std::endl;
				}
			}
		}
	}
	SendHelloBool = true;
	TimeHello = std::clock();
	socket.setBlocking(false);
	_game = game;
	Move = 0;
	player = 0;
	ResendingShoot= false;
	TimesToResend = 0;
}

NetworkClient::~NetworkClient() {
	socket.unbind();
}