#pragma once

//Player Tank objects

#define TOP_TANK 4
#define BOTTOM_TANK 5
#define BULLETOBJECT 7

#define DIRLIGHT 0
#define POINTLIGHT 1
#define SPOTLIGHT 2

#define WELCOME 0
#define INITGAME 1
#define SPAWNCLIENT 2
#define SELFMOV 3
#define RECEIVESHOOT 4
#define ENDGAME 5
#define NEWPLAYER 6
#define PING 7
#define ACKCREATEROOM 8
#define LISTROOMS 9
#define ACKACCESSROOM 10 
#define RECEIVETEXT 11
#define READYGAME 12
#define INITURN 13
#define ACKLOGIN 14

#define FAILPER 10

#define SENDHELLO 0
#define SENDDATA 1
#define SENDPING 2
#define SENDINITTURN 3
#define SENDSHOOT 4
#define SENDACKSHOOT 5
#define SENDNEWPL 6
#define SENDCREATEROOM 7
#define SENDLIST 8
#define SENDENTERROOM 9
#define SENDCHATTEXT 10
#define SENDINITGAME 11
#define SENDHELLOINIT 12
#define SENDACKINITGAME 13
#define SENDLOGIN 14
#define SENDADDGAMESQL 15

#define SQLLOGIN false