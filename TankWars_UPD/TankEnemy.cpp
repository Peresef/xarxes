#include "TankEnemy.h"



void TankEnemy::AutomaticMovement(std::vector<Cell> cells){
	TempsCells = cells;
	ActCell = 0;
}

bool TankEnemy::CheckIsinCell()
{
	//ojo que aquesta funcio esta malament, s'ha de convertir la posicio de la cel�la en posicio del mapa, WIP
	//std::cout << "\n" << ((TempsCells[ActCell].getPosY()*0.5f) + 0.25f);
	//std::cout << "\n" << getPositionTOP().y;
	//std::cout << "\n" << ActCell;
	return (glm::abs(getPositionTOP().y - ((TempsCells[ActCell].getPosY()*0.5f) + 0.25f)) < 0.1f
		&&
		glm::abs(getPositionTOP().x - ((-TempsCells[ActCell].getPosX()*0.5f) - 0.25f)) < 0.1f);
		
		
}

void TankEnemy::UpdateMovement(){
	if (_Movements.size() > 0) {
		//Aix� es la W
		if (_Movements[0] == 0) {
			moveTank(((glm::sin(glm::radians(getAngleBOTTOM())))*0.015f), -((glm::cos(glm::radians(getAngleBOTTOM())))*0.015f));
			//Panzer.moveTank((glm::sin(glm::radians(_gameElements.getGameElement(BOTTOM_TANK)._angle)))*0.015f, -(glm::cos(glm::radians(_gameElements.getGameElement(BOTTOM_TANK)._angle)))*0.015f);
		}
		//Aix� es la S
		else if (_Movements[0] == 1) {
			moveTank(-((glm::sin(glm::radians(getAngleBOTTOM())))*0.015f), ((glm::cos(glm::radians(getAngleBOTTOM())))*0.015f));
		}
		//Aix� es la D
		else if (_Movements[0] == 2) {
			rotateTank(-2);
		}
		//Aix� es la A
		else if (_Movements[0] == 3) {
			rotateTank(2);
		}
		//eliminem la primera posici� del moviment i que vagi fent
		_Movements.erase(_Movements.begin());
		if (_Movements.size() == 0) {
			// position top
			glm::vec3 positionTop;
			positionTop = getPositionTOP();
			positionTop.x = -posSinc.x;
			positionTop.y = posSinc.y;
			setPositionTOP(positionTop);
			//Positionbottom
			glm::vec3 positionBottom;
			positionBottom = getPositionBOTTOM();
			positionBottom.x = -posSinc.x;
			positionBottom.y = posSinc.y;
			setPositionBOTTOM(positionBottom);
			setAngBOTTOMAbs(angleSinc);
		
		}
	}


/*	if (_Movements) {
		setPositionBOTTOM()
		positionTop = _game->Panzer.getPositionTOP();
		_Movements = false;
	}
	
	*/

	//aixo sera el moviment, m'ho he de mirar mes 
	//la posicio 0 sempre sera la teva
	/*if (Turn) {
		if (Move) {
			if (CheckIsinCell()) {
				//aqui si es cel�la, vol dir que si es diferent la seg�ent cel�la el moviment anira amb les X
				//s'ha de filtrar que no estiguem al final
				ActPosX = TempsCells[ActCell].getPosX();
				ActPosY = TempsCells[ActCell].getPosY();
				//
				if (ActCell >= TempsCells.size() - 1) {

					//S'ha acabat el moviment per tant toca attack
					//REVISAR
					if(WillShoot)InitRotateAttack();//revisar que el angle haur� de venir del servidor
					else {
						//voldra dir que s'ha acabat el torn, cosa que vol dir que no se si s'ha de fer algo mes
						ResetVariables();
						Turn = false;
					}
					Move = false;
					return;
				}
				if (TempsCells[ActCell].getPosX() != TempsCells[ActCell + 1].getPosX())MovX = 1;
				else MovX = 0;
				//lo mateix que amb les X pero en aquest cas les Y
				if (TempsCells[ActCell].getPosY() != TempsCells[ActCell + 1].getPosY())MovY = 1;
				else MovY = 0;
				isMoving = false;
				LookAt();
				ActCell++;
			}
			if (isMoving)moveTank(((glm::sin(glm::radians(getAngleBOTTOM())))*0.05f)*MovX, -((glm::cos(glm::radians(getAngleBOTTOM())))*0.05f)*MovY);
			if (isRotating)UpdateAngleLookAt();
		}
		if (RotateAttack)UpdateAngleLookAt();
	}*/
}

void TankEnemy::LookAt(){
	//funcio de merda que ja veuras que maca sera, perque s'ha de tenir en compte les 9 posicions veines
	//mirem el de abaix i adalt
	if (TempsCells[ActCell+1].getPosX() == TempsCells[ActCell].getPosX()) {
		//amunt
		if (TempsCells[ActCell+1].getPosY() == TempsCells[ActCell].getPosY() - 1)ProxAngle = 0;
		//avall
		else if (TempsCells[ActCell+1].getPosY() == TempsCells[ActCell].getPosY() + 1)ProxAngle = 180;
	}
	else if (TempsCells[ActCell+1].getPosY() == TempsCells[ActCell].getPosY()) {
		//esquerra
		if (TempsCells[ActCell+1].getPosX() == TempsCells[ActCell].getPosX() - 1)ProxAngle = 90;
		//dreta
		else if (TempsCells[ActCell+1].getPosX() == TempsCells[ActCell].getPosX() + 1)ProxAngle =  270;
	}
	else {
		if (TempsCells[ActCell+1].getPosX() == TempsCells[ActCell].getPosX() - 1) {
			//adaltesquerra
			if (TempsCells[ActCell+1].getPosY() == TempsCells[ActCell].getPosY() - 1)ProxAngle = 45;
			//abaix esquerra
			else if (TempsCells[ActCell+1].getPosY() == TempsCells[ActCell].getPosY() + 1)ProxAngle = 135;
		}
		else if (TempsCells[ActCell+1].getPosX() == TempsCells[ActCell].getPosX() + 1) {
			//adaltdreta
			if (TempsCells[ActCell+1].getPosY() == TempsCells[ActCell].getPosY() - 1)ProxAngle = 315;
			//abaixdreta
			else if (TempsCells[ActCell+1].getPosY() == TempsCells[ActCell].getPosY() + 1)ProxAngle = 225;
		}
	}
	isRotating = true;
}

void TankEnemy::UpdateAngleLookAt(){
	//aixo vol dir que mirarem en quina direcci� s'ha de rotar
	if (!RotateAttack) {
		if (ProxAngle != 0) {
			
			if (glm::mod(getAngleBOTTOM(),360.0f) < glm::mod((float)ProxAngle, 360.0f))rotateTank(4);
			else if (glm::mod(getAngleBOTTOM(), 360.0f) > glm::mod((float)ProxAngle, 360.0f))rotateTank(-4);
		}
		else {
			if (glm::mod(getAngleBOTTOM(), 360.0f) < 180)rotateTank(-4);
			else rotateTank(4);
		}
		if (glm::abs(glm::mod(getAngleBOTTOM(), 360.0f) - glm::mod((float)ProxAngle, 360.0f)) <= 5) {
			isRotating = false;
			isMoving = true;
		}
	}
	else {
		setAngleTOPAbs(glm::mod((float)ProxAngle, 360.0f));
		setAngBOTTOMAbs(glm::mod((float)ProxAngle, 360.0f));
		ResetVariables();
		//SHOOT
		Shoot = true;
	}
	
	
	
}

void TankEnemy::InitMovement(std::vector<int> Movements, glm::vec3 pos, int angle){
	_Movements = Movements;
	angleSinc = angle;
	posSinc = pos;
	//ResetVariables();
	//LookAt();//no estic del tot */
	//TempsCells = Cells;
	/*Cell cell1;
	cell1.setCellPosition(11, 0);
	Cell cell2;
	cell2.setCellPosition(10, 0);
	Cell cell3;
	cell3.setCellPosition(9, 1);
	Cell cell4;
	cell4.setCellPosition(9, 2);
	Cell cell5;
	cell5.setCellPosition(8, 2);
	Cell cell6;
	cell6.setCellPosition(8, 3);
	TempsCells.push_back(cell1);
	TempsCells.push_back(cell2);
	TempsCells.push_back(cell3);
	TempsCells.push_back(cell4);
	TempsCells.push_back(cell5);
	TempsCells.push_back(cell6);*/
	//ens assegurem que el tank es mou
/*	AngleServer = angle;
	WillShoot = shoot;
	ActCell = 0;
	if (TempsCells.size() > 1) {
		if (ActPosX != TempsCells[1].getPosX())MovX = 1;
		else MovX = 0;
		if (ActPosY != TempsCells[1].getPosY())MovY = 1;
		else MovY = 0;
		Move = true;
		Turn = true;
		isMoving = true;
	}
	else if (WillShoot) {
		InitRotateAttack();
		Turn = true;
	}
	else {
		//No ha fet res durant el torn no se si s'haura de fer algo aqui
	}
	*/
}

void TankEnemy::InitRotateAttack(){
	ProxAngle = AngleServer;
	RotateAttack = true;
	//LookAt();
	isRotating = true;
}


//REVISAR
void TankEnemy::ResetVariables()
{
	RotateAttack = false;
	Move = false;
	isRotating = false;
	isMoving = false;
	WillShoot = false;
	Turn = false;;
	Shoot = false;//aix�
}

bool TankEnemy::getShoot(){
	return Shoot;
}

void TankEnemy::setShoot(bool boolea){
	Shoot = boolea;
}

void TankEnemy::InitTankEnemy(glm::vec3 positiontop, glm::vec3 positionbottom, float angleTOP, float angleBOTTOM, int health, int delay, int _ActPosX, int _ActPosY)
{
	initTank(positiontop, positionbottom,angleTOP, angleBOTTOM,  health,  delay);
	ActPosX = _ActPosX;
	ActPosY = _ActPosY;
}

int TankEnemy::getScore(){
	return Score;
}

void TankEnemy::setScore(int _Score){
	Score = _Score;
}

TankEnemy::TankEnemy() {
}

TankEnemy::~TankEnemy(){
}
