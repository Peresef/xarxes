#pragma once
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include <ctime>
#include "Cell.h"
#include <vector>
class Tank
{
protected: 
	int ActPosX;//cel�la X
	int ActPosY;//cel�la Y

private:
	glm::vec3 _positionTOP;
	glm::vec3 _positionBOTTOM;
	float _angleTOP;
	float _angleBOTTOM;
	int _health;
	int _delay;
	bool Shot;
	bool _canMove=true;
	float AngleBala;
	std::clock_t start; ///per fer calculs de timing
	int _ID;//es la ID que se li donara per a reconeixe'l, important assginar-les b�, ho hauria de fer el servidor

public:
	std::vector<Cell> TempsCells;
	std::vector<int> _Movements;

	bool SHOOT; //es fa servir per enviar si s'ha fet o no

	float LastPosX;
	float LastPosY;
	float FirstPosX;
	float FirstPosY;

	Tank();
	void UpdateLastPos();
	void initTank(glm::vec3 positiontop, glm::vec3 positionbottom, float angleTOP, float angleBOTTOM, int health, int delay);
	void moveTank(float x, float y);
	void setAngleTOP(int angle);
	void rotateTank(int angle);
	glm::vec3 getPositionTOP();
	glm::vec3 getPositionBOTTOM();
	void setPositionBOTTOM(glm::vec3 position);
	void setPositionTOP(glm::vec3 position);
	float getAngleTOP();
	float getAngleBOTTOM();
	float getAngleBala();
	void setAngleTOPAbs(int angle);
	void setAngBOTTOMAbs(int angle);
	void Fire();
	bool canFire();
	bool ShotActive();
	bool getCanMove();
	void setCanMove(bool estate);
	void setAnglebala(int angle);
	int getCellX();
	int getCellY();
	void setCellX(int _x);
	void setCellY(int _y);
	int GetID();
	void SetID(int _id);
	~Tank();
};

