#pragma once


//Third-party libraries
#include <GL/glew.h>			//The OpenGL Extension Wrangler
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "Window.h"
#include "GLSLProgram.h"
#include "FPSLimiter.h"
#include "OpenGLBuffers.h"
#include "Vertex.h"
#include "Geometry.h"
#include "InputManager.h"
#include "Camera.h"
#include "GameConstants.h"
#include "Tank.h"
#include "Timestep.h"
#include "Enemy.h"
#include <ctime>
#include "TextureManager.h"
#include "MaterialManager.h"
#include "Light.h"
#include "TextManager.h"
#include "TankEnemy.h"
#include "NetworkClient.h"
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include "MovementPlayer.h"
#include "Lobby.h"
#include "OutputMemoryBitStream.h"
//Game has four possible states: INIT (Preparing environment), PLAY (Playing), EXIT (Exit from the game) or MENU (Game menu)
//enum class ProgramState {LOBBY, GAME};
enum class GameState{INIT, PLAY, EXIT, MENU};
enum class RenderState { RANKING, GAME, WIN, LOSE };

//This class manages the game execution
class Game {
	
	public:		
		friend class NetworkClient;

		Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS);	//Constructor
		~Game();					//Destructor
		void run();//Game execution
		

	private:
		//LOBBY
		Lobby::ProgramState  _programState = Lobby::ProgramState::LOBBY;
		Lobby _lobby;

		NetworkClient *_Client;

		//render
		RenderState _renderState;
		std::vector<int> _ScoreEnemies;
		std::vector<int> _IDEnemies;

		std::vector<MovementPlayer> _Movements;
		bool StartGame = false; //aix� vol dir que ja es pot jugar
		int Moves = 0;
		int IDShoot = 0;
			//Attributes turn
		std::clock_t GameTurn;
		bool isinTurn = false;//per saber si esta en mig d'un torn
		bool TurnMovement = false; //per saber si esta en el de moviment
		bool TurnAttack; // Per saber si esta en el d'atac
		bool Shooted = false;

		int numberPlayers = 0;
		std::clock_t shottime;
		float Time;
		int score;
		std::clock_t spawntime;
		std::string _windowTitle;		//Window Title
		bool FPCamera;
		bool TPCamera;
		float _angleBALA;
		Tank Panzer;
		int _screenWidth;				//Screen width in pixels				
		int _screenHeight;				//Screen height in pixels				
		GameState _gameState;			//It describes the game state				
		Window _window;					//Manage the OpenGL context
		GLSLProgram _colorProgram;		//Manage the shader programs
		FPSLimiter _fpsLimiter;			//Manage the synchronization between frame rate and refresh rate
		OpenGLBuffers _openGLBuffers;	//Manage the openGL buffers
		Geometry _gameElements;			//Manage the game elements
		InputManager _inputManager;		//Manage the input devices
		Camera _camera;
		Timestep _tp;
		struct SpawnPoint{
			glm::vec3 _point;
			bool _spawned;
		};
		std::vector<SpawnPoint> SpawnPoints;
		TextureManager _textureManager;
		MaterialManager _materialManager;
		TextManager _textManager;
		
		GLuint _newColorUniform;
		GLint _textureDataLocation;
		GLint _textureScaleFactorLocation;

		//init uniform materials
		GLuint _materialAmbientUniform;
		GLuint _materialDiffuseUniform;
		GLuint _materialSpecularUniform;
		GLuint _materialShininessUniform;

		//init uniform lights
		GLuint _lightAmbientUniform;
		GLuint _lightDiffuseUniform;
		GLuint _lightSpecularUniform;
		GLuint _lightTypeUniform;
		GLuint _lightDirUniform;
		GLuint _lightCutOffUniform;
		GLuint _lightConstantUniform;
		GLuint _lightLinearUniform;
		GLuint _lightQuadraticUniform;
		GLuint _lightPosition;
		GLuint _lightOn;

		std::vector<Light> _lights;




		//EnemyTank
		std::vector<TankEnemy> _Enemies;
		bool UpdateEnemy = false; //debug bool
			//Internal methods
		void initSystems();
		void initShaders();
		void initLights();
		void gameLoop();
		void processInput();
		void doPhysics();
		bool collisionBala(glm::vec3 movement, int i);
		void executePlayerCommands();
		void renderGame();
		void moveTank();
		void SpawnEnemy(int ID, int PosX, int PosY);
		void EndGame();
		void AddScore(int _i);
		void loadGameTextures();
		void UpdateEnemyMovements();
		void CheckCell();
		int ConvertPositionToGridX(float _X);
		void InitTurn();
		void EndServerGame(bool win);
		void Disconnected();
		void Ranking();
		float ConvertGridToPositionY(float _X);
		float ConvertGridToPositionX(float _X);
		void AddMovement();
		void EraseMovements(int IDMove);
		void CheckEndGame();
};

