#pragma once
#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "GameConstants.h"

struct Room {
	std::string Name;
	int ID;
	int ActualPlayers;
	int MaxPlayers;
};


class Lobby
{

private:
	enum class WindowState {TITLE, INTRODUCENAME, CHOOSEOPTIONS, LISTGAMES, CHAT, EXIT, INTRODUCEUSER, INTRODUCEPASS};

	int _screenWidth;
	int _screenHeight;

	//ID del jugador
	int _ID;

	//bool que determina si hi ha missatge per enviar
	bool Sending;
	int MessageIDSend = 0; //MessageSending ID

	//punter al network client per a la comunicaci� amb el Lobby
	//NetworkClient * _networkCl;

	//render variables
	sf::RenderWindow window;
	std::string mensaje = " ";
	sf::Font font;
	sf::Text TitleText;
	sf::Text SubTitleText;
	sf::Text Text;
	sf::RectangleShape Separator;
	sf::RectangleShape SeparatorVertical;
	//LoopActions
	WindowState _windowState = WindowState::TITLE;
	std::vector<std::string> _ListMensajes;
	std::vector<Room> _ListRooms;



	
public:
	std::string NameRoom;//nom de la room que creem
	int MaxPlayersRoom;//max players de la room que creem

	//int de la sala a la qual es vol accedir
	int IDRoomAcces;
	std::string MessageToSend;

	//Bool de si es pot activar el bot� de ready
	bool ReadyButton = false;


	//Variables del Login
	std::string UserName;
	std::string Pass;
	enum class ProgramState { LOBBY, GAME };
	//funci� per encendre la finestra
	void InitLobby();
	void UpdateLobby(ProgramState & _programState);
	void RenderWindow();
	void RenderTitle();
	void RenderSubTitle(std::string _subtitle, float X, float Y);
	void RenderText(std::string _text, float X, float Y);
	void RenderChat();
	void RenderOptions();
	void RenderIntroduceName();
	void RenderIntroduceUser();
	void RenderIntroducePass();
	void RenderFirstScreen();
	void RenderListGames();
	void CheckInput();
	void CheckActions(char Action);
	void CheckPress();
	void AddMessageChat(std::string Message);
	void SetID(int _id);
	void InitGame();
	void ReceiveReady();
	void CheckReady(int X, int Y);
	//Funcions que conectem amb el NetworkClient
	//serveix per a donar l'OK de la creaci� de la sala, si retorna false tornara a la sala inicial
	void ACKCreateRoom(bool boolean);
	//Funci� que llista les sales
	void ListRooms(InputMemoryBitStream imbs);
	//Funci� que demana crear una partida i per tant accedira a chat si li donen acces

	bool CheckSendToNetwork();
	int GetMessageToSend();

	void ACKLogin(bool boolean);



	Lobby(int ScreenWidth, int ScreenHeight);
	~Lobby();
};

