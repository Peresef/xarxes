#pragma once
#define GLM_FORCE_RADIANS
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>

class Camera{
public:
	glm::mat4 _projectionMatrix;
	glm::mat4 _viewMatrix;
	
	Camera();
	Camera(int _screenwidth, int _screenHeight);
	~Camera();

	void init(int _screenwidth, int _screenHeight);
	void perpespectiveMatrix();
	void orthographic();
	void changePos(glm::vec3 _cameraPos, glm::vec3 _cameraFront);
	glm::vec3 getCamPos();

private:
	float _aspecRatio;
	float _FOV;
	float _far;
	float _near;
	float _projectionWidth;
	float _projectionHeight;
	glm::vec3 _cameraPos;
	glm::vec3 _cameraFront;
	glm::vec3 _cameraUp;
};

