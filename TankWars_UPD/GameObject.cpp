#include "GameObject.h"

std::ostream & operator<<(std::ostream & os, const GameObject & p){
	os<<"Object Type: "<< p._objectType << " Translate: " << p._translate.x << " " << p._translate.y << " " << p._translate.z << " Angle: " << p._angle << " Rotation: " << p._rotation.x << " " << p._rotation.y << " " << p._rotation.z << " Scale: " << p._scale.x << " " << p._scale.y << " " << p._scale.z << std::endl;
	return os;
}

std::ofstream & operator<<(std::ofstream & os, const GameObject & p){
	os << p._objectType << " ";
	os << p._translate.x << " ";
	os << p._translate.y << " ";
	os << p._translate.z << " ";
	os << p._angle;
	os << p._rotation.x << " ";
	os << p._rotation.y << " ";
	os << p._rotation.z << " ";
	os << p._scale.x << " ";
	os << p._scale.y << " ";
	os << p._scale.z << " ";
	os << p._materialType << " ";
	return os;
}

std::ifstream & operator>>(std::ifstream & is, GameObject & p){
	is >> p._objectType;
	is >> p._translate.x;
	is >> p._translate.y;
	is >> p._translate.z;
	is >> p._angle;
	is >> p._rotation.x;
	is >> p._rotation.y;
	is >> p._rotation.z;
	is >> p._scale.x;
	is >> p._scale.y;
	is >> p._scale.z;
	is >> p._materialType;
	return is;
}

bool GameObject::OverlapAABB(AABB two) {
	//Check if Box1's max is greater than Box2's min and Box1's min is less than Box2's max
	return(_boundingAABB.Max.x > two.Min.x &&
		_boundingAABB.Min.x < two.Max.x &&
		_boundingAABB.Max.y > two.Min.y &&
		_boundingAABB.Min.y < two.Max.y &&
		_boundingAABB.Max.z > two.Min.z &&
		_boundingAABB.Min.z < two.Max.z);

	//If not, it will return false

	}

/*bool GameObject::OverlapOBB(OBB BB2) {
	int isInside = 0;

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			glm::vec3 PiPj = _boundingOBB.Points[j] - BB2.Points[i];
			float tempDotProduct = glm::dot(PiPj, _boundingOBB.getVector(j));
			if (tempDotProduct > 0) {
				isInside += 1;
				if (isInside == 4) {
					return true;
				}
			}
		}
	}
	return false;
}*/
