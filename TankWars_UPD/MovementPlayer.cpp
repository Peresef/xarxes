#include "MovementPlayer.h"



MovementPlayer::MovementPlayer()
{
}


MovementPlayer::~MovementPlayer()
{
}

void MovementPlayer::AddMove(int move)
{
	_Movements.push_back(move);
}

void MovementPlayer::AddIDMove(int ID){
	_ID = ID;
}

int MovementPlayer::getID()
{
	return _ID;
}

int MovementPlayer::getSize()
{
	return _Movements.size();
}

int MovementPlayer::getMove(int pos)
{
	return _Movements[pos];
}
